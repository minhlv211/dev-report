
function setCookie(cname, cvalue, ex_second) {
    var d = new Date();
    var expires = '';
    if (ex_second != undefined) {
        d.setTime(d.getTime() + (ex_second * 1000));
        expires = "; expires=" + d.toUTCString();
    }
    document.cookie = cname + "=" + cvalue + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

$().ready(function () {

    $('.auto_submit').change(function () {
        $('.filter-form').submit();
    });

    $(document).on('keydown', '.number_only', function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+V
            (e.keyCode == 86 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything

            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && e.keyCode != 189) {
            e.preventDefault();
        }
    });

    $('.filter-form').on('submit', function (event) {
        event.preventDefault();

        var url = $(this).attr('action');
        if (!url) {
            url = location.pathname + '?';
        }

        $('input, select', this).each(function () {
            var name = $(this).attr('name');
            var value = $(this).val();

            if (this.tagName == 'INPUT' && ($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio')) {
                if (!this.checked) {
                    return;
                }
            }

            if (value && name && name != 'daterangepicker_start' && name != 'daterangepicker_end') {
                url += '&' + name + '=' + encodeURIComponent(value);
            }
        });
        window.location = url;
    });

});

function uploadFile(file,el) {
    var data = new FormData();
    data.append('file',file[0]);
    $.ajax({
        url: "/image-upload?ajax=1",
        data:data,
        method: "POST",
        cache: false,
        contentType: false,
        processData: false, // Don't process the files
        success:function (data) {
            data = JSON.parse(data);
            if (data.status) {
                $(el).summernote('insertImage', data.file, data.file);
            }
        },
        error:function (xhr,status,error) {
            console.log(status + " " + error);
        }
    });
}
