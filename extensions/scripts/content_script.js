$(document).ready(function () {
    var tableEl = document.getElementById('commit-table');
    var rowEl = tableEl.getElementsByClassName('table-row');
    var pageData = [];
    var project = 'cps_new';

    for (var i = 0; i < rowEl.length; i++) {
        var author = rowEl[i].getElementsByClassName('commit-author')[0].innerHTML;
        var created = rowEl[i].getElementsByClassName('PrettyTime')[0].title;
        var commit = $(rowEl[i]).find('.mouse-helper div:nth-child(3) div').text();
        var issue = $(rowEl[i]).find('.project--issue');
        var issueOpen = $(issue[0]).find('span').text()[0];
        var issueFixed = $(issue[1]).find('span').text()[0];

        if (issueOpen === '-') issueOpen = 0;
        if (issueFixed === '-') issueFixed = 0;

        commit = commit.trim();
        if (commit.length > 7) {
            commit = commit.substring(0,6);
        }

        pageData.push([project,author,created,commit,issueOpen,issueFixed]);
    }

    //ajax request
    $.ajax({
        url: "https://codacy.local/codacy-import",
        type: "post",
        data: {params:pageData},
        success:function(result){
            console.log(result);
        }
    });

    var next = $('.selectors').find('span:nth-child(2) a');
    if (next.length > 0) {
        setTimeout(function () {
            next[0].click();
        },3000);
    }
});
