<?php

class Controller {

    /**
     * @var Base
     */
    protected $f3;

    /**
     * @var MongoDB
     */
    protected $db;
    private $_layout = 'template';
    private $_view_name = 'main';

    protected function setLayout($layout) {
        $this->_layout = $layout;
    }

    protected function setViewName($view_name) {
        $this->_view_name = $view_name;
    }

    protected function setTitle($hoko_title) {
        $this->f3->set('hoko_title', $hoko_title);
    }

    function beforeroute() {
        $class_name = get_class($this);
        $controller_name = substr($class_name, 0, strlen($class_name) - 10);
        $controller_name = strtolower($controller_name);
        $this->f3->set('ENCODING', 'UTF-8');
    }

    function afterroute() {

		$user = AdminUserHelper::get_current_user();
		$this->f3->set('user', $user);

        $view_content = View::instance()->render($this->_view_name . '.php');
        $this->f3->set('view_content', $view_content);
        $this->f3->set('_view_name', $this->_view_name);
        echo View::instance()->render('layout/' . $this->_layout . '.php');
    }

    function __construct($f3) {

        $this->f3 = $f3;
        $message = $this->f3->get('SESSION.message');
        $this->f3->set('SESSION.message', null);
        $this->f3->set('eco_menu', $this->getMenuList());
        $this->setMessage($message['message'], $message['type']);
    }

    function redirect($url, $message = '', $type = 'success') {
        if (substr($url, 0, 7) == 'http://' | substr($url, 0, 8) == 'https://') {
            header('Location: ' . $url, true, 302);
            exit;
        }
        $this->f3->set('SESSION.message', array('type' => $type, 'message' => $message));
        $this->f3->reroute($url);
    }

    protected function setMessage($message, $type = 'success') {
        $this->f3->set('eco_message', $message);
        $this->f3->set('eco_message_type', $type);
    }

    protected function addMessage($message, $type = 'success') {
        $_old_msg = $this->f3->get('eco_message');
        if ($_old_msg) {
            $this->f3->set('eco_message', $_old_msg . '<br />' . $message);
        } else {
            $this->f3->set('eco_message', $message);
            $this->f3->set('eco_message_type', $type);
        }
    }

    private function add_sub_menu_item($item, $parent) {

		$contents = parse_ini_file(ROOT_DIR . '/routes.ini');
		static $listPermistion = null;
		if (empty($listPermistion)) {
			$listPermistion = array();
			foreach ($contents as $_key => $_value) {
				$_key = explode('/', $_key);
				$_key = $_key[1];
				$_value = str_replace('->', '.', $_value);
				$listPermistion[$_key] = $_value;
			}
		}

        if (count($item)) {
			$AdminUserHelper = new AdminUserHelper();
            foreach ($item as $k => $v) {
				$_permission = $listPermistion[$k];
				$check_permission = $AdminUserHelper->has_permission($_permission);
            	if ($check_permission) {
					$parent['children'][$k] = $v;
				}
            }
        }
        return $parent;
    }

    private function getMenuList() {
        $menu_list = array();


        $menu_list[] = array("link" => "index", "name" => "Dashboard", "icon" => "icon-home");

        $_app_menu = array("link" => "", "name" => "Shopify", "icon" => "icon-folder", 'type' => "tree", "isOpen" => true);
			$_app_menu = $this->add_sub_menu_item(array('shopify-products'    => "Shopify Product")        , $_app_menu);
			$_app_menu = $this->add_sub_menu_item(array('shopify-orders'    => "Shopify Order")        , $_app_menu);
		if (isset($_app_menu['children'])){
			$menu_list[] = $_app_menu;
		}

		$_app_menu = array("link" => "", "name" => "Ali", "icon" => "icon-folder", 'type' => "tree", "isOpen" => true);
		$_app_menu = $this->add_sub_menu_item(array('ali-products'    => "Ali Product")        , $_app_menu);
		$_app_menu = $this->add_sub_menu_item(array('ali-orders'    => "Ali Order")        , $_app_menu);
		if (isset($_app_menu['children'])){
			$menu_list[] = $_app_menu;
		}

		$_user_menu = array("link" => "", "name" => "Tracking", "icon" => "icon-user", 'type' => "tree", "isOpen" => true);
		$_user_menu = $this->add_sub_menu_item(array('tracking-list'    => "Tracking List")        , $_user_menu);
		$_user_menu = $this->add_sub_menu_item(array('tracking-stat'    => "Tracking Statistic")        , $_user_menu);
		$_user_menu = $this->add_sub_menu_item(array('tracking-status'    => "Tracking Status")        , $_user_menu);
		if (isset($_user_menu['children'])){
			$menu_list[] = $_user_menu;
		}


		$_app_menu = array("link" => "", "name" => "Report", "icon" => "icon-folder", 'type' => "tree", "isOpen" => true);
		$_app_menu = $this->add_sub_menu_item(array('hourly'    => "Report Hourly")        , $_app_menu);
		$_app_menu = $this->add_sub_menu_item(array('report'    => "Report")        , $_app_menu);
		if (isset($_app_menu['children'])){
			$menu_list[] = $_app_menu;
		}



		$_fb_menu = array("link" => "", "name" => "Facebook", "icon" => "icon-folder", 'type' => "tree", "isOpen" => true);
			$_fb_menu = $this->add_sub_menu_item(array('fb-campaign'    => "Campaign")        , $_fb_menu);
			$_fb_menu = $this->add_sub_menu_item(array('fb-comments'    => "FB Comment")        , $_fb_menu);
			$_fb_menu = $this->add_sub_menu_item(array('report-funnel'    => "Funnel Report")        , $_fb_menu);
		if (isset($_fb_menu['children'])){
			$menu_list[] = $_fb_menu;
		}

		//Email
        $_email_menu = array("link" => "", "name" => "Ticket", "icon" => "icon-folder", 'type' => "tree", "isOpen" => true);
        $_email_menu = $this->add_sub_menu_item(array('ticket-list'    => "List")        , $_email_menu);
        $_email_menu = $this->add_sub_menu_item(array('ticket-report'    => "Report")        , $_email_menu);
        $_email_menu = $this->add_sub_menu_item(array('ticket-signature'    => "Signature")        , $_email_menu);
        if (isset($_email_menu['children'])){
            $menu_list[] = $_email_menu;
        }

        //phan quyen hien thi tren menu
        $_user_menu = array("link" => "", "name" => "User", "icon" => "icon-user", 'type' => "tree", "isOpen" => true);
		$_user_menu = $this->add_sub_menu_item(array('user'    => "Danh sách tài khoản")        , $_user_menu);
		$_user_menu = $this->add_sub_menu_item(array('create-user'    => "Tạo tài khoản")        , $_user_menu);
        if (isset($_user_menu['children'])){
            $menu_list[] = $_user_menu;
        }



        $path = $this->f3->get('PATH');
        $path = substr($path, 1) . "";

        foreach ($menu_list as &$_menu) {
            if ($_menu['link'] && $_menu['link'] == $path) {
                $_menu['active'] = 1;
            } else {
                if (is_array($_menu['children']) && array_key_exists($path, $_menu['children'])) {
                    $_menu['active'] = 1;
                }
            }
        }
        $this->f3->set('TZ', 'Asia/Ho_Chi_Minh');

        return $menu_list;
    }
    
    protected function checkPermission($permission){
        $AdminUserHelper = new AdminUserHelper();
        
        $user = AdminUserHelper::get_current_user();
        if (isset($user)) {
            $check_permission = $AdminUserHelper->has_permission($permission);
            if (!$check_permission) {
                $this->setLayout('unauthorized');
            }
        } else {

            $current_url = urlencode("{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
            $this->redirect('logout?redirect=' . $current_url);
        }
    }


    function isAjax() {
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
			|| $_REQUEST['ajax'] == 1;
	}

	function image_upload()
    {
        if ($this->isAjax()) {
            $error = false;
            $path = '';
            if ($_FILES) {
                $uploaddir = './assets/upload/';
                $http = ($_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
                foreach($_FILES as $file) {
                    $file_name = time().$file['name'];
                    if(move_uploaded_file($file['tmp_name'], $uploaddir .$file_name)) {
                        $path = $http.$_SERVER['SERVER_NAME'].(ltrim($uploaddir,'.')).$file_name;
                    } else {
                        $error = true;
                    }
                }
            }
            $output = array(
                'status' =>  (!$error) ? 1 : 0,
                'file' =>  $path
            );
        }
        die(json_encode($output));
    }
}
