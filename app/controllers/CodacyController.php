<?php

class CodacyController extends Controller {


    function import($f3) {
        $params = $_POST['params'];
//        $params = json_decode($params, true);

        $codacyModel = new CodacyModel();
        $data = $codacyModel->import($params);
        echo json_encode(array('status' => 1,'data'=>$data));
        exit;
    }

    public function index($f3) {
        $this->checkPermission('codacy.index');
        $this->setTitle('Codacy Report');
        $this->setViewName('codacy/index');

        $filters = $_REQUEST['filter'];
        $filter_date = $filters['date_range'];
    }
}
