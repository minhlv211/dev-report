<?php

class AppController extends Controller {
    /**
     * @param Base $f3
     */
    function index($f3) {
		$this->checkPermission('app.index');
        $this->setTitle('Quản lý danh sách app');
        $this->setViewName('apps/index');
		if((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || $_REQUEST['ajax'] == 1) {
			$appModel = new AppModel();
			$total_record = $appModel->countAppList();
			$limit = isset($_REQUEST['limit']) ? intval($_REQUEST['limit']): 25 ;
			$offset = isset($_REQUEST['offset']) ? intval($_REQUEST['offset']): 0 ;
			$sort = isset($_REQUEST['sort']) ? ($_REQUEST['sort']): '_id' ;
			$order = isset($_REQUEST['order']) ? ($_REQUEST['order']): 'desc' ;
			$search = isset($_REQUEST['search']) ? ($_REQUEST['search']): '' ;
			$query = array();
			if ($search) {
				$query = array('app_name' => array('$regex' => new MongoDB\BSON\Regex($search, 'i')));
			}

			$appList = $appModel->getAppList($query, $limit, $offset, $sort, $order);
			$today = date('Y-m-d');
			$last7Day = date('Y-m-d', strtotime('-7 days'));
			foreach ($appList as $_k => $_app) {
				$_app['count'] = number_format($_app['count']);
				if ($_app['status'] == 0) {
					$_app['status'] = '<span class="badge badge-warning">Tạm dừng</span>';
				}elseif ($_app['status'] == 1) {
					$_app['status'] = '<span class="badge badge-info">Đang chạy</span>';
				}elseif ($_app['status'] == 2) {
					$_app['status'] = '<span class="badge badge-danger">Đẫ bị ban</span>';
				}

				$_app['action'] = '<a class="btn btn-xs btn-info" href="app-edit?id='.$_app['_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$url = 'device-stat?&filter[date_range]='.$last7Day.'-'.$today.'&filter[app]='.$_app['package_name'].'&view_by=date';
				$_app['action'] .= ' <a class="btn btn-xs btn-info" href="' . $url . '"><i class="fa fa-list"></i> Report</a>';
				$_app['app_name'] = '<a target="store" href="https://play.google.com/store/apps/details?id='.$_app['package_name'].'">'.$_app['app_name'].'</a>';
				$appList[$_k] = $_app;
			}

			global $start_exe_time;
			$end_exe_time = microtime(true);

			header('Content-Type: application/json');
			echo json_encode(array('total' => $total_record, 'rows' => $appList, 'time' => number_format($end_exe_time - $start_exe_time, 3)));
//			echo json_encode($appList);
			exit();

//			$f3->set('appList', $appList);
		}
        $f3->set('url_page', 'app-list?ajax=1');
    }

    /**
     * @param Base $f3
     */
    function edit($f3) {
		$this->checkPermission('app.edit');
        $this->setTitle('Sửa app');
        $this->setViewName('apps/edit');

        $id = $_REQUEST['id'];
        $appModel = new AppModel();
        $appInfo = $appModel->getAppInfo($id);
        $f3->set('appInfo', $appInfo);
        if (!$appInfo) {
            $this->redirect('app-list', 'App not exist!', 'error');
        }
    }

    /**
     * @param Base $f3
     */
    function create($f3) {
		$this->checkPermission('app.edit');
        $this->setTitle('Thêm app mới');
        $this->setViewName('apps/edit');
        $appInfo = array('status' => 1);
        $f3->set('appInfo', $appInfo);
    }

    /**
     * @param Base $f3
     */
    function save($f3) {
		$this->checkPermission('app.edit');

        $appModel = new AppModel();
        $app_id = $_REQUEST['app_id'];
        if ($app_id) {
            $appInfo = $appModel->getAppInfo($app_id);
            if (!$appInfo) {
                $this->redirect('app-list', 'App not exist!', 'error');
            }
        }  else {
            $appInfo = array();
        }

        $delay_time = intval($_REQUEST['delay_time']);
        if ($delay_time <= 0) {
            $delay_time = 5;
        }
        $appInfo['app_name'] = trim($_REQUEST['app_name']);
		$appInfo['email'] = trim($_REQUEST['email']);
        $appInfo['package_name'] = trim($_REQUEST['package_name']);
        $appInfo['icon'] = $_REQUEST['icon'];
        $appInfo['gcm_sender_id'] = trim($_REQUEST['gcm_sender_id']);
        $appInfo['delay_time'] = $delay_time;
        $appInfo['category'] = $_REQUEST['category'];

        $appInfo['default_ad'] = $_REQUEST['default_ad'];
        $appInfo['ad_full_key'] = $_REQUEST['ad_full_key'];
        $appInfo['ad_banner_key'] = $_REQUEST['ad_banner_key'];
        $appInfo['fan_key'] = $_REQUEST['fan_key'];
        $appInfo['sa_key'] = $_REQUEST['sa_key'];

        $appInfo['status'] = intval($_REQUEST['status']);

        if ($app_id) {
            $result = $appModel->editApp($app_id, $appInfo);
            if ($result) {
                $this->redirect('app-list', 'Sửa thông tin app thành công');
            } else {
                $this->redirect('app-list', 'Sửa thông tin app thất bại', 'error');
            }
        } else {
            $result = $appModel->createApp($appInfo);
            if ($result) {
                $this->redirect('app-list', 'Tạo app thành công');
            } else {
                $this->redirect('app-list', 'Tạo app thất bại', 'error');
            }
        }
    }
}
