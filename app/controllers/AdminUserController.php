<?php

class AdminUserController extends Controller {

    function editSave($f3) {
        $this->checkPermission('adminuser.edit');

        $id = $_REQUEST['id'];
        if (isset($id)) {
            if (strval($user['_id']) != $id) {
                $user_edit = AdminUserModel::getById($id);

                if ($user_edit) {
                    $username = $_POST['username'];
                    $email = $_POST['email'];
                    $fullname = $_POST['fullname'];
                    $role = $_POST['role'];
                    $active = intval($_POST['active']);
                    $partner = $_POST['partner'];
                    $permissions = array();

                    $AdminUserHelper = new AdminUserHelper();
                    if (isset($_POST['permissions']) && is_array($_POST['permissions'])) {
                        foreach ($_POST['permissions'] as $permission) {
                            if (!$AdminUserHelper->is_admin()) {
                                $arr = explode('.', $permission);
                                if ($arr[0] == 'adminuser') {
                                    continue;
                                }
                            }

                            if ($AdminUserHelper->has_permission($permission)) {
                                $permissions[] = $permission;
                            }
                        }
                    }
                    
                    if ($role == AdminUserHelper::ROLE_ADMIN && !$AdminUserHelper->is_admin()) {
                        $role = AdminUserHelper::ROLE_CUSTOM;
                    }
                    if (!$AdminUserHelper->is_admin()) {
                        $partner = $user_edit['partner'];
                    }

                    $user = AdminUserModel::getByUsername($username, array('_id' => array('$ne' => $user_edit['_id'])));
                    if (!$user) {
                        $user = AdminUserModel::getByEmail($email, array('_id' => array('$ne' => $user_edit['_id'])));
                        if (!$user) {
                            $user = array(
                                '_id' => $user_edit['_id'],
                                'username' => $username,
                                'email' => $email,
                                'fullname' => $fullname,
                                'role' => $role,
                                'partner' => $partner,
                                'active' => $active,
                                'permissions' => $permissions
                            );

                            $password = $_POST['password_new'];
                            if (!empty($password)) {
                                if (!isset($user['salt_password']) || empty($user['salt_password'])) {
                                    $user['salt_password'] = uniqid('pwd_');
                                }
                                $password = md5($user['salt_password'] . $password . AdminUserHelper::CRYPT_PASSWORD);
                                $user['password'] = $password;
                            }

                            AdminUserModel::save($user);
                            $this->redirect('user', 'Thay đổi thành công');
                        } else {
                            $this->redirect('edit-user?id=' . $user_edit['_id'], 'Trùng email', 'error');
                        }
                    } else {
                        $this->redirect('edit-user?id=' . $user_edit['_id'], 'Trùng tên tài khoản', 'error');
                    }
                }
            }
        }
        $this->redirect('user', 'Không tồn tại tài khoản này', 'error');
    }

    function edit($f3) {
        $this->checkPermission('adminuser.edit');
        $user = AdminUserHelper::get_current_user();
        $f3->set('user', $user);
        $id = $_REQUEST['id'];
        if (isset($id)) {
            if (strval($user['_id']) != $id) {
                $user_edit = AdminUserModel::getById($id);
                if ($user_edit) {
                    $f3->set('user_edit', $user_edit);
                } else {
                    $this->redirect('user', 'Không tồn tại tài khoản này', 'error');
                }
            } else {
                $this->redirect('user', 'Bạn không thể sửa chính mình', 'error');
            }
        } else {
            $this->redirect('user', 'Không tồn tại tài khoản này', 'error');
        }
        $this->setTitle('Sửa thông tin người dùng');
        $this->setViewName('adminuser/edit');
    }

    function delete($f3) {
        $this->checkPermission('adminuser.delete');
        $id = $_REQUEST['id'];
        $user = AdminUserHelper::get_current_user();
        if (isset($id)) {
            if (strval($user['_id']) != $id) {
                AdminUserModel::deleteById($id);
                $this->response('', array('success' => 1));
            }
        }
        $this->response('', array('success' => 0));
    }

    function index($f3) {
        $this->checkPermission('adminuser.index');
        $user = AdminUserHelper::get_current_user();
        $f3->set('user', $user);
        $this->setTitle('Danh sách tài khoản');
        $this->setViewName('adminuser/index');

        $AdminUserHelper = new AdminUserHelper();

        $filter_active = $_REQUEST['filter']['active'];
        if ($filter_active === null) {
            $filter_active = -1;
        }
        $f3->set('filter_active', $filter_active);

        $page = $_REQUEST['page'];
        if (empty($page)) {
            $page = 1;
        }
        $f3->set('page', $page);

        $filter_sort = $_REQUEST['filter_sort'];
        if (!isset($filter_sort) || empty($filter_sort)) {
            $filter_sort = array('create_at' => -1);
        }
        $f3->set('filter_sort', $filter_sort);
        
        $filter_sort_page = '';
        foreach ($filter_sort as $cname => $cvalue) {
            if ($filter_sort_page != '') {
                $filter_sort_page .= '&';
            }
            $filter_sort_page .= 'filter_sort[' . $cname . ']=' . $cvalue;
        }
        $f3->set('filter_sort_page', $filter_sort_page);
        
        $sort = array();
        foreach ($filter_sort as $cname => $cvalue) {
            $sort[$cname] = intval($cvalue);
        }
        
        $query = array();
        if ($filter_active != -1) {
            $query['active'] = intval($filter_active);
        }
        if (!$AdminUserHelper->is_admin()) {
            $query['partner'] = $user['partner'];
        }

        $limit = 20;
        $offset = ($page - 1) * $limit;
        $f3->set('offset', $offset);

        $total_page = AdminUserModel::get_count_list($query);
        $f3->set('total_page', ceil($total_page / $limit));

        $list = AdminUserModel::get_list($limit, $offset, $query, $sort);
        $f3->set('list', $list);
    }

    function saveUser($f3) {
        $this->checkPermission('adminuser.create');

        $salt_password = uniqid('pwd_');

        $username = $_POST['username'];
        $password = $_POST['password'];
        $password = md5($salt_password . $password . AdminUserHelper::CRYPT_PASSWORD);
        $email = $_POST['email'];
        $fullname = $_POST['fullname'];
        $role = $_POST['role'];
        $active = intval($_POST['active']);
        $partner = $_POST['partner'];
        $permissions = array();

        $AdminUserHelper = new AdminUserHelper();
        if (isset($_POST['permissions']) && is_array($_POST['permissions'])) {
            foreach ($_POST['permissions'] as $permission) {
                if (!$AdminUserHelper->is_admin()) {
                    $arr = explode('.', $permission);
                    if ($arr[0] == 'adminuser') {
                        continue;
                    }
                }

                if ($AdminUserHelper->has_permission($permission)) {
                    $permissions[] = $permission;
                }
            }
        }

        if ($role == AdminUserHelper::ROLE_ADMIN && !$AdminUserHelper->is_admin()) {
            $role = AdminUserHelper::ROLE_CUSTOM;
        }
        if (!$AdminUserHelper->is_admin()) {
            $user_current = AdminUserHelper::get_current_user();
            $partner = $user_current['partner'];
        }

        $user = AdminUserModel::getByUsername($username);
        if (!$user) {
            $user = AdminUserModel::getByEmail($email);
            if (!$user) {
                $user = array(
                    'username' => $username,
                    'password' => $password,
                    'salt_password' => $salt_password,
                    'email' => $email,
                    'fullname' => $fullname,
                    'role' => $role,
                    'partner' => $partner,
                    'active' => $active,
                    'permissions' => $permissions
                );

                AdminUserModel::save($user);
                $this->redirect('user', 'Thêm mới thành công');
            } else {
                $this->redirect('create-user', 'Trùng email', 'error');
            }
        } else {
            $this->redirect('create-user', 'Trùng tên tài khoản', 'error');
        }
    }

    function create($f3) {
        $this->checkPermission('adminuser.create');
        $user = AdminUserHelper::get_current_user();
        $f3->set('user', $user);
        $this->setTitle('Tạo tài khoản');
        $this->setViewName('adminuser/create');
    }

    function saveInfo($f3) {
        $password = $_POST['password'];
        $fullname = $_POST['fullname'];
        $password_new = $_POST['password_new'];
        $tabid = $_POST['tabid'];

        $user = AdminUserHelper::get_current_user();
        $user = AdminUserModel::getById(strval($user['_id']));

        if (!isset($user['salt_password']) || empty($user['salt_password'])) {
            $user['salt_password'] = uniqid('pwd_');
        }
        
        $password_auth = md5($user['salt_password'] . $password . AdminUserHelper::CRYPT_PASSWORD);
        if ($user['password'] == $password_auth || empty($password_new)) {
            $user['fullname'] = $fullname;
            if (!empty($password_new)) {
                $user['password'] = md5($user['salt_password'] . $password_new . AdminUserHelper::CRYPT_PASSWORD);
            }
            AdminUserModel::save($user);
            AdminUserHelper::refresh_current_user_info($user);
            $this->redirect('profile#' . $tabid, 'Thay đổi thành công.');
        } else {
            $this->redirect('profile#' . $tabid, 'Sai mật khẩu cũ.', 'error');
        }
    }

    function profile($f3) {
        $user = AdminUserHelper::get_current_user();
        $f3->set('user', $user);
        $this->setTitle('Thông tin người dùng');
        $this->setViewName('adminuser/profile');
    }

    function logout($f3) {
        session_destroy();
        AdminUserHelper::refresh_current_user_info(null);
        $redirect = $_REQUEST['redirect'];
        $this->redirect('login' . (isset($redirect) && !empty($redirect) ? '?redirect=' . (urlencode(urldecode($redirect))) : ''), 'Bạn đã đăng xuất.');
    }

    function login($f3) {
//    	echo '<pre>';
//    	print_r($_SERVER);
//		print_r($_SESSION);
//		die;
        $user = AdminUserHelper::get_current_user();
        $redirect = $_REQUEST['redirect'];
        if (isset($user)) {
            $this->redirect(isset($redirect) && !empty($redirect) ? $redirect : 'index');
        }
        $this->setTitle('Login');
        $this->setLayout('login');
    }

    function adminAuth($f3) {
        $user = AdminUserHelper::get_current_user();
        $redirect = $_REQUEST['redirect'];
        if (isset($user)) {
            $this->redirect(isset($redirect) && !empty($redirect) ? $redirect : 'index');
        }

        $username = $_POST['username'];
        $password = $_POST['password'];
        $remember_me = $_POST['remember_me'];

        $salt_password = uniqid('pwd_');

        $user = AdminUserModel::getByUsername($username);
		$password_auth = md5($user['salt_password'] . $password . AdminUserHelper::CRYPT_PASSWORD);
        if (isset($user)) {
            $password_auth = md5($user['salt_password'] . $password . AdminUserHelper::CRYPT_PASSWORD);
            if ($user['password'] == $password_auth) {
                if ($user['active'] == 1) {
                    AdminUserHelper::refresh_current_user_info($user);
                    if ($remember_me == 'on') {
                        //no code
                    }
                    $user['login_at'] = date('Y-m-d H:i:s');

                    $AdminUserHelper = new AdminUserHelper();

                    if ($AdminUserHelper->is_admin()) {
                        $_SESSION['partner'] = null;
                    } else {
                        $_SESSION['partner'] = $user['partner'];
                    }

                    AdminUserModel::save($user);
                    $this->redirect(isset($redirect) && !empty($redirect) ? $redirect : 'index');
                } else {
                    $this->redirect('login' . (isset($redirect) && !empty($redirect) ? '?redirect=' . (urlencode(urldecode($redirect))) : ''), 'Tài khoản bị khóa.', 'danger');
                }
            } else {
                $this->redirect('login' . (isset($redirect) && !empty($redirect) ? '?redirect=' . (urlencode(urldecode($redirect))) : ''), 'Sai tài khoản hoặc mật khẩu.', 'danger');
            }
        } else {
            $this->redirect('login' . (isset($redirect) && !empty($redirect) ? '?redirect=' . (urlencode(urldecode($redirect))) : ''), 'Sai tài khoản hoặc mật khẩu.', 'danger');
        }
    }

    private function response($message, $data = array()) {
        header('Content-Type: application/json');
        echo json_encode(array('message' => $message, 'data' => $data));
        die;
    }

}
