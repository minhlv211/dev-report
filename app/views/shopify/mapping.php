<form class="form-horizontal" method="post" action="shopify-mapping" id="edit-app">
	<div class="form-group">
		<label class="col-sm-2 control-label">Product ID</label>
		<div class="col-sm-4">
			<?php echo $product_id;?>
			<input type="hidden" name="product_id" value="<?php echo $product_id;?>" />

		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Product Name</label>
		<div class="col-sm-4">
			<?php echo $productInfo['title'];?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Product Ali</label>
		<div class="col-sm-4">
			<select class="form-control" name="ali_product_id" id="ali_product_id">
                <option value="">Select a Product</option>
				<?php
				foreach ($productList as $_product) {
					$amount = UtilHelper::number_format($_product['amount'], 2, '-', '$');
					$selected = '';
					if ($_product['product_id'] == $productInfo['product_ali']['product_id']) {
						$selected = ' selected="selected" ';
					}
					echo '<option value="'.$_product['product_id'].'" '.$selected.'>'.$_product['name'] . ' (' . $amount . ')</option>';
				}
				?>
			</select>
		</div>
	</div>


	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-4">
			<button class="btn btn-primary">Lưu lại</button>
			<a href="shopify-products" class="btn btn-default">Quay lại</a>
		</div>
	</div>
</form>

<?php if(count($orderList) > 0) { ?>
    <table class="table table-striped table-advance table-hover table-bordered">
    <tr>
        <th width="10%">Created Time</th>
        <th width="5%">Amount</th>
        <th width="5%">Payment Status</th>
        <th width="35%">Products</th>
        <th width="45%">Ali Order</th>
    </tr>
    <?php
    foreach ($orderList as $_order) {
        echo '<tr>';
		echo '<td>' . $_order['created_at'] . '</td>';
        echo '<td class="number">' . UtilHelper::number_format($_order['total_line_items_price'], 2, '-', '$') . '</td>';
		echo '<td>' . $_order['financial_status'] . '</td>';
		echo '<td>' . $_order['products'] . '</td>';
		echo '<td>' . $_order['ali_order'] . '</td>';

		echo '</tr>';
    }
    ?>
</table>
<?php } ?>

<script type="text/javascript">
    (function () {
        $('#edit-app').validate();
        $('#ali_product_id').select2({theme: "bootstrap"});
    })();
</script>