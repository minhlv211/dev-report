<form class="form-inline filter-form" action="shopify-orders?" method="get">
    <div class="form-group">
        <input name="filter[name]" value="<?php echo $filter_name; ?>"  class="form-control"/>
    </div>

	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
	</div>
</form>
<div style="overflow: auto">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="shopify_product_list"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
			<th data-field="no" data-align="right">No</th>
			<th data-field="product_id" data-visible="false">Product ID</th>
            <th data-field="created_at" data-visible="false">Created At</th>
            <th data-field="title"  data-sortable="true">Product Name</th>
			<th data-field="vendor"  data-visible="false" >Vendor</th>
            <th data-field="price"  data-visible="false" data-sortable="true">Price</th>
			<th data-field="product_ali" data-visible="false">Product Ali</th>
            <th data-field="options" data-visible="false">Options</th>
            <th data-field="amount" data-visible="false" data-class="number" data-sortable="true">Total Amount</th>
            <th data-field="real_price" data-visible="false" data-class="number" data-sortable="true">Real Price</th>
            <th data-field="quantity" data-visible="false" data-class="number" data-sortable="true">Total Order</th>
            <th data-field="discount" data-visible="false" data-class="number" data-sortable="true">Total Discount</th>
            <th data-field="sub_total" data-visible="false" data-class="number" data-sortable="true">Sub Total</th>
            <th data-field="discount_percent" data-visible="false" data-class="number" data-sortable="true">Discount Percent</th>
            <th data-field="shipping_cost" data-visible="false">Shiping Cost</th>
            <th data-field="action" data-visible="true">Action</th>
		</tr>
		</thead>
	</table>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage",
            customSort: function (sortName, sortOrder) {
                if (!sortName || !sortOrder) {
                    return;
                }
                this.data.sort(function(a, b) {
                    var x = a[sortName];
                    var y = b[sortName];
                    if (sortName != 'title') {
                        x = x.replace(',','');
                        y = y.replace(',','');
                        x = parseFloat(x);
                        y = parseFloat(y);
                        if (isNaN(x)) {
                            x = 0;
                        }
                        if (isNaN(y)) {
                            y = 0;
                        }
                    }
                    if (sortOrder == 'desc') {
                        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
                    } else {
                        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                    }

                });
        }});

        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
        });


    });
</script>