<style type="text/css">
    .timeline {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
        margin: 0;
    }

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        right: 25px;
        margin-left: -1.5px;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

    .timeline .timeline-body {
        padding: 10px;
        margin-left: 0px;
        margin: 0;
    }
    .timeline .timeline-body p {
        margin: 0;
    }
    .timeline p {
        margin: 10px 0;
        line-height: 1.5em;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li > .timeline-panel {
        width: 85%;
        float: left;
        border: 1px solid #d4d4d4;
        border-radius: 2px;
        padding: 10px;
        position: relative;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline > li > .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid #ccc;
        border-right: 0 solid #ccc;
        border-bottom: 15px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-panel:after {
        position: absolute;
        top: 27px;
        right: -14px;
        display: inline-block;
        border-top: 14px solid transparent;
        border-left: 14px solid #fff;
        border-right: 0 solid #fff;
        border-bottom: 14px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-badge {
        color: #fff;
        width: 50px;
        height: 50px;
        line-height: 50px;
        font-size: 1.4em;
        text-align: center;
        position: absolute;
        top: 16px;
        right: 0%;
        margin-left: -25px;
        background-color: #999999;
        z-index: 100;
        padding-right: 0px;
        border-top-right-radius: 50% !important;
        border-top-left-radius: 50% !important;;
        border-bottom-right-radius: 50% !important;;
        border-bottom-left-radius: 50% !important;;
    }

    .timeline > li.timeline-inverted > .timeline-panel {
        float: right;
    }

    .timeline > li.timeline-inverted > .timeline-panel:before {
        border-left-width: 0;
        border-right-width: 15px;
        left: -15px;
        right: auto;
    }

    .timeline > li.timeline-inverted > .timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }

    .timeline-badge.primary {
        background-color: #2e6da4 !important;
    }

    .timeline-badge.success {
        background-color: #3f903f !important;
    }

    .timeline-badge.warning {
        background-color: #f0ad4e !important;
    }

    .timeline-badge.danger {
        background-color: #d9534f !important;
    }

    .timeline-badge.info {
        background-color: #5bc0de !important;
    }

    .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .timeline-body > p,
    .timeline-body > ul {
        margin-bottom: 0;
    }

    .timeline-body > p + p {
        margin-top: 5px;
    }

    @media (max-width: 767px) {
        ul.timeline:before {
            left: 40px;
        }

        ul.timeline > li > .timeline-panel {
            width: calc(100% - 90px);
            width: -moz-calc(100% - 90px);
            width: -webkit-calc(100% - 90px);
        }

        ul.timeline > li > .timeline-badge {
            left: 15px;
            margin-left: 0;
            top: 16px;
        }

        ul.timeline > li > .timeline-panel {
            float: right;
        }

        ul.timeline > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
    }
</style>
<div style="margin-bottom: 10px;">
    <a href="javascript: window.history.back();" class="btn btn-default"><i class="fa fa-undo"></i> Back to list</a>
</div>

<div class="rows">
    <div class="col-md-4">

        <ul class="timeline">
            <?php
            if(count($timelineInfo) > 0) {
                foreach ($timelineInfo as $_timeline) {
                    echo '<li>';
                    echo '<div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>';
                    echo '<div class="timeline-panel">';
                    echo '<div class="timeline-heading">';
                    echo '<h4 class="timeline-title">'.$_timeline['title'].'</h4>';
                    echo '<p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '.$_timeline['time'].'</small></p>';
                    echo '</div>';
                    echo '<div class="timeline-body"><p>';
                    echo $_timeline['body'];
                    echo '</p></div>';
                    echo '</div>';
					echo '</li>';
                }
            }
            ?>
        </ul>
    </div>
    <div class="col-md-8">
        <h3>Order Detail</h3>
        <table class="table table-striped table-advance table-hover table-bordered">
            <tr>
                <th>Order ID / Number</th>
                <td><a target="shopify" href="https://mythofeastern.myshopify.com/admin/orders/<?php echo $orderInfo['order_id']; ?>"><?php echo $orderInfo['order_id']; ?> / #<?php echo $orderInfo['number'] + 1000; ?> </a></td>
            </tr>
            <tr>
                <th>Created At</th>
                <td><?php echo date('Y-m-d H:i:s', strtotime($orderInfo['created_at'])); ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?php echo $orderInfo['name']; ?></td>
            </tr>
            <tr>
                <th>Contact</th>
                <td><?php echo $orderInfo['email']; ?> / <?php echo $orderInfo['phone']; ?></td>
            </tr>
            <tr>
                <th>Payment / Fulfilment</th>
                <td>
					<?php echo $orderInfo['financial_status']; ?>
                    /
					<?php echo $orderInfo['fulfillment_status']; ?>
                </td>
            </tr>
            <tr>
                <th>Address</th>
                <td>
					<?php echo $orderInfo['flag'];?>
                    &nbsp; <?php echo $orderInfo['address'];?>
                </td>
            </tr>
            <tr>
                <th>Total Amount</th>
                <td><?php echo number_format($orderInfo['total_price'], 2); ?>$</td>
            </tr>
        </table>

        <table class="table table-striped table-advance table-hover table-bordered">
            <tr>
                <th>NO</th>
                <th>Name</th>
                <th class="number">Price</th>
                <th class="number">Quantity</th>
                <th>Tracking Number</th>
                <th>Tracking Status</th>
            </tr>
            <?php
            $no = 0;
            foreach ($orderInfo['items'] as $item) {
                echo '<tr>';
				echo '<td>' . ++$no . '</td>';
				echo '<td>' . $item['title'] . '</td>';
				echo '<td class="number">' . number_format($item['price'], 2) . '$</td>';
				echo '<td class="number">' . $item['quantity'] . '</td>';
				echo '<td><a href="javascript: showDialogTracking(\''.$item['tracking_number'].'\');">' . $item['tracking_number'] . '</a></td>';
				echo '<td style="text-align: center">' . TrackingNumberHelper::getStatusHtml($item['tracking_status'])  . '</td>';
				echo '</tr>';
            }
            ?>

        </table>
		<?php
		if (is_array($trackingList) && count($trackingList) > 0) {
			echo '<h3>Tracking Number</h3>';
			echo '<table class="table table-striped table-advance table-hover table-bordered">';
			echo '<tr>';
			echo '<th>Tracking Number</th>';
			echo '<th>Carrier Name</th>';
			echo '<th>Status</th>';
			echo '<th>From Date</th>';
			echo '<th>To Date</th>';
			echo '<th>Transit Date</th>';
			echo '<th>Other Track</th>';
			echo '</tr>';
            foreach ($trackingList as $_track) {
				echo '<tr>';
				echo '<td><a href="javascript: showDialogTracking(\''.$_track['tracking_number'].'\');">';
				echo $_track['tracking_number'].'</a></td>';
				echo '<td>'.$_track['carrier_name'].'</td>';
				echo '<td>'.TrackingNumberHelper::getStatusHtml($_track['status']).'</td>';
				echo '<td>'.$_track['start_date'].'</td>';
				if(empty($_track['end_date'])) {
					echo '<td>-</td>';
                } else {
					echo '<td>'.$_track['end_date'].'</td>';
                }
				echo '<td>'.$_track['itemTimeLength'].'</td>';
				echo '<td>';
//				echo '<a target="track_more" class="btn btn-xs btn-default" href="https://track24.net/?code='.$_track['tracking_number'].'"><i class="fa fa-info"></i> Track24</a> |';
//				echo '<a target="track_more" class="btn btn-xs btn-default" href="https://www.17track.net/en/track?nums='.$_track['tracking_number'].'"><i class="fa fa-info"></i> Track17</a> |';
				echo '<a target="track_more" class="btn btn-xs btn-default" href="http://global.cainiao.com/detail.htm?lang=en&mailNoList='.$_track['tracking_number'].'"><i class="fa fa-info"></i> Cainiao</a> |';
				echo '<a target="track_more" class="btn btn-xs btn-default" href="tracking-detail?tracking_number='.$_track['tracking_number'].'"><i class="fa fa-info"></i> Detail</a> ';

				echo '</td>';
				echo '</tr>';

            }
			echo '</table>';
		}

        if ($aliOrderList) {
			echo '<h3>Ali Order</h3>';

			foreach ($aliOrderList as $_order) {
			    echo '<table class="table table-striped table-advance table-hover table-bordered">';
                echo '<tr>';
                echo '<th>OrderId</th>';
				echo '<td><a target="aliexpress" href="https://trade.aliexpress.com/order_detail.htm?orderId='.$_order['order_id'].'">'.$_order['order_id'].'</a></td>';
				echo '<th>OrderAt</th>';
				echo '<td>'.$_order['order_at'].'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<th>Tracking Number</th>';
				echo '<td><a href="javascript: showDialogTracking(\''.$_order['tracking_no'].'\');">' . $_order['tracking_no'] . '</a></td>';
				echo '<th>Amount</th>';
				echo '<td class="number">'.number_format($_order['product_price'], 2).'$</td>';
				echo '</tr>';
			    echo '</table>';
                $no = 0;
				echo '<table class="table table-striped table-advance table-hover table-bordered">';
				echo '<tr>';
				echo '<td>No</td>';
				echo '<td>Name</td>';
				echo '<td>Price</td>';
				echo '<td>Quantity</td>';
//				echo '<td>Status</td>';
				echo '</tr>';
				foreach ($_order['product_list'] as $item) {
					echo '<tr>';
					echo '<td>' . ++$no . '</td>';
					echo '<td>' . $item['name'] . '</td>';
					echo '<td class="number">' . number_format($item['price'], 2) . '$</td>';
					echo '<td class="number">' . $item['quantity'] . '</td>';
//					echo '<td>' . $item['status'] . '</td>';
					echo '</tr>';
				}
				echo '</table>';

            }
        }
        ?>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tracking Detail</h4>
            </div>
            <div class="modal-body" id="modal-body" style="max-height: 400px; overflow: auto">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function showDialogTracking(tracking_number) {
        var url = 'http://ds.ecomobi.com/tracking-detail?tracking_number='+tracking_number+'&ajax=1';
        $.ajax({
            url: url,
            context: document.body
        }).done(function(data) {
//            console.log(data);
            $('#modal-body').html(data);
            $('#myModal').modal('show');
        });


    }
</script>

