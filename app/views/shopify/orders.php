<form class="form-inline filter-form" action="shopify-orders?" method="get">
	<div class="form-group" style="position: relative">
		<input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer" name="filter[date_range]"
			   value="<?php echo $start_date . '-' . $end_date ?>">
		<i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
	</div>


	<div class="form-group">
		<select class="form-control auto_submit" name="filter[country]" id="filter-country">
			<option value="">All Country</option>
			<?php
			if(count($reportCountry) > 0) {
				foreach ($reportCountry as $_country) {
					$selected = '';
					if ($_country['id'].'' == $filter_country) {
						$selected = ' selected="selected" ';
					}
					echo '<option value="'.$_country['id'].'" '.$selected.'>'.$_country['name'].' (' .$_country['amount'].')</option>';
				}
			}
			?>
		</select>
	</div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[product]" id="filter-product">
            <option value="">All Product</option>
			<?php
			if(count($reportProduct) > 0) {
				foreach ($reportProduct as $_product) {
					$selected = '';
					if ($_product['id'].'' == $filter_product) {
						$selected = ' selected="selected" ';
					}
					echo '<option value="'.$_product['id'].'" '.$selected.'>'.$_product['name'].' (' .$_product['amount'].')</option>';
				}
			}
			?>
        </select>
    </div>


    <div class="form-group">
        <select class="form-control auto_submit" name="filter[status]">
            <option value="">All Payment Status</option>
            <option value="paid" <?php echo $filter_status == 'paid'?' selected="selected" ':''; ?> >Paid</option>
            <option value="pending" <?php echo $filter_status == 'pending'?' selected="selected" ':''; ?> >Pending</option>
            <option value="refunded" <?php echo $filter_status == 'refunded'?' selected="selected" ':''; ?> >Refunded</option>
            <option value="partially_refunded" <?php echo $filter_status == 'partially_refunded'?' selected="selected" ':''; ?> >Partially Refunded</option>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[ali_status]">
            <option value="">All Ali Status</option>
            <option value="1" <?php echo $filter_ali_status == '1'?' selected="selected" ':''; ?> >Already Order</option>
            <option value="2" <?php echo $filter_ali_status == '2'?' selected="selected" ':''; ?> >Not Same Item</option>
            <option value="3" <?php echo $filter_ali_status == '3'?' selected="selected" ':''; ?> >Not Order Yet</option>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[tracking_status]">
            <option value="">All Tracking Status</option>
            <option value="delivered" <?php echo $filter_tracking_status == 'delivered'?' selected="selected" ':''; ?> >Delivered</option>
            <option value="transit" <?php echo $filter_tracking_status == 'transit'?' selected="selected" ':''; ?> >Transit</option>
            <option value="partial_delivered" <?php echo $filter_tracking_status == 'partial_delivered'?' selected="selected" ':''; ?> >Partial Delivered</option>
            <option value="exception" <?php echo $filter_tracking_status == 'exception'?' selected="selected" ':''; ?> >Exception</option>
            <option value="pending" <?php echo $filter_tracking_status == 'pending'?' selected="selected" ':''; ?> >Pending</option>
        </select>
    </div>


    <div class="form-group">
        <input name="filter[name]" value="<?php echo $filter_name; ?>"  class="form-control" placeholder="Name"/>
    </div>

	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
	</div>
</form>
<div style="overflow: auto">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="shopify_order_list"
		   data-side-pagination="server"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
			<th data-field="no" data-align="right">No</th>
            <th data-field="number" data-visible="false"  data-sortable="true">Order Number</th>
			<th data-field="order_id" data-visible="false"  data-sortable="true">Order ID</th>
            <th data-field="name"  data-sortable="true">Customer Name</th>
			<th data-field="email"  data-visible="false">Email</th>
            <th data-field="phone_new"  data-visible="false">Phone</th>
			<th data-field="created_at" data-visible="true"  data-sortable="true">Created At</th>
			<th data-field="currency" data-visible="false">Currency</th>
			<th data-field="total_price"  data-class="number"  data-sortable="true">Amount</th>
			<th data-field="total_discounts"  data-class="number"  data-sortable="true">Discounts</th>
            <th data-field="discount_percent"  data-class="number"  data-visible="false">Discount Percent</th>

            <th data-field="ali_order_price"  data-class="number"  data-visible="true">Ali Amount</th>
            <th data-field="ali_product_price"  data-class="number"  data-visible="false">Ali Product Amount</th>
            <th data-field="ali_shipping_price"  data-class="number"  data-visible="false">Ali Shipping Amount</th>

			<th data-field="total_line_items_price" data-visible="false"  data-class="number"  data-sortable="true">Sub Total</th>
			<th data-field="financial_status">Payment Status</th>
            <th data-field="tracking_status">Tracking Status</th>
            <th data-field="fulfillment_status" data-visible="false">Fulfillment</th>
            <th data-field="ali_status" data-visible="false">Ali Status</th>

            <th data-field="user_agent" data-visible="false">User Agent</th>
			<th data-field="referring_site" data-visible="false">Referring</th>
			<th data-field="address1" data-visible="false">Address</th>
			<th data-field="country"  data-sortable="true">Country</th>
			<th data-field="browser_ip" data-visible="false">IP</th>
            <th data-field="browser_width" data-visible="false">Browser Width</th>
            <th data-field="browser_height" data-visible="false">Browser Height</th>

            <th data-field="products" data-visible="false">Products</th>
            <th data-field="ali_order"  data-visible="false">Ali Order Detail</th>
            <th data-field="detail_order"  data-visible="true">Detail Order</th>

		</tr>
		</thead>
	</table>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#filter-country').select2({theme: "bootstrap"});
        $('#filter-product').select2({theme: "bootstrap"});
        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage"});

        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });


    });
</script>