<form class="form-inline filter-form" action="tracking-list?" method="get">
	<div class="form-group" style="position: relative">
		<input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer" name="filter[date_range]"
			   value="<?php echo $start_date . '-' . $end_date ?>">
		<i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
	</div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[status]" id="filter-status">
            <option value="">All Status</option>
            <?php
            foreach ($statusList  as $_stat) {
                $_item = $_stat['value'];
                $undelivered += ($_item == 'Pending' || $_item == 'Delivered') ? 0 : $_stat['count'] ;
                echo '<option value="'.$_item.'" '.($filter_status == $_item?' selected="selected" ':'').'>'.$_item . ' (' . number_format($_stat['count']) .')</option>';
            }
            ?>
            <?php if (count($statusList) > 0 && $undelivered > 0) { ?>
                <option value="-1" <?php if ($filter_undelivered) { echo 'selected';} ?>>Undelivered (<?php echo $undelivered; ?>)</option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[country]" id="filter-country">
            <option value="">All Country</option>
			<?php
			foreach ($countryList  as $_stat) {
				$_item = $_stat['value'];
				echo '<option value="'.$_item.'" '.($filter_country == $_item && !empty($_item)?' selected="selected" ':'').'>'. CountryHelper::convertISO2ToName($_item) . ' (' . number_format($_stat['count']) .')</option>';
			}
			?>
        </select>
    </div>


    <div class="form-group">
        <select class="form-control auto_submit" name="filter[store]" id="filter-store">
            <option value="">All Store</option>
			<?php
			foreach ($storeList  as $_stat) {
			    $_item = ($_stat['value'] =='') ? '-1' : $_stat['value'] ;
			    $label = ($_item=='-1') ? "Empty" : $_item ;
				echo '<option value="'.$_item.'" '.($filter_store == $_item && !empty($_item) ?' selected="selected" ':'').'>'.$label. ' (' . number_format($_stat['count']) .')</option>';
			}
			?>
        </select>
    </div>


    <div class="form-group">
        <select class="form-control auto_submit" name="filter[carrier]" id="filter-carrier">
            <option value="">All Couriers</option>
			<?php
			foreach ($carrierList  as $_stat) {
				$_item = $_stat['value'];
				echo '<option value="'.$_item.'" '.($filter_carrier == $_item?' selected="selected" ':'').'>'.CarrierHelper::code2Name($_item)  . ' (' . number_format($_stat['count']) .')</option>';
			}
			?>
        </select>
    </div>

    <div class="form-group">
        <input name="filter[name]" value="<?php echo $filter_name; ?>"  class="form-control" placeholder="Tracking Number"/>
    </div>

	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
		<a href="/tracking-list" class="btn btn-default">Reset</a>
	</div>
</form>
<div class="table-container">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="tracking_number_list"
		   data-side-pagination="server"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
			<th data-field="no" data-align="right">No</th>
            <th data-field="tracking_number">Tracking Number</th>
            <th data-field="order_time" data-sortable="true" data-class="nowrap">Order Time</th>
            <th data-field="ali_order_time" data-class="nowrap">Ali Order</th>
            <th data-field="start_date" data-sortable="true">Start Date</th>
            <th data-field="end_date" data-sortable="true">Last Update</th>
            <th data-field="origin_export_date" data-visible="false" data-sortable="true">Export Date</th>
            <th data-field="destination_import_date" data-visible="false" data-sortable="true">Import Date</th>
            <th data-field="delivered_date" data-visible="false" data-sortable="true">Delivered Date</th>
            <th data-field="title" data-visible="false">Order Number</th>
            <th data-field="ali_order_id" data-visible="false">Ali Order ID</th>
            <th data-field="order_id" data-visible="false">Order ID</th>
            <th data-field="store_name" data-class="nowrap">Store Name</th>
            <th data-field="slug">Carrier Code</th>
			<th data-field="tag" data-class="nowrap">Status</th>
            <th data-field="country_name" data-class="nowrap">To Country</th>
            <th data-field="timeLineBrief" data-visible="false">Time line</th>

            <th data-field="view_detail">Detail</th>
		</tr>
		</thead>
	</table>
</div>

<!-- Change Status  Modal -->
<div id="change-status" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="change-status-form" class="modal-content" action="tracking-update" method="post">
            <input type="hidden" name="tracking_number" value="">
            <input type="hidden" name="redirect_url" value="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tracking number : <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="text-align: right;padding-top: 8px">Status</div>
                    <div class="col-md-6">
                        <select name="tracking_status" id="" class="form-control">
                            <option value=""> -- Auto -- </option>
                            <option value="InTransit"> InTransit </option>
                            <option value="Delivered"> Delivered </option>
                            <option value="OutForDelivery"> OutForDelivery </option>
                            <option value="Pending"> Pending </option>
                            <option value="AttemptFail"> AttemptFail </option>
                            <option value="Expired"> Expired </option>
                            <option value="Exception"> Exception </option>
                            <option value="InfoReceived"> InfoReceived </option>
                        </select>
                    </div>
                </div>
                <div class="row delivered_date_wp" style="display: none; margin-top: 15px">
                    <div class="col-md-6" style="text-align: right;padding-top: 8px">Delivered Date</div>
                    <div class="col-md-6">
                        <div class="" style="position: relative">
                            <input style="background: transparent; cursor: pointer" readonly required type="text"  id="delivered_date" class="form-control" name="tracking_delivered_date">
                            <i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: 10px; bottom: 10px;"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>

    </div>
</div>

<script type="text/javascript">
    (function () {
        $('#change-status-form').validate({
            rules: {}
        });
    })();
    $(document).ready(function () {

        $("#delivered_date").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('input[name=redirect_url]').val(window.location.href);
        $(document).on('click','.change_stt', function(e) {
           var _number = $(this).data('number');
           var _curS = $(this).data('status');
           var md =  $("#change-status");
           var _dlDate = $(this).data('date');
           md.find('select').val(_curS);
           console.log(_dlDate);
           if (_curS === 'Delivered') {
               $(".delivered_date_wp").show().find('input[name=tracking_delivered_date]').val(_dlDate);
           }else {
               $(".delivered_date_wp").hide().find('input[name=tracking_delivered_date]').val('');
           }


           md.find('input[name=tracking_number]').val(_number);
           md.find('.modal-title span').html(_number);
        });
        $("select[name=tracking_status]").on('change', function () {
            if ($(this).val() === 'Delivered') {
                $("#delivered_date").datepicker("setDate",new Date());
                $(".delivered_date_wp").show();
            }else {
                $(".delivered_date_wp").hide().find('input[name=tracking_delivered_date]').val('');
            }
        });


        $('#filter-country').select2({theme: "bootstrap"});
        $('#filter-product').select2({theme: "bootstrap"});
        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage"});

        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });

        $('#filter-country').select2({theme: "bootstrap"});
        $('#filter-product').select2({theme: "bootstrap"});
        $('#filter-store').select2({theme: "bootstrap"});
        $('#filter-carrier').select2({theme: "bootstrap"});
        $('#filter-status').select2({theme: "bootstrap"});


    });
</script>
<style type="text/css">
    .nowrap {
        white-space: nowrap;
        clear: both;
    }
    .nowrap:after {

    }
    .table-container {
        overflow: auto;
    }

</style>