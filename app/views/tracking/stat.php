<form class="form-inline filter-form" action="tracking-stat?" method="get">
	<div class="form-group" style="position: relative">
		<input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer" name="filter[date_range]"
			   value="<?php echo $start_date . '-' . $end_date ?>">
		<i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
	</div>


    <div class="form-group">
        <select class="form-control auto_submit" name="filter[status]">
            <option value="">All Status</option>
			<?php
			foreach ($statusList as $_stat) {
				echo '<option value="'.$_stat['value'].'" '.($filter_status == $_stat['value']?' selected="selected" ':'').'>'.$_stat['value'] . ' (' . UtilHelper::number_format($_stat['count']).' )</option>';
			}
			?>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[country]" id="filter-country">
            <option value="">All Country</option>
            <?php
            foreach ($countryList as $_stat) {

                if (empty($_stat['value'])) {
					$_stat['value'] = 'other';
					$country_name = 'Other';
                } else {
					$country_name = CountryHelper::convertISO2ToName($_stat['value']);
                }
                echo '<option value="'.$_stat['value'].'" '.($filter_country == $_stat['value']?' selected="selected" ':'').'>'
                    . $country_name . ' (' . UtilHelper::number_format($_stat['count']).' )</option>';
            }
            ?>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[store]">
            <option value="">All Store</option>
			<?php
			foreach ($storeList as $_stat) {
				echo '<option value="'.$_stat['value'].'" '.($filter_store == $_stat['value'] && !empty($_stat['value'])?' selected="selected" ':'').'>'.$_stat['value'] . ' (' . UtilHelper::number_format($_stat['count']).' )</option>';
			}
			?>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[carrier]">
            <option value="">All Couriers</option>
			<?php
			foreach ($carrierList as $_stat) {
				echo '<option value="'.$_stat['value'].'" '.($filter_carrier == $_stat['value'] && !empty($_stat['value'])?' selected="selected" ':'').'>'.
                    CarrierHelper::code2Name($_stat['value']) . ' (' . UtilHelper::number_format($_stat['count']).' )</option>';
			}
			?>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="view_by">
            <option value="country_code" <?php echo $view_by == 'country_code'?' selected="selected" ':''; ?>>Country</option>
            <option value="carrier_code" <?php echo $view_by == 'carrier_code'?' selected="selected" ':''; ?>>Carrier</option>
            <option value="store_name" <?php echo $view_by == 'store_name'?' selected="selected" ':''; ?>>Store</option>
            <option value="order_date" <?php echo $view_by == 'order_date'?' selected="selected" ':''; ?>>Date</option>
            <option value="month" <?php echo $view_by == 'month'?' selected="selected" ':''; ?>>Month</option>
            <option value="weekday" <?php echo $view_by == 'weekday'?' selected="selected" ':''; ?>>Day of Week</option>
        </select>
    </div>

	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
	</div>
</form>
<div style="overflow: auto; margin-top: 20px">
    <div style="padding: 10px; margin-bottom: 10px; border: 1px solid #CCC; line-height: 1.5em; background-color: #EEE">
        <div style="margin-bottom: 10px;"><strong>Total: <?php echo number_format($totalTrack) ?></strong> &nbsp; &nbsp; &nbsp; &nbsp; </div>
        <?php
        foreach ($statusResultList as $_stat) {
            echo '<img src="/assets/custom/status/'.$_stat['value'].'.svg" width="24" alt="'.$_stat['value'].'" title="'.$_stat['value'].'" />  ';
            echo $_stat['value'] . ': ' . UtilHelper::number_format($_stat['count']);
            echo ' (' . UtilHelper::number_format($_stat['count'] / $totalTrack * 100, 1, '-', '%') . ')';
            echo '&nbsp; &nbsp; &nbsp; &nbsp; ';
        }
        ?>
    </div>
	<table class="table table-striped table-advance table-hover table-bordered" >
		<thead>
            <tr>
                <th>Name</th>
                <th class="number">Count</th>
<!--                <th class="number">Rate</th>-->
                <th class="number">Eco Process</th>
                <th class="number">Vendor Process</th>
                <th class="number">Oversea</th>
                <th class="number">Shipping</th>
                <th class="number">Total</th>
                <th class="number">Delivered</th>
            </tr>
		</thead>
        <tbody>
        <?php
        foreach ($statList as $_stat) {
            echo '<tr>';
            echo '<td>' . $_stat['value'] . '</td>';
			echo '<td class="number">' . UtilHelper::number_format($_stat['count']) . '</td>';

			echo '<td class="number">';
            echo UtilHelper::number_format($_stat['eco_process_time_avg'], 0, '-', ' hours');
            echo '<br /><span style="color: #AAA; font-size: 10px;">' . UtilHelper::number_format($_stat['eco_process_time_count'], 0, '-', '') . '</span>';
            echo '</td>';

			echo '<td class="number">';
			echo UtilHelper::number_format($_stat['process_date_avg'], 1, '-', ' days');
			echo '<br /><span style="color: #AAA; font-size: 10px;">' . UtilHelper::number_format($_stat['process_date_count'], 0, '-', '') . '</span>';
			echo '</td>';

			echo '<td class="number">';
			echo UtilHelper::number_format($_stat['export_date_avg'], 1, '-', ' days');
			echo '<br /><span style="color: #AAA; font-size: 10px;">' . UtilHelper::number_format($_stat['export_date_count'], 0, '-', '') . '</span>';
			echo '</td>';

			echo '<td class="number">';
			echo UtilHelper::number_format($_stat['ali_deliver_date_avg'], 1, '-', ' days');
			echo '<br /><span style="color: #AAA; font-size: 10px;">' . UtilHelper::number_format($_stat['ali_deliver_date_count'], 0, '-', '') . '</span>';
			echo '</td>';

			echo '<td class="number">';
			echo UtilHelper::number_format($_stat['total_deliver_date_avg'], 1, '-', ' days');
			echo '<br /><span style="color: #AAA; font-size: 10px;">' . UtilHelper::number_format($_stat['total_deliver_date_count'], 0, '-', '') . '</span>';
			echo '</td>';

			echo '<td class="number">';
			echo UtilHelper::number_format($_stat['success'] / $_stat['count'] * 100, 1, '-', '%');
			echo '<br /><span style="color: #AAA; font-size: 10px;">' . UtilHelper::number_format($_stat['success'], 0, '-', '') . '</span>';
			echo '</td>';
			echo '</tr>';
        }
        ?>
        </tbody>
	</table>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#filter-country').select2({theme: "bootstrap"});
        $('#filter-product').select2({theme: "bootstrap"});

        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'All Time': [moment('2017-08-01'), moment()]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });


    });
</script>