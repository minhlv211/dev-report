<div style="margin-bottom: 10px;">
	<a href="javascript: window.history.back();" class="btn btn-default"><i class="fa fa-undo"></i> Back to list</a>
</div>


<table class="table table-striped table-advance table-hover table-bordered">
	<tr>
		<th>Tracking Number</th>
		<td><?php echo $trackingInfo['tracking_number']; ?></td>
	</tr>
	<tr>
		<th>Status</th>
		<td><?php echo TrackingNumberHelper::getStatusHtml($trackingInfo['tag']); ?></td>
	</tr>
	<tr>
		<th>Start Time</th>
		<td><?php echo $trackingInfo['start_date']; ?></td>
	</tr>
	<tr>
		<th>End Date</th>
		<td><?php echo $trackingInfo['end_date']; ?></td>
	</tr>
	<tr>
		<th>Transit Time</th>
		<td><?php echo $trackingInfo['delivery_time']; ?></td>
	</tr>
	<tr>
		<th>Carrier</th>
		<td>
			<?php
			if ($trackingInfo['slug']) {
				echo '<a href="'.$trackingInfo['carrier']['homepage'].'">';
				echo $trackingInfo['carrier']['name'];
				if ($trackingInfo['carrier']['other_name']) {
					echo ' ('.$trackingInfo['carrier']['other_name'] . ')';
                }
				echo ' - Code: '.$trackingInfo['carrier']['code'] ;
				echo '</a>';
			}
			?>
		</td>
	</tr>
	<tr>
		<th>To Country</th>
		<td><?php echo CountryHelper::convertISO3ToName($trackingInfo['destination_country_iso3']); ?></td>
	</tr>
</table>

	<?php
	if ($trackingInfo['checkpoint_source'] && count($trackingInfo['checkpoint_source']) > 0) {
		?>
			<table class="table table-striped table-advance table-hover table-bordered">
				<tr>
					<th>STT</th>
					<th>Time</th>
					<th>Status</th>
                    <th>Location</th>
					<th>Detail</th>
				</tr>
				<?php
				$no = 0;
				foreach ($trackingInfo['checkpoint_source'] as $_track) {
					echo '<tr>';
					echo '<td>' . ++$no . '</td>';
					echo '<td style="white-space: nowrap">' . $_track['checkpoint_time'] . '</td>';
					echo '<td>' . $_track['tag'] . '</td>';
					echo '<td>' . $_track['location'] . '</td>';
					echo '<td>' . $_track['message'] . '</td>';
					echo '</tr>';
				}
				?>
			</table>
		<?php
	}
	?>
	<?php
	if ($trackingInfo['checkpoint_destination'] && count($trackingInfo['checkpoint_destination']) > 0) {
		?>
			<?php
			if ($trackingInfo['destination_info']['carrier']) {
				echo '<div style="margin-bottom: 10px">';
				echo '<a href="'.$trackingInfo['destination_info']['carrier']['homepage'].'">';
				echo '<img style="height: 24px" src="'.$trackingInfo['destination_info']['carrier']['picture'].'" /> ';
				echo $trackingInfo['destination_info']['carrier']['name'];
				echo ' ('.$trackingInfo['destination_info']['carrier']['code'] . ')';
				echo '</a>';
				echo '</div>';
			}
			?>
			<table class="table table-striped table-advance table-hover table-bordered">
				<tr>
					<th>STT</th>
					<th>Time</th>
					<th>Status</th>
                    <th>Location</th>
					<th>Detail</th>
				</tr>
				<?php
				$no = 0;
				foreach ($trackingInfo['checkpoint_destination'] as $_track) {
					echo '<tr>';
					echo '<td>' . ++$no . '</td>';
					echo '<td style="white-space: nowrap">' . $_track['checkpoint_time'] . '</td>';
					echo '<td>' . $_track['tag'] . '</td>';
					echo '<td>' . $_track['location'] . '</td>';
					echo '<td>' . $_track['message'] . '</td>';
					echo '</tr>';
				}
				?>
			</table>
		<?php
	}
	?>
