<?php
    $AdminUserHelper = new AdminUserHelper();
?>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/advanced-datatable/media/css/jquery.dataTables.css">
<style>
    .inline-checkbox {
        display: inline-table;
        cursor: pointer;
    }
</style>
<form class="form-inline filter-form" role="form" action="" method="get" id="filter-form" style="margin-bottom: 15px; position: relative">
    <div class="form-group">
            <label>Active</label>
            <select class="form-control input-large auto_submit" name="filter[active]">
                    <option value="-1" <?php if ($filter_active == -1) echo ' selected="selected" '; ?>>Tất cả</option>
                    <?php
                    echo '<option value="1" '.($filter_active == 1 ? ' selected="selected" ':'').'>Mở</option>';
                    echo '<option value="0" '.($filter_active == 0 ? ' selected="selected" ':'').'>Khóa</option>';
                    ?>
            </select>
    </div>
    <button type="submit" class="btn btn-success" data-original-title="" title="">GO</button>
    <a href="user" class="btn btn-default">Reset</a>
</form>
<div style="margin-bottom: 10px; display: inline-block;">
    <strong style="margin-right: 10px;">Hiển thị trường:</strong>
    <label class="inline-checkbox">
            <input type="checkbox" name="stt" value="1" class="field_show"/> STT &nbsp;&nbsp;
    </label>
    <label class="inline-checkbox">
            <input type="checkbox" name="username" value="1" class="field_show"/> Tên tài khoản &nbsp;&nbsp;
    </label>
    <label class="inline-checkbox">
            <input type="checkbox" name="email" value="1" class="field_show"/> Email &nbsp;&nbsp;
    </label>
    <label class="inline-checkbox">
            <input type="checkbox" name="fullname" value="1" class="field_show"/> Họ và tên &nbsp;&nbsp;
    </label>
    <?php
        if ($AdminUserHelper->is_admin()) {
            echo '<label class="inline-checkbox"><input type="checkbox" name="partner" value="1" class="field_show"/> Partner &nbsp;&nbsp;</label>';
        }
    ?>
    <label class="inline-checkbox">
            <input type="checkbox" name="role" value="1" class="field_show"/> Quyền hạn &nbsp;&nbsp;
    </label>
    <label class="inline-checkbox">
            <input type="checkbox" name="active" value="1" class="field_show"/> Active &nbsp;&nbsp;
    </label>
    <label class="inline-checkbox">
            <input type="checkbox" name="login_at" value="1" class="field_show"/> Truy cập gần nhất &nbsp;&nbsp;
    </label>
</div>
<?php
    echo PagerHelper::generateHtml($page, $total_page, 'user?filter[active]=' . $filter_active . '&' . $filter_sort_page, 'pull-right only_desktop');
?>
<div class="portlet-body flip-scroll">
    <table class="table table-hover table-bordered table-striped table-condensed flip-content" id="table-report3">
        <thead class="flip-content">
            <th field_name="stt" field_display="1" style="text-align: right">STT</th>
            <th sort_field="username" field_name="username" field_display="1">Tên tài khoản</th>
            <th sort_field="email" field_name="email" field_display="1">Email</th>
            <th sort_field="fullname" field_name="fullname" field_display="1">Họ và tên</th>
            <?php
                if ($AdminUserHelper->is_admin()) {
                    echo '<th sort_field="partner" field_name="partner" field_display="1">Partner</th>';
                }
            ?>
            <th sort_field="role" field_name="role" field_display="1">Quyền hạn</th>
            <th sort_field="active" field_name="active" field_display="1" style="text-align: center">Active</th>
            <th sort_field="login_at" field_name="login_at" field_display="1">Truy cập gần nhất</th>
            <th style="text-align: center">Thao tác</th>
        </thead>
        <tbody>
            <?php
            $index = $offset;
            foreach ($list as $info) {
                $index++;
                echo '<tr id="'. $info['_id'] .'">';
                echo '<td field_name="stt" style="text-align: right">' . $index . '</td>';
                echo '<td field_name="username">'. $info['username'] .'</td>';
                echo '<td field_name="email">'. $info['email'] .'</td>';
                echo '<td field_name="fullname">'. $info['fullname'] .'</td>';
                if ($AdminUserHelper->is_admin()) {
                    echo '<td field_name="partner">'. $info['partner'] .'</td>';
                }
                echo '<td field_name="role">'. $info['role'] .'</td>';
                echo '<td field_name="active" style="text-align: center">'. ($info['active'] == 1 ? '<span class="badge badge-success">Mở</span>' : '<span class="badge badge-danger">Khóa</span>') .'</td>';
                echo '<td field_name="login_at">'. $info['login_at'] .'</td>';
                echo '<td style="text-align: center">';
                if ($AdminUserHelper->has_permission('adminuser.edit') && $user['username'] != $info['username']) {
                    echo '<a href="edit-user?id='. $info['_id'] .'" class="btn btn-info btn-xs">Sửa thông tin</a>';
                }
                echo ' | ';
                if ($AdminUserHelper->has_permission('adminuser.delete') && $user['username'] != $info['username']) {
                    echo '<a onclick="deleteUser(\'' . $info['_id'] . '\')" href="javascript:;" class="btn btn-danger btn-xs">Xóa</a>';
                }
                echo '</td>';

                echo '</tr>';
            }
            ?>
        </tbody>
    </table>
</div>
<?php
    echo PagerHelper::generateHtml($page, $total_page, 'user?filter[active]=' . $filter_active . '&' . $filter_sort_page);
?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="width: 400px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Xóa tài khoản</h4>
            </div>
            <div class="modal-body">
                Bạn có chắc muốn xóa tài khoản?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onClick="deleteUserExec();">Xóa</button>
            </div>
        </div>
    </div>
</div>
<!--Include datatable-->
<script src="assets/global/plugins/advanced-datatable/media/js/jquery.dataTables.min.js"></script>
<script src="assets/global/plugins/advanced-datatable/media/js/DT_bootstrap.js"></script>
<script src="assets/global/plugins/advanced-datatable/media/js/DT_numericSort.js"></script>
<script src="assets/custom/common.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        show_table_id = '#table-report3';
        update_show_info();
        
        $('.auto_submit').change(function() {
            $('#filter-form').submit();
        });
    });
    
    $(document).ready(function () {
            var filter_sort = JSON.parse('<?php echo json_encode($filter_sort) ?>');
            var filter_url = '<?php echo urlencode('user?filter[active]=' . $filter_active . '&page=' . $page) ?>';
            Object.keys(filter_sort).map(function(objectKey, index) {
                var value = filter_sort[objectKey];
                if (value == 1) {
                    $('th[sort_field="' + objectKey + '"]').addClass("sorting_asc").removeClass("sorting");
                } else {
                    $('th[sort_field="' + objectKey + '"]').addClass("sorting_desc").removeClass("sorting");
                }
            });
            $('th[sort_field]').addClass("sorting");
            $('th[sort_field]').click(function(e){
                var elm = $(this);
                var url = filter_url;
                var sort = elm.attr('sort_field');
                
                //sort one
                var value = filter_sort[sort];
                filter_sort = {};
                filter_sort[sort] = value;
                //end
                
                filter_sort[sort] = filter_sort[sort] == undefined ? 1 : (filter_sort[sort] == 1 ? -1 : 1);
                
                Object.keys(filter_sort).map(function(objectKey, index) {
                    var value = filter_sort[objectKey];
                    url += '&filter_sort[' + objectKey + ']=' + value;
                });
                
                window.location = decodeURIComponent(url);
            });
        });
</script>
<script>
    var idUser = '';
    function deleteUserExec(){
        $("#myModal").modal("hide");
        $.ajax({
            url: 'delete-user',
            data: {
                id: idUser
            },
            method: 'post'
        }).done(function (response) {
            if (response.data.success == '1') {
                $('#' + idUser).fadeOut(500);
            }
        });
    }
    function deleteUser(id){
        idUser = id;
        $("#myModal").modal();
    }
</script>