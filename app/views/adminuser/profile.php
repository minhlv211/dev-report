<link href="assets/pages/css/profile-2.css" rel="stylesheet" type="text/css" />
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Account </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_1-1">
                                    <i class="fa fa-cog"></i> Information </a>
                                <span class="after"> </span>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_2-2">
                                    <i class="fa fa-lock"></i> Change Password </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <form id="tab1" role="form" action="save-info" method="post">
                                    <input type="hidden" name="tabid" value="tab_1-1" />
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input disabled type="text" value="<?php echo $user['email']; ?>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Họ và tên</label>
                                        <input type="text" name="fullname" value="<?php echo $user['fullname']; ?>" class="form-control" />
                                    </div>
                                    <div class="margiv-top-10">
                                        <button type="submit" class="btn green"> Save Changes </button>
                                        <a href="index" class="btn default"> Cancel </a>
                                    </div>
                                </form>
                            </div>
                            <div id="tab_2-2" class="tab-pane">
                                <form id="tab2" role="form" action="save-info" method="post">
                                    <input type="hidden" name="tabid" value="tab_2-2" />
                                    <input type="hidden" name="fullname" value="<?php echo $user['fullname']; ?>" />
                                    <div class="form-group">
                                        <label class="control-label">Current Password</label>
                                        <input type="password" name="password" class="form-control" /> </div>
                                    <div class="form-group">
                                        <label class="control-label">New Password</label>
                                        <input type="password" id="password_new" name="password_new" class="form-control" /> </div>
                                    <div class="form-group">
                                        <label class="control-label">Re-type New Password</label>
                                        <input type="password" name="password_again" class="form-control" /> </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green"> Change Password </button>
                                        <a href="index" class="btn default"> Cancel </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#tab2").validate({
        rules: {
            password: {
                required: true
            },
            password_new: {
                required: true
            },
            password_again: {
                required: true,
                equalTo: "#password_new"
            }
        },
        messages: {
            password: {
                required: "Mời bạn nhập mật khẩu cũ."
            },
            password_new: {
                required: "Mời bạn nhập mật khẩu mới."
            },
            password_again: {
                required: "Mời bạn nhập lại mật khẩu.",
                equalTo: "Mật khẩu không khớp."
            }
        }
    });
</script>