<?php
    $AdminUserHelper = new AdminUserHelper();
?>
<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<style>
    .permissions {
        height: 200px !important;
    }
    optgroup {
        font-weight: bold;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-form bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-dark sbold uppercase">Thông tin tài khoản</span>
                </div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" method="post" id="user-create" autocomplete="off">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tên đăng nhập</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="username"
                                       value=""
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Mật khẩu</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="password"
                                       value="123456"
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="email"
                                       value=""
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Họ và tên</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="fullname"
                                       value=""
                                       autocomplete="off">
                            </div>
                        </div>
                        <div style="display: <?php echo $AdminUserHelper->is_admin() ? 'block' : 'none' ?>" class="form-group">
                            <label class="col-md-3 control-label">Partner</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="partner"
                                       value="<?php echo !$AdminUserHelper->is_admin() ? $user['partner'] : '' ?>"
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Quyền hạn</label>
                            <div class="col-md-4">
                                <select onchange="showrole(this.value)" name="role" class="form-control">
                                    <option value="customer">Customer</option>
                                    <?php
                                        if ($AdminUserHelper->is_admin()) {
                                            echo '<option value="admin">Admin</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Active</label>
                            <div class="col-md-4">
                                <select name="active" class="form-control">
                                    <option value="1">Mở</option>
                                    <option value="0">Khóa</option>
                                </select>
                            </div>
                        </div>
                        <div id="showrole" class="showrole form-group">
                            <label class="col-md-3 control-label">Danh sách menu</label>
                            <div class="col-md-4">
                                <select multiple='multiple' name="permissions[]" class="permissions form-control">
                                    <?php
                                        foreach (AdminUserHelper::get_list_role() as $k => $v){
                                            if (!$AdminUserHelper->is_admin() && $k == 'adminuser') {
                                                continue;
                                            }
                                            if ($AdminUserHelper->is_admin() || $AdminUserHelper->has_permission_group($k)) {
                                                echo '<optgroup label="'. $v['label'] .'">';
                                                foreach ($v['list'] as $role => $value){
                                                    if ($AdminUserHelper->has_permission($k . '.' . $role)) {
                                                        echo '<option value="'. $k . '.' . $role .'">'. $value .'</option>';
                                                    }
                                                }
                                                echo '</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Lưu lại</button>
                                    <a href="user" class="btn btn-default">Quay lại</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    
    function showrole(val){
        if (val == 'admin') {
            $('#showrole').fadeOut();
        } else {
            $('#showrole').fadeIn();
        }
    }
    
    $("#user-create").validate({
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            fullname: {
                required: true
            },
            partner: {
                required: true
            }
        },
        messages: {
            username: {
                required: "Mời bạn nhập tên tài khoản."
            },
            password: {
                required: "Mời bạn nhập mật khẩu."
            },
            email: {
                required: "Mời bạn nhập email.",
                email: "Nhập sai định dạng email."
            },
            fullname: {
                required: "Mời bạn nhập họ và tên."
            },
            partner: {
                required: "Mời bạn nhập partner."
            }
        }
    });
</script>