<form class="form-inline filter-form" action="ali-orders?" method="get">
	<div class="form-group" style="position: relative">
		<input id="date-range" type="text" class="form-control" style="width: 100%; min-width: 200px;  cursor: pointer" name="filter[date_range]"
			   value="<?php echo $start_date . '-' . $end_date ?>">
		<i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
	</div>

	<div class="form-group">
		<select class="form-control auto_submit" name="filter[country]" id="filter-country">
			<option value="">All Country</option>
			<?php
				foreach ($country_list as $_k => $_country) {
					$selected = '';
					if ($_k == $filter_country) {
						$selected = ' selected="selected" ';
					}
					echo '<option value="'.$_k.'" '.$selected.'>'.$_country['value'].' (' .number_format($_country['count']).')</option>';
				}
			?>
		</select>
	</div>
    <div class="form-group">
        <select class="form-control auto_submit" name="filter[store]" id="filter-store">
            <option value="">All Store</option>
            <?php
                foreach ($store_list as $_k => $_item) {
                    echo '<option value="'.$_k.'" '.($filter_store == $_k?' selected="selected" ':'').' >'.$_item['value'].'('.number_format($_item['count']).')</option>';
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <input name="filter[name]" value="<?php echo $filter_name; ?>" class="form-control" placeholder="Customer Name" />
    </div>
    <div class="form-group">
        <input name="filter[tracking]" value="<?php echo $filter_tracking; ?>" class="form-control" placeholder="Tracking Number" />
    </div>


	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
        <a href="/ali-orders" class="btn btn-default">Reset</a>
	</div>
</form>
<div style="overflow: auto">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="ali_order_list"
		   data-side-pagination="server"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
			<th data-field="no" data-align="right">No</th>
			<th data-field="order_id" data-visible="false"  data-sortable="true">Order ID</th>
            <th data-field="name"  data-sortable="true">Customer Name</th>
            <th data-field="mobile"  data-visible="false">Phone</th>
            <th data-field="tracking_no"  data-visible="false">Tracking Number</th>
			<th data-field="order_at" data-visible="false"  data-sortable="true">Order At</th>
			<th data-field="currency" data-visible="false">Currency</th>
			<th data-field="order_price"  data-class="number"  data-sortable="true">Amount</th>
			<th data-field="product_price"  data-class="number"  data-sortable="true">Product Amount</th>
			<th data-field="shipping_price"  data-class="number"  data-sortable="true">Shipping Amount</th>
            <th data-field="discount_price"  data-visible="false" data-class="number"  data-sortable="true">Discount Amount</th>
			<th data-field="store" data-visible="false">Store Name</th>
			<th data-field="address" data-visible="false">Address</th>
			<th data-field="country"  data-sortable="true">Country</th>
            <th data-field="products"  data-visible="false">Product</th>

		</tr>
		</thead>
	</table>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#filter-country').select2({theme: "bootstrap"});
        $('#filter-store').select2({theme: "bootstrap"});

        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage"});

        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });


    });
</script>