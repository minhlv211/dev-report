<form class="form-horizontal" method="post" action="fb-campaign-mapping" id="edit-app">
	<div class="form-group">
		<label class="col-sm-2 control-label">Campaign ID</label>
		<div class="col-sm-4">
			<?php echo $campaign_id;?>
			<input type="hidden" name="cid" value="<?php echo $campaign_id;?>" />

		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Campaign Name</label>
		<div class="col-sm-4">
			<?php echo $campaign_info['campaign_name'];?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Product</label>
		<div class="col-sm-4">
			<select class="form-control" name="product_id" id="product_id">
				<option value="-1">Other</option>
				<?php
				foreach ($productList as $_product) {
					$amount = UtilHelper::number_format($_product['amount'], 2, '-', '$');
					$selected = '';
					if ($_product['product_id'] == $campaign_info['product_id']) {
						$selected = ' selected="selected" ';
					}
					echo '<option value="'.$_product['product_id'].'" '.$selected.'>'.$_product['title'] . ' (' . $amount . ')</option>';
				}
				?>
			</select>
		</div>
	</div>


	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-4">
			<button class="btn btn-primary">Lưu lại</button>
			<a href="fb-campaign" class="btn btn-default">Quay lại</a>
		</div>
	</div>
</form>

<script type="text/javascript">
    (function () {
        $('#edit-app').validate();
        $('#product_id').select2({theme: "bootstrap"});
    })();
</script>