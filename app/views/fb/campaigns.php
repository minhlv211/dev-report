<form class="form-inline filter-form" action="fb-campaign?" method="get">
    <div class="form-group" style="position: relative; min-width: 200px">
        <input id="date-range" type="text" class="form-control" style="width: 100%;  cursor: pointer" name="filter[date_range]"
               value="<?php echo $start_date . '-' . $end_date ?>">
        <i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
    </div>

	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
	</div>
</form>
<div style="overflow: auto">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="fb_campaign_list"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
			<th data-field="no" data-align="right">No</th>
            <th data-field="account_id" data-visible="false">Account ID</th>
			<th data-field="campaign_id" data-visible="false">Campaign ID</th>
            <th data-field="campaign_name" data-visible="true">Campaign Name</th>
            <th data-field="spend" data-visible="true" data-class="number" data-sortable="true">Spend</th>
            <th data-field="impressions" data-visible="true" data-class="number" data-sortable="true">Impressions</th>
            <th data-field="clicks" data-visible="false" data-class="number" data-sortable="true">clicks</th>
            <th data-field="inline_link_clicks" data-visible="true" data-class="number" data-sortable="true">Link Clicks</th>
            <th data-field="start_date" data-visible="true">Start Date</th>
            <th data-field="end_date" data-visible="false">End Date</th>
            <th data-field="product">Product Mapping</th>
            <th data-field="action">Action</th>
		</tr>
		</thead>
	</table>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage",
            customSort: function (sortName, sortOrder) {
                if (!sortName || !sortOrder) {
                    return;
                }
                this.data.sort(function(a, b) {
                    var x = a[sortName];
                    var y = b[sortName];
                    if (sortName != 'title') {
                        x = x.replace(',','');
                        y = y.replace(',','');
                        x = parseFloat(x);
                        y = parseFloat(y);
                        if (isNaN(x)) {
                            x = 0;
                        }
                        if (isNaN(y)) {
                            y = 0;
                        }
                    }
                    if (sortOrder == 'desc') {
                        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
                    } else {
                        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                    }

                });
        }});

        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
        });


    });
</script>