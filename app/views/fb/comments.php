<form class="form-inline filter-form" action="fb-comments?" method="get">
<!--	<div class="form-group" style="position: relative">-->
<!--		<input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer" name="filter[date_range]"-->
<!--			   value="--><?php //echo $start_date . '-' . $end_date ?><!--">-->
<!--		<i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>-->
<!--	</div>-->

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[hidden]">
            <option value="-1">All Comment</option>
            <option value="1" <?php echo $filter_hidden == '1'?' selected="selected" ':''; ?> >Hidden Comment</option>
            <option value="2" <?php echo $filter_hidden == '2'?' selected="selected" ':''; ?> >Visible Comment</option>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[negative]">
            <option value="-1">All Comment</option>
            <option value="1" <?php echo $filter_negative == '1'?' selected="selected" ':''; ?> >Negative Feedback</option>
            <option value="2" <?php echo $filter_negative == '2'?' selected="selected" ':''; ?> >Positive Feedback</option>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[reply]">
            <option value="-1">All Comment</option>
            <option value="2" <?php echo $filter_reply == '2'?' selected="selected" ':''; ?> >Non-Reply Comments</option>
            <option value="1" <?php echo $filter_reply == '1'?' selected="selected" ':''; ?> >Replied Comments</option>
            <option value="3" <?php echo $filter_reply == '3'?' selected="selected" ':''; ?> >Ignored Comments</option>
        </select>
    </div>

	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
	</div>

    <label class="checkbox">
        <input type="checkbox" class="form-control auto_submit" value="1" name="show_all" <?php echo $show_all?' checked="checked"':''; ?> />
        Always show button
    </label>
</form>
<div style="overflow: auto">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="fb_comments_list"
		   data-side-pagination="server"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
            <th data-class="no_wrap" data-field="created_time" data-visible="false"data-sortable="true">Create Time</th>
			<th data-field="last_date" data-visible="true"" data-sortable="true">Last Update</th>
            <th data-field="post_from_name"  data-visible="true">Page Info</th>
            <th data-field="from_name"  data-visible="true">Customer Name</th>
            <th data-field="message" data-visible="true">Message</th>
			<th data-field="comment_count" data-visible="true"  data-sortable="true" data-align="right" >Comment Count</th>
            <th data-field="like_count" data-visible="true"  data-sortable="true" data-align="right" >Like Count</th>
            <th data-field="is_hidden" data-visible="true" >Hidden</th>
            <th data-field="reply" data-visible="true" >Reply</th>
            <th data-field="link" data-visible="true" >Link</th>
            <th data-field="id" data-visible="false" >ID</th>



		</tr>
		</thead>
	</table>
</div>
<div class="comment_overlay hidden">
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#filter-country').select2({theme: "bootstrap"});
        $('#filter-product').select2({theme: "bootstrap"});
        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage"});

        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });

        $('#table-report').on("click", ".translate", function (e) {
            e.preventDefault();
            var comment_id = $(this).attr('id');
            var comment_text = $('#comment_' + comment_id).html();
            $('#comment_' + comment_id).html(comment_text + '<img src="/assets/custom/loading.gif" width="24" >')
            var data_post = {message: comment_text};
            var this_link = $(this);
            $.ajax({
                type: "GET",
                url: '/fb-translate',
                data: data_post
            })
            .done(function( data ) {
                $('#comment_' + comment_id).html(comment_text + ' <br />Translate: ' + data);
            }).always(function() {
                $(this_link).hide();
            });
            console.log(comment_text);
        });


        $('#table-report').on("click", ".mt-comments", function(e) {
            if ($(e.target).hasClass('translate')) {
                return;
            }
            if ($('.reply_form', $(this).parent()).css('display') == 'none') {
                $('.reply_form', $(this).parent()).slideDown(200);
                $('.hidden-button', $(this).parent()).show();

            } else {
                $('.reply_form', $(this).parent()).slideUp(200);
                $('.hidden-button', $(this).parent()).hide();
            }

        });



        $('#table-report').on("click", "a.btn-ajax", function (e) {
            e.preventDefault();

            var url = $(this).attr('href');
            $('.comment_overlay').removeClass('hidden');
            $.ajax({
                url: url
            })
            .done(function( data ) {
                $("[name=refresh]").click();
                $('.comment_overlay').addClass('hidden');
            }).fail(function() {
                $("[name=refresh]").click();
                $('.comment_overlay').addClass('hidden');
            });
        });
        $('#table-report').on("click", ".reply_button", function (e) {
            replyComment($(this).parent().parent())
        });
        $('#table-report').on("keydown", ".reply_message", function (e) {
            if (event.which == 13) {
                replyComment($(this).parent().parent())
            }
        });

        function replyComment(parent_item) {
            var commend_id = $('.comment_id', parent_item).val();
            var reply_message = $('.reply_message', parent_item).val();
            if (!reply_message) {
                alert('You must Enter content for reply!');
                return;
            }
            $('.comment_overlay').removeClass('hidden');
            var data_post = {id: commend_id, message: reply_message};
            $.ajax({
                type: "POST",
                url: '/fb-comment-reply',
                data: data_post
            })
            .done(function( data ) {
                $('.comment_overlay').addClass('hidden');
                $("[name=refresh]").click();
                if (data.status == 0) {
                    alert('Comment Fail!');
                }
            }).always(function() {
                $('.comment_overlay').addClass('hidden');
            });

        }



    });
</script>
<style type="text/css">
    #table-report img {
        max-height: 100px;
        max-width: 100px;
    }
    .table .btn.hidden-button{
        position: absolute;
        top: 0px;
        right: 0px;
        margin-right: 0px;
        <?php
            if(!$show_all) {
                echo 'display: none;';
            }
        ?>
    }
    .hidden_comment , .mt-comments .hidden_comment .name, .hidden_comment a{
        color: #AAA !important;
    }


    .comment_overlay {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        opacity: 0.9;
        background: url(/assets/custom/loading2.gif) center no-repeat #fff;
    }
</style>