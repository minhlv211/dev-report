
<a href="app-create" class="btn btn-primary"><i class="fa fa-edit"></i> Thêm app mới</a>

<br /><br />


<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
       data-search="true"
       data-show-refresh="true"
       data-show-toggle="true"
       data-show-columns="true"
       data-mobile-responsive="true"
       data-show-export="true"
       data-pagination="true"
       data-cookie="true"
       data-cookie-id-table="app_list"
       data-side-pagination="server"
       data-pagination-v-align="both"
       data-page-size="25"
       data-sortable="true"
       data-page-list="[10,25,50,100, 500, 1000, 5000]"
       data-url="<?php echo $url_page;?>"
>
    <thead>
    <tr>
        <th data-field="no" data-align="right">No</th>
        <th data-field="app_name" data-sortable="true">App Name</th>
        <th data-field="gcm_sender_id" data-visible="false">GCM ID</th>
        <th data-field="email" data-visible="false">Email</th>
        <th data-field="package_name" data-visible="false">Package Name</th>
        <th data-field="status">Status</th>
        <th data-field="create_at" data-sortable="true">Create Date</th>
        <th data-field="last_modify" data-sortable="true">Last Update</th>

        <th data-field="count" data-sortable="true" data-class="number">Device</th>
        <th data-field="action" ></th>
    </tr>
    </thead>
</table>


<script type="text/javascript">
    function queryParams() {
        var params = {};
        $('#toolbar').find('input[name]').each(function () {
            params[$(this).attr('name')] = $(this).val();
        });
        console.log(params);
        return params;
    }

    $(document).ready(function () {

        var $table = $('#table-report'),
            $ok = $('#ok');

        $(function () {
            $ok.click(function () {
                $table.bootstrapTable('refresh');
            });
        });


        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage"});

        /*
        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#start_date').val(start.format('YYYY-MM-DD'));
            $('#end_date').val(end.format('YYYY-MM-DD'));
            $('#filter-form').submit();
        });

        */


    });
</script>