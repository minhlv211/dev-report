<form class="form-horizontal" method="post" action="fb-campaign-mapping" id="edit-app">
    <div class="form-group">
        <label class="col-sm-2 control-label">Campaign ID</label>
        <div class="col-sm-4">
            <?php echo $campaign_id;?>
            <input type="hidden" name="cid" value="<?php echo $campaign_id;?>" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Package name</label>
        <div class="col-sm-4">
			<?php echo $campaign_info['campaign_name'];?>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Nhóm ứng dụng</label>
        <div class="col-sm-4">
            <select class="form-control" name="category">
                <option value="">Chọn nhóm</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tình trạng</label>
        <div class="col-sm-4">
            <select class="form-control" name="status">
                <option value="1" <?php echo $appInfo['status'] == 1?' selected="selected" ':'';  ?>>Đang chạy</option>
                <option value="0" <?php echo $appInfo['status'] == 0?' selected="selected" ':'';  ?>>Tạm dừng</option>
                <option value="2" <?php echo $appInfo['status'] == 2?' selected="selected" ':'';  ?>>Đã bị ban</option>
            </select>
        </div>
    </div>

    <fieldset>
        <legend>Thông tin quảng cáo</legend>
        <div class="form-group">
            <label class="col-sm-2 control-label">Quảng cáo mặc định</label>
            <div class="col-sm-4">
                <select class="form-control" name="default_ad">
                    <option value="ad" <?php echo $appInfo['default_ad'] == 'ad'?' selected="selected" ':'';  ?>>Admob</option>
                    <option value="fan" <?php echo $appInfo['default_ad'] == 'fan'?' selected="selected" ':'';  ?>>Facebook</option>
                    <option value="sa" <?php echo $appInfo['default_ad'] == 'sa'?' selected="selected" ':'';  ?>>Startapp</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Admob Full ID</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="ad_full_key"
                       value="<?php echo $appInfo['ad_full_key']; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Admob Banner ID</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="ad_banner_key"
                       value="<?php echo $appInfo['ad_banner_key']; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Facebook Ads ID</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="fan_key"
                       value="<?php echo $appInfo['fan_key']; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Startapp ID</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="sa_key"
                       value="<?php echo $appInfo['sa_key']; ?>"/>
            </div>
        </div>

    </fieldset>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-4">
            <button class="btn btn-primary">Lưu lại</button>
            <a href="app-list" class="btn btn-default">Quay lại</a>
        </div>
    </div>
</form>

<script type="text/javascript">
    (function () {
        $('#edit-app').validate();
    })();
</script>