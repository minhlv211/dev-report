<div class="row">
	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 blue" href="report">
			<div class="visual">
				<i class="fa fa-bar-chart-o"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo $reportToday['sell_amount']; ?>"><?php echo UtilHelper::number_format($reportToday['sell_amount'], 0, '-', '$'); ?></span> </div>
				<div class="desc"> Today Revenue </div>
			</div>
		</a>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 red" href="report">
			<div class="visual">
				<i class="fa fa-bar-chart-o"></i>
			</div>
			<div class="details">
				<div class="number">
                    <span data-counter="counterup" data-value="<?php echo $reportToday['est_total_spend']; ?>">
                        <?php echo UtilHelper::number_format($reportToday['est_total_spend'], 0, '-', '$'); ?> /
						<?php echo UtilHelper::number_format($reportToday['spend'], 0, '-', '$'); ?>
                    </span> </div>
				<div class="desc"> Total Spend / Marketing</div>
			</div>
		</a>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 green" href="report">
			<div class="visual">
				<i class="fa fa-bar-chart-o"></i>
			</div>
			<div class="details">
				<div class="number">
                    <span data-counter="counterup" data-value="<?php echo $reportToday['est_profit']; ?>">
                        <?php echo UtilHelper::number_format($reportToday['est_profit'], 0, '-', '$'); ?> /
						<?php echo UtilHelper::number_format($reportToday['est_roi'] * 100, 1, '-', '%'); ?>

                    </span> </div>
				<div class="desc"> Estimate Profit / Margin</div>
			</div>
		</a>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 purple" href="report?&filter[date_range]=<?php echo $yesterday . '-' . $yesterday; ?>">
			<div class="visual">
				<i class="fa fa-bar-chart-o"></i>
			</div>
			<div class="details">
				<div class="number">
                    <span data-counter="counterup" data-value="<?php echo $reportYesterday['sell_amount']; ?>">
                        <?php echo UtilHelper::number_format($reportYesterday['sell_amount'], 0, '-', '$'); ?> /
						<?php echo UtilHelper::number_format($reportYesterday['est_roi'] * 100, 1, '-', '%'); ?>
                        </span> </div>
				<div class="desc"> Yesterday Revenue / Margin</div>
			</div>
		</a>
	</div>
</div>
