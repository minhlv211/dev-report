<form class="form-inline filter-form" action="ticket-report?" method="get">
    <div class="form-group" style="position: relative">
        <input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer" name="filter[date_range]"
               value="<?php echo $start_date . '-' . $end_date ?>">
        <i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
    </div>

<!--    <div class="form-group">-->
<!--        <input name="filter[email]" value="--><?php //echo $filter_email; ?><!--"  class="form-control" placeholder="Email"/>-->
<!--    </div>-->

    <div class="form-group">
        <button type="submit" class="btn btn-info">Filters</button>
    </div>
</form>
<div style="overflow: auto">
    <table class="table table-striped table-advance table-hover table-bordered" id="table-report"
           data-show-refresh="true"
           data-show-toggle="true"
           data-show-columns="true"
           data-mobile-responsive="true"
           data-show-export="true"
           data-pagination="true"
           data-cookie="true"
           data-cookie-id-table="email_list"
           data-side-pagination="server"
           data-pagination-v-align="both"
           data-page-size="25"
           data-sortable="true"
           data-page-list="[10,25,50,100, 500, 1000, 5000]"
           data-url="<?php echo $url_page;?>"
    >
        <thead>
        <tr>
            <th data-field="date" data-visible="true">Date</th>
            <th data-field="total" data-class="number" data-visible="true">Total</th>
            <th data-field="open" data-class="number" data-visible="true">Open</th>
            <th data-field="answered" data-class="number" data-visible="true">Answered</th>
            <th data-field="waiting" data-class="number" data-visible="true">Waiting</th>
            <th data-field="closed" data-class="number" data-visible="true">Closed</th>
<!--            <th data-field="average_time_reply" data-class="number" data-visible="true">Average Time Reply</th>-->
        </tr>
        </thead>
    </table>
</div>
<div class="comment_overlay hidden">
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage"});
        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });


    });
</script>
<style type="text/css">
    .comment_overlay {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        opacity: 0.9;
        background: url(/assets/custom/loading2.gif) center no-repeat #fff;
    }
</style>