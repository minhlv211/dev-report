<form class="form-inline filter-form" action="ticket-list?" method="get">
    <input type="hidden" name="filter[status]" >


	<div class="form-group" style="position: relative">
		<input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer" name="filter[date_range]"
			   value="<?php echo $start_date . '-' . $end_date ?>">
		<i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
	</div>


<!--    <div class="form-group">-->
<!--        <select class="form-control auto_submit" name="filter[status]" id="filter-status">-->
<!--            <option value="-1">All Status</option>-->
<!--            --><?php //foreach ($statusList as $k => $value) { ?>
<!--                <option value="--><?php //echo $value['value'] ?><!--" --><?php //echo ($filter_status == $value['value']) ?' selected="selected" ':''; ?><!-- >--><?php //echo $k.' ('.$value['count'].')'; ?><!--</option>-->
<!--            --><?php //} ?>
<!--        </select>-->
<!--    </div>-->

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[country]" id="filter-country">
            <option value="">All Country</option>
            <?php
            if(count($countryList) > 0) {
                foreach ($countryList as $_k => $_country) {
                    $selected = '';
                    if ($_k.'' == $filter_country) {
                        $selected = ' selected="selected" ';
                    }
                    echo '<option value="'.$_k.'" '.$selected.'>'.$_country['value'].' (' .$_country['count'].')</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <input name="filter[email]" value="<?php echo $filter_email; ?>"  class="form-control" placeholder="Email"/>
    </div>
    <div class="form-group">
        <input name="filter[number]" value="<?php echo $filter_number; ?>"  class="form-control" placeholder="Order Number"/>
    </div>

	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
	</div>
    <br><br>
    <ul class="nav nav-tabs">
        <?php foreach ($statusList as $k => $value) { ?>
            <li class="<?php echo ((string)$filter_status === $value['value'] || count($statusList) == 1) ?'active':'';  ?>" data-value="<?php echo $value['value'] ?>" >
                <a data-toggle="" href="<?php echo $query_url.'&filter[status]='.$value['value'] ?>"><?php echo $k.' ('.$value['count'].')'; ?></a>
            </li>
        <?php } ?>
    </ul>
</form>
<div style="overflow: auto">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="email_list"
		   data-side-pagination="server"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
            <th data-field="_id" data-visible="false">ID</th>
            <th data-class="no_wrap" data-field="to_time" data-sortable="true">Last Time</th>
            <th data-class="no_wrap" data-field="from_time" data-visible="false" data-sortable="true">First Time</th>
            <th data-field="email" data-visible="true">Email</th>
			<th data-field="topic" data-visible="true">Topic</th>
            <th data-field="status" data-visible="true" data-class="text-center">Status</th>
            <th data-field="country" data-visible="true">Country</th>
            <th data-field="count" data-visible="true" data-sortable="true">Total Mail</th>
            <th data-field="link" data-visible="true">Detail</th>
		</tr>
		</thead>
	</table>
</div>
<div class="comment_overlay hidden">
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#filter-status').select2({theme: "bootstrap"});
        $('#filter-country').select2({theme: "bootstrap"});
        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage"});
        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });


    });
</script>
<style type="text/css">
    .comment_overlay {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        opacity: 0.9;
        background: url(/assets/custom/loading2.gif) center no-repeat #fff;
    }
</style>