<link rel="stylesheet" type="text/css" href="/assets/custom/summernote.css">
<script src="/assets/custom/summernote.min.js"></script>
<style type="text/css">

</style>

<div class="row">
    <div class="col-md-6">

        <fieldset style="margin-top: 20px">
            <form id="reply-form" class="form" action="" method="post">
                <div class="form-group">
                    <textarea class="form-control" rows="15" id="signature" name="signature"><?php echo $signature; ?></textarea>
                </div>
                <div class="form-group">
                    <button id="save-signature" type="submit" class="btn btn-info">Save</button>
                </div>
            </form>

        </fieldset>
    </div>
</div>
<script type="text/javascript">

    $("#signature").summernote({
        height:200,
        callbacks: {
            onImageUpload: function(file) {
               uploadFile(file,'#signature');
            }
        }
    });

</script>
<style>
    blockquote, .yahoo_quoted {
        display: none !important;
    }
    .detail-info {
        display: none;
    }
</style>