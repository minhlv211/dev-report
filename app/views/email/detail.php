<link rel="stylesheet" type="text/css" href="/assets/custom/summernote.css">
<script src="/assets/custom/summernote.min.js"></script>
<style type="text/css">
    .full_desc {
        display: none;
    }
    .short_desc {
        line-height: 1.5em;
        cursor: pointer;
        font-size: 12px;
        overflow: hidden;
    }
    .container_desc {
        overflow: hidden;
        border-bottom:1px solid #CCC;
        padding: 5px 0;
    }
    .short_desc .content {
        font-size: 11px;
        color: #AAA;
    }
    .ticket-action-nav {
        padding: 10px;
        position: fixed;
        right: 0;
        top: 50px;
        text-align: right;
        z-index: 9;
    }
    .a-status:hover .st-item-wp {display: block}
    .ticket-action-nav div[class^="a-"] { display: inline-block;position: relative}
    .st-item-wp {display:none;position: absolute; width: 100%; border: 1px solid #e9e9e9; background: white  }
    .st-item { padding: 7px; cursor: pointer; }
    .st-item:hover {
        background: #0b94ea;
        color: white;
    }
    <?php if (count($emailList) > 5) { ?>
    .container_desc_wp .container_desc {  display: none;  }
    .container_desc_wp .container_desc:first-child ,.container_desc_wp .container_desc:last-child {  display: block; }
    <?php } ?>
    .container_desc_collapse {
        cursor: pointer;
        text-align: center;
        position: relative;
        padding: 7px 0;
        border-bottom: 1px solid #CCC;
        z-index: 9;
    }
    .container_desc_collapse span {
        background: white;
        padding: 0 10px;
    }
    .container_desc_collapse em {
        height: 12px;
        width: 100%;
        border-bottom: 1px solid #ccc;
        border-top: 1px solid #ccc;
        display: inline-block;
        position: absolute;
        left: 0;
        top: 11px;
        z-index: -1;
    }

</style>
<!-- start fixed nav -->
<div class="ticket-action-nav col-md-6">
    <div class="a-reply">
        <button class="btn btn-default" onclick=""><i class="fa fa-reply" aria-hidden="true"></i> Reply</button>
    </div>
    <div class="a-note">
        <button class="btn btn-default" data-toggle="modal" data-target="#ticket-note"><i class="fa fa-sticky-note" aria-hidden="true"></i> Note</button>
    </div>
    <div class="a-status">
        <button class="btn btn-default"><i class="fa fa-flag" aria-hidden="true"></i> Status <i class="fa fa-angle-down" aria-hidden="true" style="padding-left: 5px"></i></button>
        <form action="ticket-edit" method="post">
            <input type="hidden" name="id" value="<?php echo $ticket['_id'] ?>">
            <input type="hidden" name="_email" value="<?php echo $ticket['email'] ?>">
            <input type="hidden" name="status" id="sc-status" value="">

            <div class="form-group st-item-wp">
                <?php foreach ($statusList as $k => $item) { ?>
                    <div class="st-item" data-value="<?php echo $k; ?>"> <?php if($k == $ticket['status'] && isset($ticket['status'])) {echo '<i class="fa fa-check-circle-o" aria-hidden="true"></i> ';} echo $item; ?></div>
                <?php } ?>
            </div>
        </form>
        <script>
            $('.st-item').on('click', function () {
                console.log($(this).data('value'));
               $('#sc-status').val($(this).data('value'));
               $(this).closest('form').submit();
            });
        </script>
    </div>
    <div class="a-back">
        <a href="javascript: window.location.href='/ticket-list';" class="btn btn-default"><i class="fa fa-undo"></i> Back to list</a>
    </div>
</div>

<div id="ticket-note" class="modal fade" role="dialog">
    <form action="ticket-edit" method="post" class="row ticket-action" style="margin-bottom: 20px">
        <input type="hidden" name="id" value="<?php echo $ticket['_id'] ?>">
        <input type="hidden" name="_email" value="<?php echo $ticket['email'] ?>">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add note</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea name="note" class="form-control" style="height: 100px"><?php echo $ticket['note'] ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
    </form>
</div>

<div class="row">
	<div class="col-md-6">
		<h2 style="margin-top: 0; border-bottom: 1px solid #EEE; padding-bottom: 10px"><?php echo $ticket['topic'];?></h2>
        <div class="container_desc_wp">
            <?php

            $lastId = '';
            $parent = '';
            $topic = '';
            foreach ($emailList as $_email) {
                $attachment = '';
                if (isset($_email['attachment']) && !empty($_email['attachment'])) {
                    foreach ($_email['attachment'] as $att) {
                        $cid = ltrim(rtrim($att['cid'],'>'),'<');

                        if (!empty($cid) && strpos($_email['body']['text/html'],$cid) !== false) {
                            $_email['body']['text/html'] = str_replace(
                                "cid:".$cid,
                                $att['url'],
                                $_email['body']['text/html']);
                        }else {
                            $attPrev = (preg_match('#^image/\w+#i',$att['type'])) ? '<img src="'.$att['url'].'" >' : '<i class="fa fa-file-o" aria-hidden="true"></i>';
                            $attachment .= '<div class="att-wrapper">';
                            $attachment .= '<a href="'.$att['url'].'" 
                            download="'.$att['name'].'"><span>'.$att['name'].'</span>
                            <div class="att-prev">'.$attPrev.'</div></a>';
                            $attachment .= '</div>';
                        }
                    }
                }

                $full_desc = ($_email['body']['text/html']) ? $_email['body']['text/html'] : nl2br($_email['body']['text/plain']);
                echo '<div class="container_desc">';
                echo '<div class="short_desc">'.
                        '<div class="pull-left">' .
                            $_email['from'] .
                        '</div>'.
                        '<div class="pull-right">' .
                            UtilHelper::time_elapsed_string($_email['date_time']) .
                        '</div>'.
                        '<div class="content"><br />' .
                            $_email['short_desc'] .
                    '   </div>
                    </div>';
                echo '<div class="full_desc">' .$full_desc;
                echo ($attachment) ? '<hr /><h5 style="font-weight: 600">Attachment</h5><div class="attachment-group">'.$attachment.'</div>' : '';
                echo '</div>';
                echo '</div>';
                $lastId = $_email['id'];
                $parent = $_email['parent'];
                $topic = $_email['topic'];
            }
            ?>
        </div>
		<fieldset style="margin-top: 20px">
			<legend style="margin-bottom: 10px">Reply email</legend>
            <?php if ($ticket['note'] != '') { ?>
                <h5 style="color: #e43a45; font-weight: 600">Note : <span style="font-weight: normal"><?php echo $ticket['note'] ?></span></h5>
            <?php } ?>
			<form id="reply-form" class="form" action="ticket-reply" method="post">
                <input type="hidden" name="email" value="<?php echo $fromEmail;?>">
                <input type="hidden" name="id" value="<?php echo $lastId;?>">
                <input type="hidden" name="topic" value="<?php echo $topic;?>">
				<div class="form-group">
					<label>Subject</label>
					<input type="text" class="form-control" name="reply_subject" value="RE: <?php echo $ticket['topic'];?>" />
				</div>
				<div class="form-group">
					<label>Content</label>
					<textarea class="form-control" rows="15" id="reply_content" name="reply_content">
                        <?php echo (!empty($signature)) ? '<br><br>'.$signature : ''; ?>
                    </textarea>
				</div>
<!--                <div class="form-group">-->
<!--                    <label class="checkbox">-->
<!--                        <input type="checkbox" name="include_quote" value="1" checked="checked"  style="position: static; margin-left: 0; vertical-align: top"/>-->
<!--                        Include Quote-->
<!--                    </label>-->
<!---->
<!--                </div>-->
<!--				<div class="form-group">-->
<!--					<label>Attachments</label>-->
<!--					<input type="file" name="file" placeholder="Add a Attachment" />-->
<!--				</div>-->
            </form>
            <form id="ticket-form" action="ticket-edit" method="post">
                <input type="hidden" name="id" value="<?php echo $ticket['_id'] ?>">
                <input type="hidden" name="_email" value="<?php echo $ticket['email'] ?>">
                <div class="form-group form-inline">
                    <label for="ticket-status">Status </label>
                    <select name="status" id="ticket_status" class="form-control" onchange=" /*$(this).closest('form').submit()*/">
                        <?php foreach ($statusList as $k => $item) { ?>
                            <option value="<?php echo $k; ?>" <?php if($k == $ticket['status'] && isset($ticket['status'])) {echo 'selected';} ?>><?php echo $item; ?></option>
                        <?php } ?>
                    </select>
<!--                    <h5 style="font-weight: 600">Status : <span data-toggle="modal" data-target="#ticket-status" class="tk---><?php //echo strtolower($statusList[$ticket['status']]) ?><!--">--><?php //echo $statusList[$ticket['status']] ?><!--</span></h5>-->
                </div>
            </form>
            <div class="form-group">
                <button id="ticket-edit" type="button" class="btn btn-success">Update Status</button>
                <button id="reply-post" type="button" class="btn btn-info">Post Reply</button>
            </div>
            <div id="ticket-status" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <form action="ticket-edit" method="post" style="margin-top: 15px">
                        <input type="hidden" name="id" value="<?php echo $ticket['_id'] ?>">
                        <input type="hidden" name="_email" value="<?php echo $ticket['email'] ?>">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Change status</h4>
                            </div>
                            <div class="modal-body">
                                <select class="form-control" name="status" id="statsus">
                                    <?php foreach ($statusList as $k => $item) { ?>
                                        <option value="<?php echo $k; ?>" <?php if($k == $ticket['status'] && isset($ticket['status'])) {echo 'selected';} ?>><?php echo $item; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </form>>
                </div>
            </div>

		</fieldset>
	</div>
	<div class="col-md-6">
		<h2 style="margin-top: 0">Order Information</h2>
		<?php
		foreach ($orderList as $_order) {
		?>
			<table class="table table-striped table-advance table-hover table-bordered">
				<tr>
					<th>Time</th>
					<td>
						<strong><?php echo date('Y-m-d', strtotime($_order['created_at'])); ?></strong>
						<?php echo date('H:i:s', strtotime($_order['created_at'])); ?>
						(<?php echo UtilHelper::time_elapsed_string($_order['created_at']);?>)
					</td>
                    <th>Price</th>
                    <td><?php echo UtilHelper::number_format($_order['total_price'], 2, '-', '$'); ?></td>

				</tr>
				<tr>
					<th>Order ID</th>
					<td><?php echo $_order['order_id'] . ' / #' . ($_order['number'] + 1000); ?> </td>
					<th>Name</th>
					<td><?php echo $_order['name']; ?></td>
				</tr>
<!--				<tr>-->
<!--					<th>Email</th>-->
<!--					<td>--><?php //echo $_order['email']; ?><!--</td>-->
<!--                    <th>Phone</th>-->
<!--                    <td>--><?php //echo $_order['phone']; ?><!--</td>-->
<!--                </tr>-->
<!--				<tr>-->
<!--					<th>Financial</th>-->
<!--					<td>--><?php //echo $_order['financial_status']; ?><!--</td>-->
<!--				</tr>-->
                <tr>
                    <th>Address</th>
                    <td colspan="3">
                        <?php echo $_order['address1'] . ', ' . $_order['city'] . ', ' . $_order['province']. ', ' . $_order['country']; ?>
                        <img src="http://js.mythofeastern.com/flag/flags-mini/<?php echo strtolower($_order['country_code']);?>.png" height="12" />
                    </td>
                </tr>
				<tr>
					<th>Items</th>
					<td colspan="3">
						<?php
						foreach ($_order['items'] as $_item) {
							echo $_item['title'] . '(' . UtilHelper::number_format($_item['price'], 2, '-', '$') .'x' . $_item['quantity'] . ') - ';
							echo '<a target="cainiao" href="http://global.cainiao.com/detail.htm?mailNoList='.$_item['tracking_number'].'">' .$_item['tracking_number'] . '</a><br />';
						}
						?>
					</td>
				</tr>
			</table>
		<?php

			$trackingList = $_order['trackingList'];
			if (count($trackingList) > 0) {
			    foreach ($trackingList as $_tracking) {?>
                    <table class="table table-striped table-advance table-hover table-bordered">
                        <tr>
                            <th>Tracking</th>
                            <td><?php echo $_tracking['tracking_number'] ?></td>
                            <th>Status</th>
                            <td>
                                <img src="/assets/custom/status/<?php echo $_tracking['tag'] ?>.svg" style="float: left; width: 24px" /> &nbsp;
								<?php echo $_tracking['tag'] ?>
								(<?php echo $_tracking['end_date']; ?>)
                            </td>
                        </tr>
                        <tr>
                            <th>Courier</th>
                            <td><?php echo CarrierHelper::code2Name($_tracking['slug']) ?></td>
                            <th>Courier</th>
                            <td>
                                <?php
                                echo CarrierHelper::code2Name($_tracking['slug2']);
                                $_url = CarrierHelper::code2Url($_tracking['slug2']);
                                $_mobile = CarrierHelper::code2Phone($_tracking['slug2']);
                                if ($_mobile) {
                                    echo ' (' . $_mobile . ')';
                                 }
								if ($_url) {
									echo ' <a class="btn btn-default btn-xs" href="'.$_url.'"><i class="fa fa-globe"></i> Link</a> ';
								}
                                ?>
                            </td>
                        </tr>
<!--                        <tr>-->
<!--                            <th>Ali Order Date</th>-->
<!--                            <td>--><?php //echo substr($_tracking['ali_order_time'], 0, 10); ?><!--</td>-->
<!--                            <th>Last Update</th>-->
<!--                            <td>--><?php //echo $_tracking['end_date']; ?><!--</td>-->
<!--                        </tr>-->
                        <?php if ($_tracking['checkpoint_destination']) { ?>
                        <tr>
                            <td colspan="4" style="font-size: 12px">
                                <strong>Detail Information from <?php echo CarrierHelper::code2Name($_tracking['slug2']) ?></strong> <a href="#" class="show-more">Show more</a>
                                <div class="detail-info">
                                <?php

                                foreach ($_tracking['checkpoint_destination'] as $_checkpoint) {
                                    $checkpoint_time = $_checkpoint['checkpoint_time'];
									$checkpoint_time = date('Y-m-d H:i', strtotime($checkpoint_time));
                                    echo $checkpoint_time . ': ' . $_checkpoint['message'] . '<br />';
                                }
                                ?>
                                    <div><a href="#" class="show-less">Hide</a></div>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
						<?php if ($_tracking['checkpoint_source']) { ?>
                            <tr>
                                <td colspan="4" style="font-size: 12px">
                                    <strong>Detail Information from <?php echo CarrierHelper::code2Name($_tracking['slug']) ?></strong> <a href="#" class="show-more">Show more</a>
                                    <div class="detail-info">
									<?php
									foreach ($_tracking['checkpoint_source'] as $_checkpoint) {
										$checkpoint_time = $_checkpoint['checkpoint_time'];
										$checkpoint_time = date('Y-m-d H:i', strtotime($checkpoint_time));
										echo $checkpoint_time . ': ' . $_checkpoint['message'] . '<br />';
									}
									?>
                                        <div><a href="#" class="show-less">Hide</a></div>
                                    </div>
                                </td>
                            </tr>
						<?php } ?>
                    </table>
                <?php
                }
            }

            if ($_order['ali_info'] && $_order['ali_info']['brief_ali']) {
			    foreach ($_order['ali_info']['brief_ali'] as $_ali_order) {
			        ?>
                        <table class="table table-striped table-advance table-hover table-bordered">
                            <tr>
                                <th>Order Time</th>
                                <td>
                                    <?php echo date('Y-m-d H:i', strtotime($_ali_order['order_at']) + 3600 * 14);?>  -
                                    <a href="https://trade.aliexpress.com/order_detail.htm?&orderId=<?php echo $_ali_order['order_id'];?>">
										<?php echo $_ali_order['order_id'];?>
                                    </a>
                                </td>
                                <th>Store name</th>
                                <td><?php echo $_ali_order['product_ali'][0]['store_name'];?></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                            <?php
                            foreach ($_ali_order['product_ali'] as $_product) {
                                echo '<a target="_blank" href="https://www.aliexpress.com/item/-/'.$_product['product_id'].'.html">';
                                echo $_product['name'] . ' (' . $_product['quantity'] . ')';
                                echo '</a> <br />';
                            }
                            ?>
                                </td>
                            </tr>

                        </table>
                    <?php
                }

            }

            echo '<hr />';
		}
		?>
        <script type="text/javascript">
            window.onload = function () {
                $(".full_desc").each(function (index,v) {
                    if ($(this).find('blockquote').length > 0) {
                        $(this).append('<div class="em_qt"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></div>');
                        $(this).find('.em_qt').insertBefore($(this).find('blockquote'));
                    }
                });
                $('.em_qt').on('click', function () {
                   $(this).parent().find('blockquote').toggle();
                });
            }

            $("#reply_content").summernote({
                height:200,
                callbacks: {
                    onImageUpload: function(file) {
                        uploadFile(file,'#reply_content');
                    }
                }
            });
            //collapse email
            var listEmail = $('.container_desc');
            if (listEmail.length > 5){
                listEmail.last().before('<div class="container_desc_collapse">' +
                    '<span>' +(listEmail.length - 2) + " older messages" + '</span><em></em>' +
                    '</div>');
                console.log(listEmail.length);
                $('body').on('click','.container_desc_collapse', function () {
                    $(this).hide().promise().done(function () {
                        $('.container_desc').show();
                    });
                });
            }


            $('.a-reply button').on('click', function () {
                $('html, body').animate({scrollTop: $('.note-editable').offset().top},500,function() {
                    $('.note-editable').focus();
                });
            });

            $('#ticket-edit').on('click',function () {
                $('#ticket-form').submit();
            });
            $('#reply-post').on('click',function () {
                $('#reply-form').submit();
            });

            $('#status').select2({theme: "bootstrap"});
            $('.show-more').click(function () {
                $('.detail-info', $(this).parent()).slideDown();
                $(this).hide();
            })
            $('.show-less').click(function () {
                $('.detail-info', $(this).parent().parent().parent()).slideUp();
                $('.show-more', $(this).parent().parent().parent()).show();
            });
            $('.short_desc').click(function () {
                $('.full_desc', $(this).parent()).slideToggle(200);
                $('.content', this).toggle();
            });
            $('.short_desc:last').click();
        </script>
    </div>
</div>
<style>
    .em_qt {}
    .em_qt i {
        cursor: pointer;
        line-height: 0px !important;
        padding: 4px 7px;
        border: 1px solid #c7c7c7;
        color: #909090;
    }
	blockquote, .yahoo_quoted {
		display: none ;
	}
    .detail-info {
        display: none;
    }
</style>