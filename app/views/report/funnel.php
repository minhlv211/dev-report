<form class="form-inline filter-form" action="report-funnel?" method="get">
    <div class="form-group" style="position: relative">
        <input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer"
               name="filter[date_range]"
               value="<?php echo $start_date . '-' . $end_date ?>">
        <i class="fa fa-calendar"
           style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
    </div>

<!--    <div class="form-group">-->
<!--        <select class="form-control auto_submit" name="filter_by">-->
<!--            <option value="" --><?php //echo empty($filter_by)?' selected="selected" ':''; ?><!-- >No Filter</option>-->
<!--            <option value="country" --><?php //echo $filter_by == 'country'?' selected="selected" ':''; ?><!-- >Country</option>-->
<!--            <option value="device" --><?php //echo $filter_by == 'device'?' selected="selected" ':''; ?><!-- >Device</option>-->
<!--            <option value="platform" --><?php //echo $filter_by == 'platform'?' selected="selected" ':''; ?><!-- >Platform</option>-->
<!--        </select>-->
<!--        <select class="form-control auto_submit" name="filter[country]">-->
<!--            <option value="">Select a Country</option>-->
<!--        </select>-->
<!--    </div>-->


    <div class="form-group">
        <label>View By</label>
        <select class="form-control auto_submit" name="view_by">
            <option value="date" <?php echo $view_by == 'date'?' selected="selected" ':''; ?> >Date</option>
            <option value="product_id" <?php echo $view_by == 'product_id'?' selected="selected" ':''; ?> >Product</option>
            <option value="country" <?php echo $view_by == 'country'?' selected="selected" ':''; ?> >Country</option>
            <option value="device" <?php echo $view_by == 'device'?' selected="selected" ':''; ?> >Device</option>
            <option value="platform" <?php echo $view_by == 'platform'?' selected="selected" ':''; ?> >Platform</option>
            <option value="age" <?php echo $view_by == 'age'?' selected="selected" ':''; ?> >Age</option>
            <option value="gender" <?php echo $view_by == 'gender'?' selected="selected" ':''; ?> >Gender</option>
            <option value="hourly" <?php echo $view_by == 'hourly'?' selected="selected" ':''; ?> >Hourly</option>
        </select>
    </div>

    <?php if ($view_by == 'hourly') {?>
        <div class="form-group">
            <label for="filter-timezone">Timezone</label>
            <select class="form-control auto_submit" name="filter[timezone]" id="filter-timezone">
                <option value="">(GMT -06:00) Central Time</option>
                <option value="7" <?php echo ($filter_timezone) ? 'selected="selected"' : '' ; ?>>(GMT +07:00) Server Time</option>
            </select>
        </div>
    <?php  } ?>

    <div class="form-group">
        <button type="submit" class="btn btn-info">Filters</button>
    </div>
</form>
<div style="overflow: auto">
    <table class="table table-striped table-advance table-hover table-bordered" id="table-report"
           data-show-refresh="true"
           data-show-toggle="true"
           data-show-columns="true"
           data-mobile-responsive="true"
           data-show-export="true"
           data-pagination="true"
           data-cookie="true"
           data-cookie-id-table="report_funnel"
           data-pagination-v-align="both"
           data-page-size="25"

           data-page-list="[10,25,50,100, 500, 1000, 5000]"
           data-url="<?php echo $url_page; ?>"
    >
        <thead>
        <tr>
            <th data-field="id" data-visible="false"> ID</th>
            <th data-field="name" data-visible="true"> Name</th>
            <th data-field="ctr" data-visible="true" data-class="number">CTR</th>
            <th data-field="cr_cart" data-visible="true" data-class="number">Add To Cart Rate</th>
            <th data-field="cr_checkout" data-visible="true" data-class="number">Checkout Rate</th>
            <th data-field="cr_purchase" data-visible="true" data-class="number">Purchase Rate</th>
            <th data-field="cr" data-visible="true" data-class="number">CR Rate</th>

            <th data-field="impressions" data-visible="false" data-class="number">Impressions</th>
            <th data-field="inline_link_clicks" data-visible="false" data-class="number">Click</th>
            <th data-field="view_content" data-visible="false" data-class="number">View Content</th>
            <th data-field="add_to_cart" data-visible="false" data-class="number">Add to Cart</th>
            <th data-field="checkout" data-visible="false" data-class="number">Checkout</th>
            <th data-field="purchase" data-visible="false" data-class="number">Purchase</th>

            <th data-field="checkout_value" data-visible="false" data-class="number">Checkout Value</th>
            <th data-field="purchase_value" data-visible="true" data-class="number">Purchase Value</th>
            <th data-field="post_engagement" data-visible="false" data-class="number">Post Engagement</th>
            <th data-field="spend" data-class="number" data-visible="false">Marketing Cost</th>
            <th data-field="rate" data-class="number" data-visible="false">Purchase Rate</th>
        </tr>
        </thead>
    </table>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#filter-product').select2({theme: "bootstrap"});
        $('#filter-country').select2({theme: "bootstrap"});
        $('#table-report').bootstrapTable({
            "cookieStorage": "localStorage"
        });

        $('#table-report').on('load-success.bs.table', function (e, data) {
            console.log(data);
            return;
            var total_count = 0;
            for (var i = 0; i < data.length; i++) {
                var _data = data[i];
                if (_data['value'] == 'Total') {
                    total_count = _data['count'];
                }
            }
            var other_count = 0;
            var data_value = [];
            var data_category = [];
            var data_series = [];
            for (var i = 0; i < data.length; i++) {
                var _data = data[i];
                if (_data['value'] == 'Total') {
                    continue;
                }
                var _count = _data['count'];
                if (_count < total_count / 100 || i >= 10) {
                    other_count += _count;
                } else {
                    data_value[data_value.length] = {
                        name: _data['value'],
                        y: _data['count']
                    };
                }
            }
            data_value[data_value.length] = {
                name: 'Other',
                y: other_count
            };

            for (var i = data.length - 1; i >= 0; i--) {
                var _data = data[i];
                if (_data['value'] == 'Total') {
                    continue;
                }
                data_category[data_category.length] = _data['value'].substring(5);
                data_series[data_series.length] = _data['count'];
            }
			<?php if($view_by == 'date') { ?>
            Highcharts.chart('chart_detail', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Device Info'
                },
                xAxis: {
                    categories: data_category
                },
                yAxis: {
                    title: {
                        text: 'Device Count'
                    },
                    min: 0
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Device',
                    data: data_series
                }]
            });
			<?php } else { ?>
            // Build the chart
            Highcharts.chart('chart_detail', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Report Detail'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Percent',
                    colorByPoint: true,
                    data: data_value
                }]
            });
			<?php } ?>
        });
        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });


    });
</script>