<form class="form-inline filter-form" action="report-platform?" method="get">
	<div class="form-group" style="position: relative">
		<input id="date-range" type="text" class="form-control" style="min-width: 200px; width: 100%;  cursor: pointer" name="filter[date_range]"
			   value="<?php echo $start_date . '-' . $end_date ?>">
		<i class="fa fa-calendar" style="cursor: pointer; position: absolute; right: 10px; top: auto; bottom: 10px;"></i>
	</div>


    <div class="form-group">
        <select class="form-control auto_submit" name="filter[platform]">
            <option value="">All Platform</option>
			<?php
			if(count($reportCountry) > 0) {
				foreach ($reportCountry as $_country) {
					$selected = '';
					if ($_country['id'].'' == $filter_platform) {
						$selected = ' selected="selected" ';
					}
					echo '<option value="'.$_country['id'].'" '.$selected.'>'.$_country['name'].' (' .$_country['amount'].')</option>';
				}
			}
			?>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control auto_submit" name="filter[product]">
            <option value="">All Product</option>
			<?php
			if(count($reportProduct) > 0) {
				foreach ($reportProduct as $_product) {
					$selected = '';
					if ($_product['id'].'' == $filter_product) {
						$selected = ' selected="selected" ';
					}
					echo '<option value="'.$_product['id'].'" '.$selected.'>'.$_product['name'].' (' .$_product['amount'].')</option>';
				}
			}
			?>
        </select>
    </div>


	<div class="form-group">
		<label>View By</label>
		<select class="form-control" name="view_by">
			<option value="publisher_platform" <?php echo $view_by == 'publisher_platform'?' selected="selected" ':'' ?>>Platform</option>
			<option value="product_id" <?php echo $view_by == 'product_id'?' selected="selected" ':'' ?>>Product</option>
            <option value="date" <?php echo $view_by == 'date'?' selected="selected" ':'' ?>>Date</option>
            <option value="month" <?php echo $view_by == 'month'?' selected="selected" ':'' ?>>Month</option>
		</select>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-info">Filters</button>
	</div>
</form>
<div style="overflow: auto">
	<table class="table table-striped table-advance table-hover table-bordered" id="table-report"
		   data-show-refresh="true"
		   data-show-toggle="true"
		   data-show-columns="true"
		   data-mobile-responsive="true"
		   data-show-export="true"
		   data-pagination="true"
		   data-cookie="true"
		   data-cookie-id-table="report_list"
		   data-pagination-v-align="both"
		   data-page-size="25"
		   data-sortable="true"
		   data-page-list="[10,25,50,100, 500, 1000, 5000]"
		   data-url="<?php echo $url_page;?>"
	>
		<thead>
		<tr>
			<th data-field="id" data-visible="false"  data-sortable="true"> ID</th>
			<th data-field="name" data-visible="true"  data-sortable="true"> Name</th>
			<th data-field="impressions"  data-visible="false" data-class="number"  data-sortable="true">Impressions</th>
			<th data-field="inline_link_clicks"  data-visible="false" data-class="number"  data-sortable="true">Click</th>
            <th data-field="ctr"  data-visible="false" data-class="number"  data-sortable="true">CTR</th>
            <th data-field="sell_order"  data-class="number"  data-sortable="true">Sell Order</th>
            <th data-field="cvr"  data-visible="false" data-class="number"  data-sortable="true">CVR</th>
            <th data-field="sell_quantity"  data-class="number"  data-sortable="true">Quantity</th>
            <th data-field="avg_item"  data-class="number"  data-visible="false" data-sortable="true">Item Per Order</th>

            <th data-field="ecpc"  data-class="number"  data-visible="false" data-sortable="true">eCPC</th>
            <th data-field="ecpa"  data-class="number"  data-visible="false" data-sortable="true">eCPA</th>


			<th data-field="sell_amount"  data-class="number"  data-sortable="true">Revenue</th>
            <th data-field="sell_discount"  data-visible="false"  data-class="number"  data-sortable="true">Sell Discount</th>
            <th data-field="est_total_spend"  data-class="number"  data-sortable="true">Estimate Total Spend</th>
			<th data-field="spend"  data-class="number"  data-visible="false" data-sortable="true">Marketing Cost</th>
			<th data-field="est_buy_amount"  data-class="number"  data-visible="false" data-sortable="true">Estimate Buy Amount</th>
			<th data-field="est_shipping_cost"  data-class="number"  data-visible="false" data-sortable="true">Estimate Shipping Cost</th>
            <th data-field="payment_fee"  data-class="number"  data-visible="false" data-sortable="true">Payment Fee</th>

			<th data-field="est_profit"  data-class="number"  data-sortable="true">Estimate Profit</th>
            <th data-field="est_roi"  data-class="number"  data-sortable="true">Estimate Margin</th>


		</tr>
		</thead>
	</table>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#table-report').bootstrapTable({"cookieStorage" : "localStorage",
            customSort: function (sortName, sortOrder) {
                if (!sortName || !sortOrder) {
                    return;
                }
                this.data.sort(function(a, b) {
                    var x = a[sortName];
                    var y = b[sortName];
                    if (sortName != 'name' && sortName != 'id') {
                        x = x.replace(',','');
                        y = y.replace(',','');
                        x = parseFloat(x);
                        y = parseFloat(y);
                        if (isNaN(x)) {
                            x = 0;
                        }
                        if (isNaN(y)) {
                            y = 0;
                        }
                    }
                    if (sortOrder == 'desc') {
                        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
                    } else {
                        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                    }

                });
            }});

        $('#table-report').on('load-success.bs.table', function (e, data) {
            console.log(data);
            return;
            var total_count = 0;
            for (var i = 0; i < data.length; i++) {
                var _data = data[i];
                if (_data['value'] == 'Total') {
                    total_count = _data['count'];
                }
            }
            var other_count = 0;
            var data_value = [];
            var data_category = [];
            var data_series = [];
            for (var i = 0; i < data.length; i++) {
                var _data = data[i];
                if (_data['value'] == 'Total') {
                    continue;
                }
                var _count = _data['count'];
                if (_count < total_count / 100 || i >= 10) {
                    other_count += _count;
                } else {
                    data_value[data_value.length] = {
                        name: _data['value'],
                        y: _data['count']
                    };
                }
            }
            data_value[data_value.length] = {
                name: 'Other',
                y: other_count
            };

            for (var i = data.length - 1; i >= 0; i--) {
                var _data = data[i];
                if (_data['value'] == 'Total') {
                    continue;
                }
                data_category[data_category.length] = _data['value'].substring(5);
                data_series[data_series.length] = _data['count'];
            }
			<?php if($view_by == 'date') { ?>
            Highcharts.chart('chart_detail', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Device Info'
                },
                xAxis: {
                    categories: data_category
                },
                yAxis: {
                    title: {
                        text: 'Device Count'
                    },
                    min: 0
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Device',
                    data: data_series
                }]
            });
			<?php } else { ?>
            // Build the chart
            Highcharts.chart('chart_detail', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Report Detail'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Percent',
                    colorByPoint: true,
                    data: data_value
                }]
            });
			<?php } ?>
        });
        $('#date-range').daterangepicker({
            "startDate": new Date("<?php echo $start_date ?>"),
            "endDate": new Date("<?php echo $end_date ?>"),
            locale: {
                format: 'YYYY-MM-DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#date-range').val(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
            $('.filter-form').submit();
        });


    });
</script>