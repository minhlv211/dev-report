<?php
class MobileHelper
{
	static function fixMobileNumber($mobile)
	{
		if (empty($mobile)) {
			return null;
		}
		if ($mobile{0} == '+') {
			$mobile = substr($mobile, 1);
		}
		$mobile = trim($mobile);
		$new_mobile = preg_replace('/[^0-9]/', '', $mobile);
		if (empty($new_mobile)) {
			return null;
		}
		$start_at = substr($new_mobile, 0, 2);
		if ($start_at == '84') {
			$new_mobile = substr($new_mobile, 2);
		}

		if ($new_mobile{0} !== '0') {
			$new_mobile = '0' . $new_mobile;
		}
		$start_at = substr($new_mobile, 0, 2);
		$start_at3 = substr($new_mobile, 0, 3);
		if ($start_at !== '01' && $start_at !== '09' && $start_at3 !== '086' && $start_at3 !== '089' && $start_at3 !== '088') {
			return null;
		}
		if ($start_at == '01' && strlen($new_mobile) != 11) {
			return null;
		}
		if ($start_at == '09' && strlen($new_mobile) != 10) {
			return null;
		}
		if ($start_at == '08' && strlen($new_mobile) != 10) {
			return null;
		}
		return $new_mobile;
	}

	static function getCarrierFormNumber($number) {
		if (empty($number)) {
			return null;
		}
		$viettel_list = array('098','097','096','0169','0168','0167','0166','0165','0164','0163','0162', '0161', '086');
		$mobifone_list = array('090','093','0120','0121','0122','0126','0128', '089');
		$vinaphone_list = array('091','094','0123','0124','0125','0127','0129', '088');
		$vnmobile_list = array('092', '0188', '0186', '0188');
		$gphone_list = array('0993','0994','0995','0996','099', '0199');

		if (self::_getCarrierFormNumber($number, $viettel_list)) {
			return 'viettel';
		}
		if (self::_getCarrierFormNumber($number, $mobifone_list)) {
			return 'mobifone';
		}
		if (self::_getCarrierFormNumber($number, $vinaphone_list)) {
			return 'vinaphone';
		}
		if (self::_getCarrierFormNumber($number, $vnmobile_list)) {
			return 'vnmobile';
		}
		if (self::_getCarrierFormNumber($number, $gphone_list)) {
			return 'gphone';
		}

		if (preg_match('/0[0-9]{9,10}/', $number)) {
			return 'FixMobile';
		}

		return 'other';
	}
	static function _getCarrierFormNumber($number, $list_prefix) {
		foreach ($list_prefix as $_prefix) {
			$length = strlen($_prefix);
			if (substr($number, 0, $length) == $_prefix) {
				return true;
			}
		}
		return false;
	}
}