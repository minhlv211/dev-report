<?php
class CarrierHelper {
	static function getList() {
		static $carrierList = null;
		if (empty($carrierList)) {
			$trackingModel = new TrackingNumberModel();
			$carrierListRaw = $trackingModel->getCarrierList();
			$carrierList = array();
			foreach ($carrierListRaw as $_carrier) {
				$_code = $_carrier['code'];
				$_name = $_carrier['name'];
				$carrierList[$_code] = $_name;
			}
		}
		return $carrierList;
	}

	static function getListRaw() {
		static $carrierList = null;
		if (empty($carrierList)) {
			$trackingModel = new TrackingNumberModel();
			$carrierListRaw = $trackingModel->getCarrierList();
			$carrierList = array();
			foreach ($carrierListRaw as $_carrier) {
				$_code = $_carrier['code'];
				$carrierList[$_code] = $_carrier;
			}
		}
		return $carrierList;
	}

	static function getCarrierInfo($code) {
		$carrierList = self::getListRaw();
		if (isset($carrierList[$code])) {
			return $carrierList[$code];
		}
		return null;

	}

	static function code2Name($code) {
		$carrierList = self::getListRaw();
		if (isset($carrierList[$code])) {
			return $carrierList[$code]['name'];
		}
		return '';
	}

	static function code2Phone($code) {
		$carrierList = self::getListRaw();
		if (isset($carrierList[$code])) {
			return $carrierList[$code]['phone'];
		}
		return '';
	}
	static function code2Url($code) {
		$carrierList = self::getListRaw();
		if (isset($carrierList[$code])) {
			return $carrierList[$code]['homepage'];
		}
		return '';
	}

}