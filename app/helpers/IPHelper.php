<?php

class IPHelper
{
	static function getRealIp()
	{
		$list_ip = self::getIpList();
		return trim($list_ip[0]);
	}

	static function getIp()
	{
		return $_SERVER['REMOTE_ADDR'];
	}

	static function getIpList()
	{
		$x_real_ip = $_SERVER['HTTP_X_REAL_IP'];
		$forward_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
		$last_ip = $_SERVER['REMOTE_ADDR'];

		$list_ip = array();
		if ($forward_for) {
			if (strpos($forward_for, ',') !== false) {
				$forward_for = explode(',', $forward_for);
				foreach ($forward_for as $_ip) {
					$list_ip[] = trim($_ip);
				}
			} else {
				$list_ip[] = trim($forward_for);
			}
		}
		if ($x_real_ip) {
			if (strpos($x_real_ip, ',') !== false) {
				$x_real_ip = explode(',', $x_real_ip);
				foreach ($x_real_ip as $_ip) {
					$list_ip[] = trim($_ip);
				}
			} else {
				$list_ip[] = trim($x_real_ip);
			}
		}

		$list_ip[] = $last_ip;

		foreach ($list_ip as $_k => $_ip) {
			$_ip = trim($_ip);
			if (self::is_private_ip($_ip)) {
				unset($list_ip[$_k]);
			}
		}
		$list_ip = array_values($list_ip);
		if (count($list_ip) == 0) {
			$list_ip = array('127.0.0.1');
		}
		return $list_ip;
	}

	static function is_private_ip($_ip)
	{
		$_ip = explode('.', $_ip);
		if (count($_ip) != 4) {
			return true;
		}
		if ($_ip[0] == 127 || $_ip[0] == 10) {
			return true;
		}
		if ($_ip[0] == 192 && $_ip[1] == 168) {
			return true;
		}
		if ($_ip[0] == 172 && $_ip[1] >= 16 && $_ip[1] <= 31) {
			return true;
		}
		return false;
	}
}