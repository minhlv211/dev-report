<?php

class OSHelper
{
	function getOSInfo($user_agent)
	{

		$ua = $user_agent; /* User Agent of Browser */

		$ubuntu = "/Ubuntu/"; /* Ubuntu */
		$debian = "/Debian/"; /* Debian */
		$cros = "/CrOS/"; /* Debian */
		$linux = "/(X11|Linux)/"; /* Linux */
		$ios = "/iPhone|biPhone.*Mobile|\biPod|\biPad/"; /* iOS */
		$windows = "/(Windows NT)/"; /* Windows */
		$window_phone = "/Windows Phone/"; /* Windows */
		$macos = "/Mac OS X/"; /* Mac OS X */
		$android = "/Android|Adr/"; /* Android */
		$symbian = "/SymbianOS|Series40|Series60|Series 60|Series 40|Symbian|S40|S60/"; /* Java desctop Web Browser */
		$java = "/J2ME\/|\bMIDP\\b|\bCLDC\b/"; /* Java desctop Web Browser */
		/* pattern part end */
		if (preg_match($window_phone, $ua)) {
			$os_info['title'] = 'Windows Phone';
			$v = "/Windows Phone [0-9_.]{2,10}/";
			preg_match($v, $ua, $result);

			$version = $result[0];
			$version = str_replace('Windows Phone ', '', $version);
			$version = str_replace('_', '.', $version);
			$os_info['version'] = $version;

		} elseif (preg_match($android, $ua)) {
			$os_info['title'] = 'Android';
			$v = '/Android [0-9_.]{2,10}/';
			if (preg_match($v, $ua, $result)) {
				preg_match($v, $ua, $result);
				$version = $result[0];
				$version = str_replace('Android ', '', $version);
				$os_info['version'] = $version;
			}
			if (empty($os_info['version'])) {
				$v = '/Adr [0-9_.]{2,10}/';
				if (preg_match($v, $ua, $result)) {
					preg_match($v, $ua, $result);
					$version = $result[0];
					$version = str_replace('Adr ', '', $version);
					$os_info['version'] = $version;
				}
			}
			if (empty($os_info['version'])) {
				$v = '/Android\/[0-9_.]{2,10}/';
				if (preg_match($v, $ua, $result)) {
					preg_match($v, $ua, $result);
					$version = $result[0];
					$version = str_replace('Android/', '', $version);
					$os_info['version'] = $version;
				}
			}
			if (empty($os_info['version'])) {
				$v = '/MocorDroid[0-9_.]{2,10}/';
				if (preg_match($v, $ua, $result)) {
					preg_match($v, $ua, $result);
					$version = $result[0];
					$version = str_replace('MocorDroid', '', $version);
					$os_info['version'] = $version;
				}
			}
		} elseif (preg_match($ios, $ua)) {
			$os_info['title'] = 'iOs';
			$v = "/OS [0-9_]{2,10}/";
			preg_match($v, $ua, $result);
			$version = $result[0];
			$version = str_replace('OS ', '', $version);
			$version = str_replace('_', '.', $version);
			$os_info['version'] = $version;
		} elseif (preg_match($symbian, $ua)) {
			$os_info['title'] = 'Symbian';
		} elseif (preg_match($java, $ua)) {
			$os_info['title'] = 'Java';
		} elseif (preg_match($ubuntu, $ua)) {
			$os_info['title'] = 'Ubuntu';
		} elseif (preg_match($debian, $ua)) {
			$os_info['title'] = 'Debian';
		} elseif (preg_match($cros, $ua)) {
			$os_info['title'] = 'Chrome OS';
		} elseif (preg_match($windows, $ua)) {
			$os_info['title'] = 'Windows';
			$v = '/Windows NT [0-9_.]{2,10}/';
			if (preg_match($v, $ua, $result)) {
				preg_match($v, $ua, $result);
				$version = $result[0];
				$version = str_replace('Windows NT ', '', $version);
				if ($version == '5.1') {
					$version = 'XP';
				}
				if ($version == '6.1') {
					$version = '7';
				}
				if ($version == '6.2') {
					$version = '8';
				}
				if ($version == '6.3') {
					$version = '8.1';
				}
				if ($version == '10.0') {
					$version = '10';
				}

				$os_info['version'] = $version;
			}

		} elseif (preg_match($macos, $ua)) {
			$os_info['title'] = 'OSX';
			$v = "/Mac OS X [0-9_.]{2,10}/";
			preg_match($v, $ua, $result);
			$version = $result[0];
			$version = str_replace('Mac OS X ', '', $version);
			$version = str_replace('_', '.', $version);
			$os_info['version'] = $version;

		} elseif (preg_match($linux, $ua)) {
			$os_info['title'] = 'Linux';
		} else {
			$os_info['title'] = 'Other';
		}

		return $os_info;
	}

	function getModel()
	{

	}

	function getDeviceName($ua, $width) {
		$android = "/Android|Adr/";
		$iPhone = "/iPhone/";
		$iPad = "/iPad/";
		$iPod = "/iPod/";
		$pc = "/Windows NT|Mac OS X|Debian|Ubuntu/";
		if (preg_match($iPad, $ua)) {
			return 'ipad';
		}
		if (preg_match($iPhone, $ua)) {
			return 'iphone';
		}
		if (preg_match($iPod, $ua)) {
			return 'ipod';
		}
		if (preg_match($pc, $ua)) {
			return 'desktop';
		}
		if (preg_match($android, $ua)) {
			if ($width > 480) {
				return 'android_tablet';

			} else {
				return 'android_smartphone';
			}
		}
		return 'other';
	}
}