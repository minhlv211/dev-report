<?php
class ObjectHelper {
	static function get($pid_list) {
		if (!is_array($pid_list)) {
			$pid_list = explode(',', $pid_list);
		}
		if (count($pid_list) > 50) {
			return array('status' => 0, 'errror_msg' => 'Maximum 50 item per time.');
		}
		$url_content = CrawlerHelper::build_url('page', $pid_list);
		$url = $url_content['url'];
		$post_data = $url_content['data'];

		$raw_data = CrawlerHelper::call_url($url, $post_data);
		$page_list = array();
		if (count($raw_data) > 0) {
			foreach ($raw_data as $_index => $_raw) {
				$pid = $pid_list[$_index];
				$page_list[$pid] = json_decode($_raw['body'], true);
			}
		}
		return $page_list;
	}
}