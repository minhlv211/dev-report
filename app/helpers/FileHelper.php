<?php
class FileHelper {
	static function scan($path) {
		if (!is_dir($path) || !is_readable($path)) {
			return false;
		}

		$total_file = $total_dir = $file_size = 0;

		$list = scandir($path);
		foreach ($list as $_p) {
			if ($_p == '.' || $_p == '..') {
				continue;
			}

			$file = $path . '/' . $_p;

			if (is_file($file)) {
				$total_file += 1;
				$file_size += filesize($file);
			}
			if (is_dir($file)) {
				$total_dir += 1;
				$result = self::scan($file);

				$total_file += $result['file'];
				$total_dir += $result['dir'];
				$file_size += $result['size'];
			}
		}
		return array(
			'file' => $total_file,
			'dir' => $total_dir,
			'size' => $file_size,
		);
	}
}