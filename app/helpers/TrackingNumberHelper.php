<?php
class TrackingNumberHelper  {
	static function updateListTracking($page = 1, $limit = 2000) {

		$_trackingModel = new TrackingNumberModel();
		$track = new Trackingmore;
		$tracking_list = $track->getTrackingsList($page, $limit);
		$tracking_list = $tracking_list['data']['items'];

		foreach ($tracking_list as $_tracking) {
			$_trackingModel->updateTracking($_tracking);

		}
		return $tracking_list;
	}

	static  function updateRealtimeTracking($tracking_number) {
		require_once ROOT_DIR . '/app/models/TrackingNumberModel.php';
		$track = new Trackingmore;
		$carrierCode = $track->detectCarrier($tracking_number);
		$carrierCode = $carrierCode['data'][0]['code'];
		if (empty($carrierCode)) {
			return false;
		}
		$trackingInfo = $track->getRealtimeTrackingResults($carrierCode, $tracking_number);
		$trackingInfo = $trackingInfo['data']['items'][0];
		if (empty($trackingInfo)) {
			return false;
		}
		$_trackingModel = new TrackingNumberModel();
		return $_trackingModel->updateTracking($trackingInfo);
	}

	static function addTracking($tracking_number) {
		$track = new Trackingmore;
		if (!$tracking_number) {
			return false;
		}
		$trackingModel = new TrackingNumberModel();
		$tracking_info = $trackingModel->getTracking($tracking_number);
		if ($tracking_info) {
			return false;
		}
		$track_1 = $track->detectCarrier($tracking_number);
		$code = $track_1['data'][0]['code'];
		$tracking_info = array(
			'tracking_number' => $tracking_number,
			'status' => 'pending',
		);
		$trackingModel->updateTracking($tracking_info);

		if (empty($code)) {
			return false;;
		}
		$extraInfo = array();
		$extraInfo['tracking_number']= $tracking_number;
		$extraInfo['carrier_code']   = $code;
		$extraInfo['title']          = '';
		$extraInfo['customer_name']  = '';
		$extraInfo['customer_email'] = '';
		$extraInfo['order_id']       = '';

		return $track->createTracking($code, $tracking_number);
	}

	static function updateCarrierList() {
		$track = new Trackingmore;
		$carrierList = $track->getCarrierList();
		$carrierList = $carrierList['data'];
		require_once ROOT_DIR . '/app/models/TrackingNumberModel.php';
		$trackingModel = new TrackingNumberModel();
		$trackingModel->updateCarrierList($carrierList);
	}

	static function getCarrierList() {
		require_once ROOT_DIR . '/app/models/TrackingNumberModel.php';
		$trackingModel = new TrackingNumberModel();
		return $trackingModel->getCarrierList();
	}

	static function getStatusHtml($status) {
		$status = trim($status);
		if (strip_tags($status) != $status) {
			return $status;
		}
		switch ($status) {
			case 'transit':
				$status = '<span class="label  label-info">Transit</span>';
				break;
			case 'delivered':
				$status = '<span class="label  label-success">Delivered</span>';
				break;
			default:
				$status = '<span class="label  label-warning">'.ucfirst($status).'</span>';
				break;
		}
		return $status;
		//
	}
}