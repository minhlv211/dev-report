<?php



class GeoHepper
{

    static private $_gi;
    /**
     * @var GeoHepper
     */
    static private $instance;

    private function __construct()
    {
    }

    static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new GeoHepper();
        }
        return self::$instance;

    }

    static private function getGi()
    {
        if (empty(self::$_gi)) {
            include_once ROOT_DIR . '/libs/geoip/geoipcity.inc';
            self::$_gi = geoip_open(ROOT_DIR . "/libs/geoip/GeoLiteCity.dat", GEOIP_STANDARD);
        }
        return self::$_gi;
    }

    function getFullInfo($ip, $cacheDB = true)
    {
        static $infoList = array();

        if (!$this->checkIpFormat($ip)) {
            return array();
        }

        $gi = self::getGi();
        $infoList[$ip] = (array)geoip_record_by_addr($gi, $ip);

        return $infoList[$ip];
    }

    function getCountryCode($ip = null, $cacheDB = true)
    {
        if (substr($ip, 0, 7) == '168.90.') {
            return 'br';
        }
        if (empty($ip)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if ($ip == '127.0.0.1') {
            $country_code = 'vn';
        } else {
            $info = $this->getFullInfo($ip, $cacheDB);
            $country_code = $info['country_code'];
            $country_code = strtolower($country_code);
        }

        return $country_code;
    }

    function getCountryCode3($ip = null, $cacheDB = true)
    {
        $info = $this->getFullInfo($ip, $cacheDB);
        return strtolower($info['country_code3']);
    }

    function getCountryName($ip = null, $cacheDB = true)
    {
        $info = $this->getFullInfo($ip, $cacheDB);
        return $info['country_name'];
    }

    function getCity($ip = null)
    {
        $info = $this->getFullInfo($ip, false);
        return $info['city'];
    }

    function getRegion($ip = null, $cacheDB = true)
    {
        $info = $this->getFullInfo($ip, $cacheDB);
        return $info['region'];
    }

    function getRealIp()
    {

    }

    function destroy()
    {
        if (!empty(self::$_gi)) {

            geoip_close(self::$_gi);
        }
    }

    private function checkIpFormat($ip) {
        if (!is_string($ip)) {
            return false;
        }
        $ip_exp = explode('.', $ip);
        if (count($ip_exp) != 4) {
            return false;
        }
        foreach ($ip_exp as $_ip_item) {
            if (!preg_match('/[0-9]{1,3}/', $_ip_item)) {
                return false;
            }
            $_ip_item = intval($_ip_item);
            if ($_ip_item < 0 || $_ip_item > 255) {
                return false;
            }
        }
        return true;
    }
}

register_shutdown_function('close_geoip');
function close_geoip()
{
    GeoHepper::getInstance()->destroy();
}
