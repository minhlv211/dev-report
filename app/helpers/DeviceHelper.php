<?php
class DeviceHelper {
	static function androidVersionConvert($api_version) {
		if($api_version < 14) {
			return 'Gingerbread 2.3';
		}
		if($api_version >= 14 && $api_version <= 15) {
			return 'Ice Cream Sandwich 4.0.x';
		}
		if($api_version >= 16 && $api_version <= 18) {
			return 'Jelly Bean 4.1-4.3';
		}
		if($api_version >= 19 && $api_version <= 20) {
			return 'KitKat 4.4+';
		}
		if($api_version >= 21 && $api_version <= 22) {
			return 'Lollipop 5.x';
		}
		if($api_version == 23) {
			return 'Marshmallow 6.x';
		}
		if($api_version >= 24) {
			return 'Nougat 7.x';
		}
		return $api_version;
	}

	static function fixResolution($resolution) {
		$resolution_arr = explode('x', $resolution);
		$width = $resolution_arr[0];
		$height = $resolution_arr[1];
		if ($width > $height) {
			$width = $height;
		}
		if ($width > 1400) {
			return '2K';
		}
		if ($width > 1000) {
			return 'Full HD';
		}
		if ($width >= 800) {
			return '800x1280';
		}
		if ($width >= 720) {
			return '720x1280';
		}
		if ($width >= 540) {
			return '540x960';
		}

		if ($width >= 400) {
			return '480x800';
		}

		if ($width < 400) {
			return '320x480';
		}

		switch ($width) {
			case '720':
			case '768':
				return '720x1280';
			case '800':
				return '800x1280';
			case '1440':
			case '1536':
				return '2K';
			case '480':
			case '470':
				return '480x800';
			case '540':
			case '600':
				return '540x960';
			case '240':
				return '240x320';
			case '320':
				return '320x480';
		}

		return $resolution;
	}

	static function isTestDevice($deviceInfo) {
		$ip = $deviceInfo['ip'];
		$device_name = $deviceInfo['device_name'];
		if ($ip == '27.72.100.10') {
			return true;
		}

		if (substr($ip, 0, 4) == '104.') {
			return true;
		}
		if(strpos($device_name, 'x86') !== false) {
			return true;
		}
		return false;
	}

	static function checkBlockGCM($deviceInfo) {
		$ip = $deviceInfo['ip'];
		$country = $deviceInfo['country'];
		$device_name = $deviceInfo['device_name'];
		if (substr($ip, 0, 4) == '104.') {
			return true;
		}
		if(strpos($device_name, 'x86') !== false) {
			return true;
		}
		if ($country == 'vn' && $ip != '27.72.100.10') {
			return true;
		}

		return false;
	}

	static function isValidToken($token) {
		if (strlen($token) < 120) {
			return false;
		}
		if (strpos($token, 'java.lang.') !== false) {
			return false;
		}

		return true;
	}
}