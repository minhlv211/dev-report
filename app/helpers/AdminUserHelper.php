<?php

/**
*
*/
class AdminUserHelper {
    const ROLE_ADMIN        = 'admin';
    const ROLE_CUSTOM       = 'customer';
    const CRYPT_PASSWORD       = 'ECO@123456789999';
    
    function __construct(){
        $this->user  =   array();
        $username = self::get_current_user();
        $username = $username['username'];
        
        $db     = EcoDb::get_instance()->getDb();
        $user_collection    = $db->admin_user;
        $query  = array('username' => $username);
        $user     = $user_collection->findOne($query);
        $this->user = $user;
    }
    
    public static function get_list_role(){


		$contents = parse_ini_file(ROOT_DIR . '/routes.ini');
		$listPermistion = array();
		foreach ($contents as $_key => $_value) {
			$_key = explode('/', $_key);
			$_key = $_key[1];
			$_value_arr = explode('->', $_value);
			$_main_key = $_value_arr[0];
			if (!isset($listPermistion[$_main_key])) {
				$listPermistion[$_main_key] = array(
					'label' => $_main_key,
					'list' => array()
				);
			}
			$listPermistion[$_main_key]['list'][$_value_arr[1]] = $_value;
		}

		return $listPermistion;


        return array(
            'dashboard' => array(
                'label' => 'Dashboard',
                'list' => array(
                    'index' => 'Xem'
                )
            ),
            'adminuser' => array(
                'label' => 'User',
                'list' => array(
                    'index' => 'Danh sách tài khoản',
                    'create' => 'Tạo tài khoản',
                    'edit' => 'Sửa thông tin',
                    'delete' => 'Xóa'
                )
            ),
            'lead' => array(
                'label' => 'Lead',
                'list' => array(
                    'index' => 'Lead Listing',
                    'statistic' => 'Lead Statistic',
                    'score' => 'Lead Scoring',
                    'lead_import' => 'Lead import',
                    'lead_export' => 'Lead export'
                )
            ),
            'pushnotification' => array(
                'label' => 'Push notification',
                'list' => array(
                    'index' => 'Danh sách đăng ký',
                    'create' => 'Gửi push notification',
                    'listSend' => 'Danh sách gửi',
                    'delete' => 'Xóa',
                    'changeStatusSchedule' => 'Trạng thái đặt lịch'
                )
            ),
            'tracking' => array(
                'label' => 'Tracking',
                'list' => array(
                    'index' => 'Report',
                    'qualityReport' => 'Quality Report',
                    'leadDetail' => 'Lead Detail',
                    'jsConfig' => 'Automation Config'
                )
            ),
            'reportads' => array(
                'label' => 'Report ads',
                'list' => array(
                    'index' => 'Report',
                    'report_daily' => 'Daily Report',
                    'report_detail' => 'Insight Report',
                    'report_hourly' => 'Hourly Report',
                    'group_link' => 'Group URL',
                    'list_token' => 'Token FB ads, GG ads'
                )
            )
        );
    }
    
    public static function get_current_user(){
        $user_info  = isset($_SESSION['user']) ? $_SESSION['user'] : null;
        return $user_info;
    }
    
    public static function refresh_current_user_info($info){
        $_SESSION['user'] = $info;
    }
    
    public function is($role){
        $current_user   = self::get_current_user();
        if ($current_user['role'] === $role) return true;
        return false;
    }
    
    public function has_permission_group($group){
        $user   = $this->user;
        if($user){
            if ($user['role'] === self::ROLE_ADMIN) return true;
            $permissions    = $user['permissions'];
            if (count($permissions)){
                $flag = false;
                foreach ($permissions as $permission){
                    $arr = explode('.', $permission);
                    if ($arr[0] == $group) {
                        $flag = true;
                        break;
                    }
                }
                return $flag;
            }else{
                return false;
            }
        }
    }
    
    public function has_permission($permission, $user = null){
        if (!isset($user)) {
            $user   = $this->user;
        }
        if($user){
            if ($user['role'] === self::ROLE_ADMIN) return true;
            $permissions    = (array) $user['permissions'];
            if (count($permissions)){
                if (in_array($permission, $permissions)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
    
    public function is_admin(){
        $user   = $this->user;
        if($user && $user['role'] === self::ROLE_ADMIN){
            return true;
        }
        return false;
    }
}