<?php

class PagerHelper {

    static function generateHtml($page, $total_page, $sub_url, $className = 'pull-left') {
        $htmlItem = array();
        $htmlItem[] = '<div style="margin: 0 0 10px;" class="'. $className .' pagination">';
        $htmlItem[] = '<ul style="margin: 0;" class="pagination">';
        $htmlItem[] = '<li class="page-first ' . ($page <= 1 ? 'disabled' : '') . '"><a href="' . $sub_url . '&page=1">«</a></li>';
        $htmlItem[] = '<li class="page-pre ' . ($page <= 1 ? 'disabled' : '') . '"><a href="' . $sub_url . '&page=' . ($page - 1 > 0 ? $page - 1 : 1) . '">‹</a></li>';
        if ($page > 5) {
            $htmlItem[] = '<li class="page-number"><a href="' . $sub_url . '&page=1">1</a></li>';
            $htmlItem[] = '<li class="page-number"><a href="javascript:;">...</a> </li>';
        }

        for ($i = -3; $i <= 3; $i++) {
            $index_page = $page + $i;
            if ($index_page < 1 || $index_page > $total_page) {
                continue;
            }
            if ($index_page == $page) {
                $htmlItem[] = '<li class="page-number active"><a href="' . $sub_url . '&page=' . $index_page . '">' . number_format($index_page) . '</a></li>';
            } else {
                $htmlItem[] = '<li class="page-number"><a href="' . $sub_url . '&page=' . $index_page . '">' . number_format($index_page) . '</a></li>';
            }
        }

        if ($page < $total_page - 5) {
            $htmlItem[] = '<li class="page-number"><a href="javascript:;">...</a> </li>';
            $htmlItem[] = '<li class="page-number"><a href="' . $sub_url . '&page=' . $total_page . '">' . number_format($total_page) . '</a></li>';
        }

        $htmlItem[] = '<li class="page-next ' . ($page >= $total_page ? 'disabled' : '') . '"><a href="' . $sub_url . '&page=' . ($page + 1 <= $total_page ? $page + 1 : $total_page) . '">›</a></li>';
        $htmlItem[] = '<li class="page-last ' . ($page >= $total_page ? 'disabled' : '') . '"><a href="' . $sub_url . '&page=' . number_format($total_page) . '">»</a></li>';

        $htmlItem[] = '</ul>';
        $htmlItem[] = '</div>';


        return join("\n", $htmlItem);
    }
}

//<div class="pull-left pagination">
//    <ul class="pagination">
//        <li class="page-first disabled"><a href="#">«</a></li>
//        <li class="page-pre disabled"><a href="#">‹</a></li>
//        <li class="page-number active"><a href="#">1</a></li>
//        <li class="page-next disabled"><a href="#">›</a></li>
//        <li class="page-last disabled"><a href="#">»</a></li>
//    </ul>
//</div>