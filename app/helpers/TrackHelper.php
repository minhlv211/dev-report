<?php

class TrackHelper
{
	function getTrackingList($page = 1, $limit = 200)
	{
		$key = 'dd8d5c9f-e936-474d-b8be-b15841b79259';
//		$couriers = new AfterShip\Couriers($key);
		$trackings = new AfterShip\Trackings($key);
//		$last_check_point = new AfterShip\LastCheckPoint($key);
		$tracks = $trackings->all(array('page' => $page, 'limit' => $limit, 'lang' => 'en'));
		$tracks = $tracks['data']['trackings'];
		$needField = array(
			'id', 'created_at', 'updated_at', 'last_updated_at', 'tracking_number',
			'slug', 'active', 'delivery_time', 'destination_country_iso3', 'expected_delivery',
			'order_id', 'shipment_package_count', 'shipment_pickup_date',
			'shipment_delivery_date', 'shipment_type', 'shipment_weight', 'shipment_weight_unit',
			'tag', 'title', 'tracked_count', 'checkpoints'
		);
		$needField2 = array(
			'slug', 'checkpoint_time', 'location', 'message', 'tag'
		);
		foreach ($tracks as $_k => $_track) {
			var_dump($_track['tracking_number']);
			foreach ($_track as $_l => $_v) {
				if (!in_array($_l, $needField)) {
					unset($_track[$_l]);
				}
			}

			$start_time = $end_time = '';
			if (count($_track['checkpoints']) > 0) {
				$_checkpoint_source = $_checkpoint_destination = array();
				foreach ($_track['checkpoints'] as $_l => $_checkpoint) {
					foreach ($_checkpoint as $_m => $_v) {
						if (!in_array($_m, $needField2)) {
							unset($_checkpoint[$_m]);
						}
					}
					$_track['checkpoints'][$_l] = $_checkpoint;

					if (empty($start_time) || $start_time > $_checkpoint['checkpoint_time']) {
						$start_time = $_checkpoint['checkpoint_time'];
					}
					if (empty($end_time) || $end_time < $_checkpoint['checkpoint_time']) {
						$end_time = $_checkpoint['checkpoint_time'];
					}
					if ($_checkpoint['slug'] == $_track['slug']) {
						$_checkpoint_source[] = $_checkpoint;
					} else {
						$_checkpoint_destination[] = $_checkpoint;
					}
				}
				$_track['checkpoint_source'] = $_checkpoint_source;
				$_track['checkpoint_destination'] = $_checkpoint_destination;
				unset($_track['checkpoints']);
			}
//			if (empty($start_time)) {
//				$start_time = $_track['created_at'];
//			}

			if (!empty($start_time)) {
				$start_date = date('Y-m-d', strtotime($start_time));
			} else {
				$start_date = '';
			}
			if (!empty($end_time)) {
				$end_date = date('Y-m-d', strtotime($end_time));
			} else {
				$end_date = '';
			}
			$_track['start_date'] = $start_date;
			$_track['end_date'] = $end_date;
			$tracks[$_k] = $_track;
		}

		$mongo = EcoDb::get_instance()->getDb();
		$_collection = $mongo->selectCollection('trackings');
		foreach ($tracks as $_track) {
			$query = array('tracking_number' => $_track['tracking_number']);
			$update = array('$set' => $_track);

			$queryStatus = array('tracking_number' => $_track['tracking_number'],'tag_manual'=>1);
            if (count($_collection->findOne($queryStatus)) == 0 ){
                $_collection->updateOne($query, $update, array('upsert' => true));
            }
        }

	}
}