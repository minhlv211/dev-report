<?php

class PartnerHelper {

    public static function getAllPartner() {
        $list = array();

        $list['default'] = 'Default';
        $list['kyna'] = 'Kyna';

        return $list;
    }

    public static function getDataSendFB($item, $partner) {
        if ($item['_relationship_status'] == "In a relationship" || $item['_relationship_status'] == "In an open relationship" || $item['_relationship_status'] == "It's complicated") {
            $item['_relationship_status'] = 'Relationship';
        }

        $age = 0;
        $university = '';
        $high_school = '';
        $work = array();
        if (isset($item['_fb_data']) && isset($item['_fb_data']['work'])) {
            foreach ($item['_fb_data']['work'] as $w) {
                $w2 = array();
                $w2['employer'] = isset($w['employer']) ? $w['employer']['name'] : '';
                $w2['position'] = isset($w['position']) ? $w['position']['name'] : '';
                $w2['location'] = isset($w['location']) ? $w['location']['name'] : '';
                $w2['start_date'] = isset($w['start_date']) ? $w['start_date'] : '';
                $work[] = $w2;
            }
        }
        if (isset($item['_fb_data']) && isset($item['_fb_data']['education'])) {
            foreach ($item['_fb_data']['education'] as $e) {
                if (empty($high_school) && isset($e['school']) && $e['type'] == 'High School') {
                    $high_school = $e['school']['name'];
                }
                if (empty($university) && isset($e['school']) && $e['type'] == 'College') {
                    $university = $e['school']['name'];
                }
            }
        }
        if (isset($item['_fb_data']) && isset($item['_fb_data']['birthday'])) {
            if (strlen($item['_fb_data']['birthday']) == 10) {
                $age = intval(date("Y")) - intval(substr($item['_fb_data']['birthday'], 6));
            }
        }

        if ($partner == 'topica') {
            $user_info = array(
                'id' => $item['_fb_id'],
                'link' => !empty($item['_fb_id']) && $item['_fb_id'] != 'NOFB' ? 'https://www.facebook.com/' . $item['_fb_id'] . '/about' : '',
                'picture' => !empty($item['_fb_id']) ? 'https://graph.facebook.com/' . $item['_fb_id'] . '/picture?width=120&height=120' : '',
                'name' => $item['_fb_name'],
                'age' => $age,
                'gender' => isset($item['_gender']) && $item['_gender'] != 'NOFB' ? ucwords($item['_gender']) : '',
                'status' => isset($item['_relationship_status']) && $item['_relationship_status'] != 'NOFB' ? $item['_relationship_status'] : '',
                'location' => $item['_current_city'] && $item['_current_city'] != 'NOFB' ? $item['_current_city'] : '',
                'hometown' => $item['_hometown'] && $item['_hometown'] != 'NOFB' ? $item['_hometown'] : '',
                'work' => $work,
                'high_school' => $high_school,
                'university' => $university
            );
            return array('unique_id' => $item['_unique_id'], 'score' => $item['_score'], 'user_info' => $user_info);
        }
        if ($partner == 'kyna') {
            $user_info = array(
                'id' => $item['_fb_id'],
                'link' => !empty($item['_fb_id']) && $item['_fb_id'] != 'NOFB' ? 'https://www.facebook.com/' . $item['_fb_id'] . '/about' : '',
                'picture' => !empty($item['_fb_id']) ? 'https://graph.facebook.com/' . $item['_fb_id'] . '/picture?width=120&height=120' : '',
                'name' => $item['_fb_name'],
                'age' => $age,
                'gender' => isset($item['_gender']) && $item['_gender'] != 'NOFB' ? ucwords($item['_gender']) : '',
                'status' => isset($item['_relationship_status']) && $item['_relationship_status'] != 'NOFB' ? $item['_relationship_status'] : '',
                'location' => $item['_current_city'] && $item['_current_city'] != 'NOFB' ? $item['_current_city'] : '',
                'hometown' => $item['_hometown'] && $item['_hometown'] != 'NOFB' ? $item['_hometown'] : '',
                'work' => $work,
                'high_school' => $high_school,
                'university' => $university
            );
            return array('unique_id' => $item['_unique_id'], 'score' => $item['_score'], 'user_info' => $user_info);
        }
        return null;
    }

    public static function getDataSendScore($info, $partner) {
        if ($partner == 'topica') {
            return array(
                'email' => $info['_email'],
                'cds_id' => $info['_unique_id'],
                'mobile' => $info['_mobile'],
                'extra_data' => array(
                    'full_name' => $info['fullname'],
                    'utm_source' => $info['utm_source'],
                    'branch' => $info['branch'],
                    'birthday_year' => $info['age'],
                    'nganhdk' => $info['nganhdk'],
                    'trinhdo' => $info['trinhdo'],
                    'topica_loc' => $info['address']
                )
            );
        }
        if ($partner == 'kyna') {
            return array(
                'email' => $info['_email'],
                'id' => $info['_unique_id'],
                'mobile' => $info['_mobile'],
                'extra_data' => array(
                    'full_name' => $info['user_name'],
                    'course_type' => $info['course_type'],
                    'landing_page_type' => $info['landing_page_type'],
                    'form_name' => $info['form_name'],
                    'affiliate' => $info['affiliate'],
                    'location' => $info['location']
                )
            );
        }
        return null;
    }

}

?>