<?php
require_once ROOT_DIR .'/libs/class.browser.php';
class UseragentHelper {

	static function getOsTitle($user_agent) {
		$browser = new EcoBrower($user_agent);
		return $browser->browser['os']['title'];
	}

	static function getOsVersion($user_agent) {
		$browser = new EcoBrower($user_agent);
		return $browser->browser['os']['version'];
	}

	static function getBrowserTitle($user_agent) {
		$browser = new EcoBrower($user_agent);
		return $browser->browser['browser']['title'];

	}
	static function getBrowserVersion($user_agent) {
		$browser = new EcoBrower($user_agent);
		return $browser->browser['browser']['version'];

	}
	static function getDevice($user_agent) {
		$browser = new EcoBrower($user_agent);
		return $browser->browser['device'];
	}

	static function checkBot($user_agent) {
		$list_bots = array(
			'bot',
			'Bot',
			'SkypeUriPreview',
			'Google',
			'facebookexternalhit',
			'Crawler',
			'CSM Click'
		);

		foreach ($list_bots as $_bot) {
			if (strpos($user_agent, $_bot) !== false) {
				return true;
			}
		}
		return false;

	}

	static function isIgnoreIp($ip) {
		$ignoreIp = array('14.161.30.118', '127.0.0.1');
		return in_array($ip, $ignoreIp);
	}

	static function checkIgnoreUseragent($user_agent) {
		static $useragent_list;
		if (empty($useragent_list)) {
			$configModel = new ConfigModel();
			$useragent_list = $configModel->getConfig('useragent_list');
			$useragent_list = trim($useragent_list);
			$useragent_list = explode("\n", $useragent_list);
			foreach ($useragent_list as $_k => $_agent) {
				$useragent_list[$_k] = trim($_agent);
			}
		}
		foreach ($useragent_list as $_agent) {
			if (strpos($user_agent, $_agent) !== false) {
				return true;
			}
		}
		return false;

	}
	static function checkIgnoreIp($ip) {
		if (!is_string($ip)) {
			return true;
		}
		$ip_part = explode('.', $ip);
		if (count($ip_part) != 4) {
			return true;
		}
		foreach ($ip_part as $_ip) {
			if (!preg_match('/[0-9]{1,3}/', $_ip)) {
				return true;
			}
			$_ip = intval($_ip);
			if ($_ip < 0 || $_ip > 255) {
				return true;
			}
		}
		if ($ip == '127.0.0.1') {
			return true;
		}
		if ($ip_part[0] == 10) {
			return true;
		}
		if ($ip_part[0] == 192 && $ip_part[1] == 168) {
			return true;
		}
		if ($ip_part[0] == 172 && $ip_part[1] >= 16 && $ip_part[1] <= 31) {
			return true;
		}

		static $ip_list;
		if (empty($ip_list)) {
			$configModel = new ConfigModel();
			$ip_list = $configModel->getConfig('ip_list');
			$ip_list = trim($ip_list);
			$ip_list = explode("\n", $ip_list);
			foreach ($ip_list as $_k => $_ip) {
				$ip_list[$_k] = trim($_ip);
			}
		}
		if (in_array($ip, $ip_list)) {
			return true;
		}
		return false;

//		$useragent_list = $configModel->getConfig('useragent_list');
//		$cookieid_list = $configModel->getConfig('cookieid_list');

	}

	static function getModelFromUserAgent($user_agent) {
		$os = self::getOsTitle($user_agent);
		$os_version = self::getOsVersion($user_agent);
		if ($os != 'Android' || empty($os_version)) {
			return null;
		}
		$pos = strpos($user_agent, '(');
		if ($pos) {
			$user_agent = substr($user_agent, $pos + 1);
		}
		$pos = strpos($user_agent, ')');
		if ($pos) {
			$user_agent = substr($user_agent, 0, $pos - 1);
		}
		$user_agent = explode(';', $user_agent);
		$is_exist = false;
		foreach($user_agent as $_part) {
			if (strpos($_part, 'Build/') !== false) {
				$user_agent = $_part;
				$is_exist = true;
				break;
			}
		}
		if (!$is_exist) {
			$user_agent = $user_agent[count($user_agent) - 1];
		}

		$pos = strpos($user_agent, 'Build/');
		if ($pos) {
			$user_agent = substr($user_agent, 0, $pos - 1);
		}
		$user_agent = trim($user_agent);
		return $user_agent;
	}

}