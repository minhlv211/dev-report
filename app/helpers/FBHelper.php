<?php

class FBHelper
{
	static function getToken($type = 'default', $account_id = '1856139851382075')
	{
		$commentTokenList = array(
			'1856139851382075' => 'EAAHGZCM6a4H4BAPb001vbO826Tb13HY62vYtfkFsKiz0ZCJFoQRbKzVUC5SZCwMCT8IuZA7AWhzNrTWrZBZAVvJozNJYR4sZCt9khG6GY92NjZAdLqefYP1BMFe6kcaEOjhZCERLCvqpZCCjOkFiZBvlmAZBDeC3njCe3sYvDWFnR2JFuwZDZD',
			'282042242270891' => 'EAAHGZCM6a4H4BAKpV6ZBSCuUbSrVwZCg9EDi0Yc1dVGUfUsV3Ma8XbZCovssWuC283ub2eJcFCbDCG0MluDr2hpptl7uwWlhcuZAu7dKCu7f3a1plmkh6eM9m0fsEu3354bstMMrYg6ScFb1bo6D7jivzKHluyCGfYfhtXvq4rwZDZD',
		);

		if ($type == 'comment') {
			if (isset($commentTokenList[$account_id])) {
				return $commentTokenList[$account_id];
			}
		}
		return '';

	}


	static function hideComment($comment_id, $account_id, $post_id) {
		$token = self::getToken('comment', $account_id);
		$url = 'https://graph.facebook.com/v2.10/'.$comment_id.'?&access_token=' . $token;
		$result = self::crawlerUrl($url, 'POST', array('is_hidden' => true));
		self::getPostInfo($post_id);
	}

	static function unHideComment($comment_id, $account_id, $post_id) {
		$token = self::getToken('comment', $account_id);
		$url = 'https://graph.facebook.com/v2.10/'.$comment_id.'?&access_token=' . $token;
		$result = self::crawlerUrl($url, 'POST', array('is_hidden' => false));
		self::getPostInfo($post_id);
	}

	static function likeComment($comment_id) {

	}

	static function loveComment($comment_id) {

	}

	static function unLikeComment($comment_id) {

	}

	static function replyComment($comment_id, $account_id, $post_id, $message) {
		$token = self::getToken('comment', $account_id);
		$url = 'https://graph.facebook.com/v2.10/'.$comment_id.'/comments?&access_token=' . $token;
		$result = self::crawlerUrl($url, 'POST', array('message' => $message));
		if (!$result) {
			return false;
		}
		self::getPostInfo($post_id);
		return true;
	}


	static function getPostInfo($post_id)
	{
		if (empty($post_id)) {
			return;
		}

		$token = self::getToken();
		$fields = '&fields=id,created_time,message,comments,from,is_hidden,is_expired,likes,link,picture,shares,status_type,subscribed,type,updated_time,object_id,privacy';
		$url = "https://graph.facebook.com/{$post_id}?{$fields}&access_token=" . $token;
		$data = self::crawlerUrl($url);
		$postInfo = json_decode($data, true);
		$postInfo['comment_count'] = $postInfo['comments']['count'];
		$postInfo['likes_count'] = $postInfo['likes']['count'];
		$postInfo['shares_count'] = $postInfo['shares']['count'];
		$postInfo['from_id'] = $postInfo['from']['id'];
		$postInfo['from_name'] = $postInfo['from']['name'];
		$postInfo['privacy'] = $postInfo['privacy']['value'];
		unset($postInfo['comments']);
		unset($postInfo['likes']);
		unset($postInfo['shares']);
		unset($postInfo['from']);

		$post_from_id = $postInfo['from_id'];
		$post_from_name = $postInfo['from_name'];


		$fields = '&fields=id,created_time,from,message,like_count,comment_count,is_hidden,message_tags,attachment,object,parent';
		$url = "https://graph.facebook.com/v2.10/{$post_id}/comments?{$fields}&limit=100&access_token=" . $token;

		$data = self::crawlerUrl($url);
		$data = json_decode($data, true);
		$commentList = $data['data'];
		$next_url = $data['paging']['next'];
		while ($next_url) {
			$data = self::crawlerUrl($next_url);
			$data = json_decode($data, true);
			$next_url = $data['paging']['next'];
			$data = $data['data'];
			foreach ($data as $_comment) {
				$commentList[] = $_comment;
			}
		}

		if (count($commentList) > 0) {
			foreach ($commentList as $_k => $_comment) {
				$_comment['from_id'] = $_comment['from']['id'];
				$_comment['from_name'] = $_comment['from']['name'];
				$_comment['post_from_id'] = $post_from_id;
				$_comment['post_from_name'] = $post_from_name;
				unset($_comment['from']);
				if ($_comment['parent'] && $_comment['parent']['id']) {
					$_comment['parent_id'] = $_comment['parent']['id'];
					unset($_comment['parent']);
				}
				if ($_comment['message_tags']) {
					$_message = $_comment['message'];
					for ($i = count($_comment['message_tags']) - 1; $i >= 0; $i--) {
						$_tag = $_comment['message_tags'][$i];
						$_id = $_tag['id'];
						$_offset = $_tag['offset'];
						$_length = $_tag['length'];
						$_name = $_tag['name'];
						$_tag_content = '<a href="https://www.facebook.com/' . $_id . '">' . $_name . '</a>';
						$_message = substr($_message, 0, $_offset) . $_tag_content . substr($_message, $_offset + $_length);
					}
					$_comment['message'] = $_message;
					unset($_comment['message_tags']);
				}

				if ($_comment['attachment']) {
					$attachment_type = $_comment['attachment']['type'];
					if ($attachment_type == 'photo' || $attachment_type == 'sticker') {
						$_attachment_images = $_comment['attachment']['media']['image'];
						$_message = $_comment['message'];
						if (!empty($_message)) {
							$_message = $_message . '<br />' . PHP_EOL;
						}
						$_width = $_attachment_images['width'];
						$_height = $_attachment_images['height'];
						$_src = $_attachment_images['src'];
						$_message = $_message . '<img src="' . $_src . '" width="' . $_width . '" height="' . $_height . '" />';

						$_comment['message'] = $_message;
					}
					unset($_comment['attachment']);

				}

				$_comment['message'] = iconv('UTF-8', 'UTF-8//IGNORE', $_comment['message']);

				if ($_comment['is_hidden']) {
					$_comment['is_hidden'] = 1;
				} else {
					$_comment['is_hidden'] = 2;
				}

				$is_negative = self::detectNegativeComment($_comment['message']);
				if ($is_negative) {
					$_comment['is_negative'] = 1;
				} else {
					$_comment['is_negative'] = 2;
				}

				$commentList[$_k] = $_comment;
			}
			$commentListTmp = $commentList;
			$commentList = array();
			foreach ($commentListTmp as $_k => $_comment) {
				$_id = $_comment['id'];
				$_parent_id = $_comment['parent_id'];
				if ($_parent_id) {
					unset($_comment['parent_id']);
					unset($_comment['comment_count']);
					if($_comment['is_negative'] == 1) {
						$commentList[$_parent_id]['is_negative'] = 1;
					}
					$commentList[$_parent_id]['comments'][] = $_comment;
				} else {
					$commentList[$_id] = $_comment;
				}
			}

			foreach ($commentList as $_k => $_comment) {
				if (is_array($_comment['comments']) && count($_comment['comments']) > 0) {
					$_c = count($_comment['comments']);
					$_last_comment = $_comment['comments'][$_c - 1];
					$_comment['last_id'] = $_last_comment['from_id'];
					$_comment['last_date'] = $_last_comment['created_time'];
				} else {
					$_comment['last_id'] = null;
					$_comment['last_date'] = $_comment['created_time'];
				}
				if ($_comment['last_id'] == '1856139851382075' || $_comment['last_id'] == '282042242270891') {
					$_comment['reply'] = 1;
				} else {
					$_comment['reply'] = 2;
				}
				$_comment['post_id'] = $post_id;
				$commentList[$_k] = $_comment;
			}

			$commentList = array_values($commentList);
		}


		$mongo = EcoDb::get_instance()->getDb();
		$fb_posts = $mongo->selectCollection('fb_posts');
		$fb_comments = $mongo->selectCollection('fb_comments');
		$query = array('id' => $postInfo['id']);
		$update = array('$set' => $postInfo);
		$fb_posts->updateOne($query, $update, array('upsert' => true));
		if (count($commentList) > 0) {
			foreach ($commentList as $_comment) {
				//var_dump($_comment['id']);
				$query = array('id' => $_comment['id']);
				$current_info = $fb_comments->findOne($query);
				if ($current_info && $current_info['reply'] == 3) {
					unset($_comment['reply']);
				}
				$update = array('$set' => $_comment);
				$fb_comments->updateOne($query, $update, array('upsert' => true));
			}
		}
	}


	static function getPostList($account_id) {
		$token = self::getToken();
		$fields = 'fields=id';
		$url = "https://graph.facebook.com/{$account_id}/posts?{$fields}&limit=1000&access_token=" . $token;
		$data = self::crawlerUrl($url);
		$data = json_decode($data, true);
		return $data['data'];
	}

	static function crawlerUrl($url, $method = 'GET', $data = array(), $timeout = 30)
	{

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);

		if ($method == 'POST') {
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		}

		$response = curl_exec($curl);
		return $response;
	}

	static function detectNegativeComment($message) {
		if (empty($message)) {
			return false;
		}

		$listKeyword = array(
			'scam',  'shit', 'ugly', 'ignoring ', 'China',
			'can not believe' , 'aliexpress', 'never delivered', 'fake', 'poor quality',
			'sucks', 'cheap junk', 'waiting', 'haven’t received', 'so small',
			'not received' , 'dont trust', 'don\'t trust', 'many complaints',
			'very small', 'disappointed', 'no response', 'expensive', 'too bad',
			'didn\'t get', 'to small', 'idiots', 'waiting'
		);
		foreach ($listKeyword as $_keyword) {
			if (strpos($message, $_keyword) !== false) {
				return true;
			}
		}
		return false;
	}


}