<?php
require_once ROOT_DIR . '/vendor/eden/core/src/Control.php';

class EmailHelper
{
	private $_email = 'support@mythofeastern.com';
	private $_password = 'moesupport';
	private $_attachDir = '/mnt/hgfs/WORK/dropship/assets/emails/';

	/**
	 * @return Eden\Mail\Smtp
	 */
	function getSMTP()
	{
		static $smtp = null;
		if (empty($smtp)) {
			$smtp = eden('mail')->smtp( 'smtp.zoho.com', $this->_email, $this->_password, 465, true);
		}
		return $smtp;
	}

	/**
	 * @return Eden\Mail\Imap
	 */
	function getIMAP()
	{
		static $imap = null;
		if (empty($imap)) {
			$imap = eden('mail')->imap( 'imappro.zoho.com', $this->_email, $this->_password, 993, true);
		}
		return $imap;
	}

	function replyEmail($to, $bodyHTML) {
	}

	function countEmail($box = 'INBOX') {
		$imap = $this->getIMAP();
		$imap->setActiveMailbox($box);
		return $imap->getEmailTotal();
	}

    /**Get all email
     * @param string $box
     */
	function fetchAllEmail($box = 'INBOX')
    {
        $mongo = EcoDb::get_instance()->getDb();
        $collection = $mongo->selectCollection('emails');
        $countEmail = $this->countEmail($box);

        $imap = $this->getIMAP();
        $imap->setActiveMailbox($box);

        $emailList = array();
        for ($i = 0; $i<  ($countEmail / 10) + 1; $i++) {
            $emails = $imap->getEmails($i*10,10);

            if (count($emails) == 0) {
                echo 'no new email';
                return;
            }

            foreach ($emails as $_k => $_email) {
                echo $_email['uid']."\r\n";
                if(isset($emailList[$_email['uid']])) {
                    continue;
                }else {
                    $_email = $imap->getUniqueEmails($_email['uid'], true);

                    //save attach file by date
                    $attachment = $_email['attachment'];
                    unset($_email['attachment']);
                    if (!empty($attachment)) {
                        $dir = $this->_attachDir.date('Y-m',$_email['date']).'/';
                        if (!file_exists($dir)) { mkdir($dir,0777, true);}
                        foreach ($attachment as $name => $_att) {
                            $nameArr = explode('.',$name);
                            $fileName = md5(base64_encode($_att['data'])).'.'.end($nameArr);
                            if (!file_exists($dir.$fileName)) {
                                file_put_contents($dir.$fileName, ($_att['data']));
                            }
                            $_email['attachment'][] = array(
                                'cid' => $_att['cid'],
                                'url' => "/assets/emails/".date('Y-m',$_email['date'])."/".$fileName,
                                'type' => $_att['type'],
                                'name' => $name
                            );
                        }
                    }


                    if (count($_email['body']) > 0) {
                        foreach ($_email['body'] as $_key => $_value) {
                            if ($_key != 'text/plain' && $_key != 'text/html') {
                                unset($_email['body'][$_key]);
                                continue;
                            }
                            $_email['body']['text/plain'] = iconv('UTF-8', 'UTF-8//IGNORE', $_email['body']['text/plain']);
                            $_email['body']['text/html'] = iconv('UTF-8', 'UTF-8//IGNORE', $_email['body']['text/html']);
                        }
                    }

                    $_email['date_time'] = date('Y-m-d H:i:s', $_email['date']);
                    $_email['date'] = date('Y-m-d', $_email['date']);

                    //Select 1 from list "to" & "from"
                    $_email['from'] = $_email['from']['email'];
                    $_email['to'] = $_email['to'][0]['email'];

//                    $collection->insertOne($_email);
                    $emailList[$_email['uid']] = $_email;
                }
            }
        }
        $collection->insertMany(array_values($emailList));
        echo "end : ".date('H:i:s');
    }

	function fetchEmail($box = 'INBOX') {
	    $countEmail = $this->countEmail($box);
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('emails');

        $skip = $collection->count(array('mailbox' => $box));

        $imap = $this->getIMAP();
//        $next = $imap->getNextUid();
		$imap->setActiveMailbox($box);

		$emails = $imap->getEmails(0, $countEmail-$skip); //for product
//		$emails = $imap->getEmails(0, 10); // for dev

		if (count($emails) == 0) {
			return;
		}
		$insertEmails = array();
        foreach ($emails as $_k => $_email) {
            $_old = $collection->findOne(array('id'=> $_email['id']));


            if (count($_old) == 0) {
                $_email = $imap->getUniqueEmails($_email['uid'],true);

                //save attach file by date
                $attachment = $_email['attachment'];
                unset($_email['attachment']);
                if (!empty($attachment)) {
                    $dir = $this->_attachDir.date('Y-m',$_email['date']).'/';
                    if (!file_exists($dir)) { mkdir($dir,0777, true);}
                    foreach ($attachment as $name => $_att) {
                        $nameArr = explode('.',$name);
                        $fileName = md5(base64_encode($_att['data'])).'.'.end($nameArr);
                        if (!file_exists($dir.$fileName)) {
                            file_put_contents($dir.$fileName, ($_att['data']));
                        }
                        $_email['attachment'][] = array(
                            'cid' => $_att['cid'],
                            'url' => "/assets/emails/".date('Y-m',$_email['date'])."/".$fileName,
                            'type' => $_att['type'],
                            'name' => $name
                        );
                    }
                }

                if (count($_email['body']) > 0) {
                    foreach ($_email['body'] as $_key => $_value) {
                        if ($_key != 'text/plain' && $_key != 'text/html') {
                            unset($_email['body'][$_key]);
                            continue;
                        }
                        $_email['body']['text/plain'] = iconv('UTF-8', 'UTF-8//IGNORE', $_email['body']['text/plain']);
                        $_email['body']['text/html'] = iconv('UTF-8', 'UTF-8//IGNORE', $_email['body']['text/html']);
                    }
                }
                $_email['date_time'] = date('Y-m-d H:i:s', $_email['date']);
                $_email['date'] = date('Y-m-d', $_email['date']);


                //Select 1 from list "to" & "from"
                $_email['from'] = $_email['from']['email'];
                $_email['to'] = $_email['to'][0]['email'];

                $insertEmails[] = $_email;
//                $collection->insertOne($_email);
                echo $_email['uid']."\n\r";
            }
        }
        if (!empty($insertEmails)) {$collection->insertMany($insertEmails);}
	}

    /**Backup function
     * @param $limit
     * @param $skip
     * @param string $box
     */
	function fetchEmailBK($limit, $skip, $box = 'INBOX') {
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('emails');

		$imap = $this->getIMAP();
		$imap->setActiveMailbox($box);
		$emails = $imap->getEmails($skip, $limit);
		if (count($emails) == 0) {
			return;
		}
		$uidList = array();
		$emailList = array();
		foreach ($emails as $_k => $_email) {
			$emails[$_k] = $_email;
			$uidList[] = $_email['uid'];
			$emailList[$_email['uid']] = $_email;
		}
		$cursor = $collection->find(array('uid' => array('$in' => $uidList), 'mailbox' => $box));
		foreach ($cursor as $_email) {

			$_uid = $_email['uid'];
			var_dump($_uid);
			$_emailUpdate = $emailList[$_uid];
			$_emailUpdate['from'] = $_emailUpdate['from']['email'];

			$toEmail = array();
			foreach ($_emailUpdate['to'] as $_to) {
				$toEmail[] = $_to['email'];
			}
			$_emailUpdate['to'] = $toEmail;
			$query = array('uid' => $_uid, 'mailbox' => $box);
			unset($_emailUpdate['cc']);
			unset($_emailUpdate['bcc']);
			unset($_emailUpdate['flags']);
			$update = array('$set' => $_emailUpdate, '$unset' => array('cc' => 1, 'bcc' => 1, 'flags' => 1));
			$collection->updateOne($query, $update);
			unset($emailList[$_uid]);
		}
		if (count($emailList) > 0) {
			foreach ($emailList as $_k => $_email) {
				var_dump($_email['uid']);
				$_email = $imap->getUniqueEmails($_email['uid'], true);
				$attachment = $_email['attachment'];
				unset($_email['attachment']);
				if (count($_email['body']) > 0) {
					foreach ($_email['body'] as $_key => $_value) {
						if ($_key != 'text/plain' && $_key != 'text/html') {
							unset($_email['body'][$_key]);
							continue;
						}
						$_email['body']['text/plain'] = iconv('UTF-8', 'UTF-8//IGNORE', $_email['body']['text/plain']);
						$_email['body']['text/html'] = iconv('UTF-8', 'UTF-8//IGNORE', $_email['body']['text/html']);
					}
				}
				$_email['date_time'] = date('Y-m-d H:i:s', $_email['date']);
				$_email['date'] = date('Y-m-d', $_email['date']);

				if (count($attachment) > 0) {
					$_email['has_attachment'] = true;
				} else {
					$_email['has_attachment'] = false;
				}

				//Select 1 from list "to" & "from"
                $_email['from'] = $_email['from']['email'];
				$_email['to'] = $_email['to'][0]['email'];

				$collection->insertOne($_email);
				$emailList[$_k] = $_email;
			}
		}
	}
}