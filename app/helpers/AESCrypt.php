<?php

class AESCrypt
{
//
	protected $password; //

	function __construct($password)
	{
		if (isset($password)) {
			if (is_file($password))
				$this->passfile = $password;
			else
				$this->setPassword($password);
		}
	}

	/*
	 * Set the user entered password
	 * converts format to single byte UTF-16LE if UTF-8
	 * Note: uses little-endian for conversion from UTF-8 as required value resides in lsb of word
	*/
	public function setPassword($password)
	{
		$this->password = mb_convert_encoding($password, "UTF-16LE", "UTF-8");// tested to match aescrypt format for passwd
	}


	/*
	 * The $data is encrypted and returned in encrypted form.
	 *
	*/
	public function encrypt($plaintext)
	{
		if (empty($this->password))
			return false;
		if (empty($plaintext))
			return false;


		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$len = strlen($plaintext);
		$padding = $iv_size - ($len % $iv_size);
		$plaintext .= str_repeat(chr(0), $padding);

		$iv = str_repeat(chr(0), $iv_size);

		$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->password, $plaintext, MCRYPT_MODE_CBC, $iv);

		$ciphertext = $this->base64url_encode($ciphertext);

		return $ciphertext;
	}

	/*
	* AESCrypt decrypt
	* The $data is decrypted and returned in un-encrypted form.
	* fails return false
	*
	* version can be either 1 or 2
	*
	*
	*
   */
	public function decrypt($ciphertext_dec)
	{
		if (empty($this->password))
			return false;
		if (empty($ciphertext_dec))
			return false;

		$ciphertext_dec = $this->base64url_decode($ciphertext_dec);
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv_dec = str_repeat(chr(0), $iv_size);


		# may remove 00h valued characters from end of plain text
		$plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->password, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

		$plaintext_dec = trim($plaintext_dec);
		return $plaintext_dec;
	}

	function base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	function base64url_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}

}