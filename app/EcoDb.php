<?php

class EcoDb {

    private $list_db;
    private $db;

    /**
     * @var EcoDb
     */
    protected static $instance;

    /**
     * @return EcoDb
     */
    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct() {
        
    }

    /**
     * @return MongoDB\Database
     */
    function getDb() {
        global $f3;

        if (!isset($this->db)) {
            if (!empty($f3)) {
                $db_path = $f3->get('mongodb');
                $db_name = $f3->get('dbname');
            } else {
                $config = parse_ini_file(ROOT_DIR . '/config.ini');
                $db_path = $config['mongodb'];
                $db_name = $config['dbname'];
            }
            $db = new MongoDB\Client($db_path, array());
            $db = $db->selectDatabase($db_name);
            $this->db = $db;
        }
        return $this->db;
    }

    /**
     * @return MongoDB\Database
     */
    function getDbPartner($partner = '') {
        
        global $f3;

        if (isset($_SESSION['partner'])) {
            $partner = $_SESSION['partner'];
        }

        if (empty($partner)) {
            if (!empty($f3)) {
                $partner = $f3->get('partner');
            } else {
                $config = parse_ini_file(ROOT_DIR . '/config.ini');
                $partner = $config['partner'];
            }
        }

        if (!isset($this->list_db[$partner])) {
            if (!empty($f3)) {
                $db_path = $f3->get('mongodb_partner_' . $partner);
                $db_name = $f3->get('dbname_partner_' . $partner);
            } else {
                if (!isset($config)) {
                    $config = parse_ini_file(ROOT_DIR . '/config.ini');
                }
                $db_path = $config['mongodb_partner_' . $partner];
                $db_name = $config['dbname_partner_' . $partner];
            }
            $db = new MongoDB\Client($db_path, array());
            $db = $db->selectDatabase($db_name);
            $this->list_db[$partner] = $db;
        }

        return $this->list_db[$partner];
    }

    /**
     * @return Redis
     */
    function getRedis() {
        global $f3;
        static $redis = null;
        if (empty($redis)) {
            $redis = new Redis();


            if (!empty($f3)) {
                $redis_host = $f3->get('redis_host');
                $redis_port = $f3->get('redis_port');
            } else {
                $config = parse_ini_file(ROOT_DIR . '/config.ini');
                $redis_host = $config['redis_host'];
                $redis_port = $config['redis_port'];
            }
            $redis->connect($redis_host, $redis_port);
        }
        return $redis;
    }

}
