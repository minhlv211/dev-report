<?php
class AliModel extends RootModel {
	function import($orderData) {
		$mongo = EcoDb::get_instance()->getDb();
		$ali_orders = $mongo->selectCollection('ali_orders');
		$order_id = $orderData['order_id'];
		if (empty($order_id)) {
			return false;
		}
		unset($orderData['order_id']);
		$current_order = $ali_orders->findOne(array('order_id' => $order_id));
		if ($current_order) {
			$orderData['created_date'] = date('Y-m-d');
			$orderData['created_at'] = date('Y-m-d H:i:s');
			$orderData['last_modify'] = date('Y-m-d H:i:s');
		} else {
			$orderData['last_modify'] = date('Y-m-d H:i:s');
		}

		$address = $orderData['address'];
		$address = trim($address);
		$addressArr = explode(',', $address);
		$country = $addressArr[count($addressArr) - 1];

		//For special case with long country name
		if ($country == 'Croatia (local name: Hrvatska)') $country = 'Croatia';
		if ($country == 'Slovakia (Slovak Republic)') $country = 'Slovakia';
		if ($country == 'Russian Federation') $country = 'Russian';


		$country = trim($country);
		$country_code = CountryHelper::country2code($country);

		$orderData['country'] = $country;
		$orderData['country_code'] = $country_code;


		$orderData['product_price'] = floatval($orderData['product_price']);
		$orderData['shipping_price'] = floatval($orderData['shipping_price']);
		$orderData['change_price'] = floatval($orderData['change_price']);
		$orderData['discount_price'] = floatval($orderData['discount_price']);
		$orderData['order_price'] = floatval($orderData['order_price']);

		if ($orderData['order_at']) {
			$orderData['order_at_date'] = substr($orderData['order_at'], 0, 10);
		}
		if (is_array($orderData['product_list']) && count($orderData['product_list']) > 0) {
			$product_list = $orderData['product_list'];
			foreach ($product_list as &$_product) {
				$_product['price'] = floatval($_product['price']);
				$_product['amount'] = floatval($_product['amount']);
				$_product['quantity'] = intval($_product['quantity']);
				$_url = $_product['url'];
				$_url = parse_url($_url);
				$_query = $_url['query'];
				parse_str($_query, $queryArr);
				$_product['product_id'] = $queryArr['productId'];

			}
			$orderData['product_list'] = $product_list;
		}

		$orderData = array('$set' => $orderData);

		$ali_orders->updateOne(array('order_id' => $order_id), $orderData, array('upsert' => true));

		return $orderData;
	}

	function getOrder($filter, $limit = 25, $offset = 0, $sort = 'order_at', $order = 'desc') {
		$mongo = EcoDb::get_instance()->getDb();
		$order_collection = $mongo->selectCollection('ali_orders');
		if ($order == 'asc') {
			$order = 1;
		} else {
			$order = -1;
		}
		$cursor = $order_collection->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
		$orderList = array();
		foreach ($cursor as $_order) {
			$order_id = $_order['order_id'];
			$orderList[$order_id] = $_order;
		}
		return $orderList;
	}

	function countOrder($filter) {
		$mongo = EcoDb::get_instance()->getDb();
		$order_collection = $mongo->selectCollection('ali_orders');
		return $order_collection->count($filter);
	}

	function getProductList($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$product_collection = $mongo->selectCollection('ali_products');
		$cursor = $product_collection->find($filter, array('sort' => array('amount' => -1)));
		$productList = array();
		foreach ($cursor as $_product) {
			$productList[] = $_product;
		}
		return $productList;
	}

	function report($filter,$field)
    {
        $mongo = EcoDb::get_instance()->getDb();
        $product_collection = $mongo->selectCollection('ali_orders');
        $cursor = $product_collection->find($filter);

        $report = array();
        foreach ($cursor as $_value) {
            if ($field == 'country_code') {
                $report[$_value[$field]]['value'] = CountryHelper::country_code_to_country($_value[$field]);
            }else {
                $report[$_value[$field]]['value'] = $_value[$field];
            }
            $report[$_value[$field]]['count'] += 1;
        }
        return $report;
    }

}