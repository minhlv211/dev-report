<?php
class CodacyModel extends RootModel {
    function import($data) {
        $mongo = EcoDb::get_instance()->getDb();
        $codacy_collection = $mongo->selectCollection('codacy_report');

        $return = [];
        foreach ($data as $_item) {
            $oldRecord = $codacy_collection->count(['commit' => $_item[3]]);
            if ($oldRecord > 0) continue;
            $insert = [
                'project'       => $_item[0],
                'author'        => $_item[1],
                'commit'        => $_item[3],
                'new_issue'     => $_item[4],
                'fixed_issue'   => $_item[5],
                'created'       => date("Y-m-d H:i:s", strtotime($_item[2] )),
            ];

            $codacy_collection->insertOne($insert);
            $return[] = $insert['commit'];
        }

        return $return;
    }

}