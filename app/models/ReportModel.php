<?php
class ReportModel extends RootModel {

	private function _calculateReport($cursor, $view_by) {
		$reportInfo = array();
		foreach ($cursor as $_report) {
			if ($view_by == 'month') {
				$id = $_report['date'];
				$id = substr($id, 0, 7);
			} else {
				$id = $_report[$view_by];
			}

			if ($view_by == 'product_id' && empty($id)) {
				$id = '-1';
			}
			$reportInfo[$id]['id'] = $id;
			$reportInfo[$id]['spend'] += $_report['spend'];
			$reportInfo[$id]['impressions'] += $_report['impressions'];
			$reportInfo[$id]['clicks'] += $_report['clicks'];
			$reportInfo[$id]['inline_link_clicks'] += $_report['inline_link_clicks'];
			$reportInfo[$id]['sell_quantity'] += $_report['sell_quantity'];
			$reportInfo[$id]['sell_amount'] += $_report['sell_amount'];
			$reportInfo[$id]['sell_discount'] += $_report['sell_discount'];
			$reportInfo[$id]['est_buy_amount'] += $_report['est_buy_amount'];
			$reportInfo[$id]['est_shipping_cost'] += $_report['est_shipping_cost'];
			$reportInfo[$id]['sell_order'] += $_report['sell_order'];


			$reportInfo[$id]['checkout_value'] += $_report['checkout_value'];
			$reportInfo[$id]['purchase_value'] += $_report['purchase_value'];
			$reportInfo[$id]['post_engagement'] += $_report['post_engagement'];
			$reportInfo[$id]['view_content'] += $_report['view_content'];
			$reportInfo[$id]['add_to_cart'] += $_report['add_to_cart'];
			$reportInfo[$id]['checkout'] += $_report['checkout'];
			$reportInfo[$id]['purchase'] += $_report['purchase'];
		}

		return $reportInfo;
	}


	function getReport($filter, $view_by = 'country') {
		$mongo = EcoDb::get_instance()->getDb();
		$report_country_product = $mongo->selectCollection('report_country_product');
		$cursor = $report_country_product->find($filter);
		$reportInfo = $this->_calculateReport($cursor, $view_by);
		return $reportInfo;
	}

	function getReportDevice($filter, $view_by = 'impression_device') {
		$mongo = EcoDb::get_instance()->getDb();
		$report_country_product = $mongo->selectCollection('report_device_product');
		$cursor = $report_country_product->find($filter);
		$reportInfo = $this->_calculateReport($cursor, $view_by);
		return $reportInfo;
	}

	function getReportUpSale($filter, $view_by = 'product_id') {
		$mongo = EcoDb::get_instance()->getDb();
		$report_country_product = $mongo->selectCollection('report_upsale_product');
		$cursor = $report_country_product->find($filter);
		$reportInfo = $this->_calculateReport($cursor, $view_by);

		return $reportInfo;
	}

	function getReportPlatform($filter, $view_by = 'publisher_platform') {
		$mongo = EcoDb::get_instance()->getDb();
		$report_country_product = $mongo->selectCollection('report_platform_product');
		$cursor = $report_country_product->find($filter);
		$reportInfo = $this->_calculateReport($cursor, $view_by);

		return $reportInfo;
	}

	function getReportHourly($filter,$time_offset = false, $view_by = 'hourly') {
		$mongo = EcoDb::get_instance()->getDb();
		$report_country_product = $mongo->selectCollection('report_hourly_product');

		if ($time_offset) {
		    if (is_array($filter['date'])) {
                $filter['date']['$lte'] = date('Y-m-d',strtotime($filter['date']['$lte'].' + 1 day'));
            }else {
                $filter['date'] = array(
                    '$gte' => $filter['date'],
                    '$lte' => date('Y-m-d',strtotime($filter['date'].' + 1 day'))
                );
            }
        }

		$cursor = $report_country_product->find($filter);

        $list = $cursor->toArray();

        //if time_offset = true
        if ($time_offset) {
            $start_date = $filter['date']['$gte'];
            $end_date = $filter['date']['$lte'];
            foreach ($list as $_k => $_value) {
                $hourly = explode('-',$_value['hourly']);
                if ($_value['date'] == $start_date) { //trim start date
                    if (strtotime($hourly[0])  < strtotime('13:00')){ //11:00 diff from gmt + 7 and gmt - 6
                        unset($list[$_k]);
                    }
                }
                if ($_value['date'] == $end_date) { //trim end date
                    if (strtotime($hourly[0])  >= strtotime('13:00')){ //11:00 diff from gmt + 7 and gmt - 6
                        unset($list[$_k]);
                    }
                }

                if (isset($list[$_k])) {
                    //convert to real timezone datetime
                    $real_date = $_value['date'].' '.$hourly[0];

                    $list[$_k]['hourly'] = date('H:i', strtotime($hourly[0].'- 13 hours'))."-".date('H:i', strtotime($hourly[1].'- 13 hours'));
                    $list[$_k]['date'] = date('Y-m-d', strtotime($real_date.' - 13 hours'));
                }
            }
        }

		$reportInfo = $this->_calculateReport($list, $view_by);
		return $reportInfo;
	}

	function getReportAge($filter, $view_by = 'age') {
		$mongo = EcoDb::get_instance()->getDb();
		$report_country_product = $mongo->selectCollection('report_age_product');
		$cursor = $report_country_product->find($filter);
		$reportInfo = $this->_calculateReport($cursor, $view_by);
		return $reportInfo;
	}

	function getReportGender($filter, $view_by = 'gender') {
		$mongo = EcoDb::get_instance()->getDb();
		$report_country_product = $mongo->selectCollection('report_gender_product');
		$cursor = $report_country_product->find($filter);
		$reportInfo = $this->_calculateReport($cursor, $view_by);
		return $reportInfo;
	}

}