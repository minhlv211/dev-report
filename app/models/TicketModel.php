<?php
class TicketModel extends RootModel {
    protected $__collection;

    function __construct()
    {
        $mongo = EcoDb::get_instance()->getDb();
        $this->__collection = $mongo->selectCollection('ticket');
    }

    function editTicket($query,$data)
    {
        try {
            return $this->__collection->updateOne($query, array('$set' => $data));
        } catch (MongoDB\Driver\Exception\InvalidArgumentException $e) {
            return false;
        }
    }

    function countTickets($filter = array()) {
        return $this->__collection->count($filter);
    }

    function getTickets($email) {
        return $this->__collection->findOne(array('email' => $email));

    }

    function getTicketList($filter  = array(), $limit = 25, $offset = 0, $sort = 'date_time', $order = 'desc') {
        if ($order == 'asc') {
            $order = 1;
        } else {
            $order = -1;
        }

        if (isset($filter['number'])) {
            $email = $this->getEmailByOrderNumber($filter['number']);
            unset($filter['number']);

            if ($email) {
                $filter['email'] = $email;
                $cursor = $this->__collection->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
            }else {
                return [];
            }
        }else {
            $cursor = $this->__collection->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
        }

        $commentList = array();
        foreach ($cursor as $_comment) {
            $commentList[] = $_comment;
        }
        return $commentList;
    }

    function trackingStatistic($filters,$option = array()) {
        $field = 'status';
        $listFields = array(
            'count',
        );

        if (isset($filters['number']) && !empty($filters['number'])) {
            $email = $this->getEmailByOrderNumber($filters['number']);
            if (!$email) return [];
            if (isset($filters['email']) && $filters['email'] !== $email) return [];
            $filters['email'] = $email;
            unset($filters['number']);
        }

        $cursor = $this->__collection->find($filters);
        $trackingCount = array();
        foreach ($cursor as $_track) {
            if ($option[$_track[$field]]) {
                $trackingCount[$option[$_track[$field]]]['value'] = $_track[$field];
            }
            foreach ($listFields as $_f) {
                if ($option[$_track[$field]]) {
                    $trackingCount[$option[$_track[$field]]][$_f] += 1;
                }
            }
        }
        return $trackingCount;
    }

    function trackingCountry($filters,$option = array()) {
        $field = 'country';
        $listFields = array(
            'count',
        );

        if (isset($filters['number']) && !empty($filters['number'])) {
            $email = $this->getEmailByOrderNumber($filters['number']);
            if (!$email) return [];
            if (isset($filters['email']) && $filters['email'] !== $email) return [];
            $filters['email'] = $email;
            unset($filters['number']);
        }

        $cursor = $this->__collection->find($filters);
        $trackingCount = array();
        foreach ($cursor as $_track) {
            if (!empty($_track['country_code'])) {
                $trackingCount[$_track['country_code']]['count'] += 1;
                $trackingCount[$_track['country_code']]['value'] = ($_track['country_code'] != 'other') ? CountryHelper::country_code_to_country($_track['country_code']) : "Other";
            }
        }
//        $tmp = $trackingCount['other'];
//        unset($trackingCount['other']);
//        $trackingCount['other'] = $tmp;

        return $trackingCount;
    }

    function getEmailByOrderNumber($orderNumber) {
        $mongo = EcoDb::get_instance()->getDb();
        $orderCollection =  $mongo->selectCollection('orders');
        if (!is_numeric($orderNumber)) return false;

        $orderList = $orderCollection->findOne(array('number'=>$orderNumber - 1000));
        return (count($orderList) > 0) ? $orderList['email'] : false;
    }

}