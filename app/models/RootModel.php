<?php
class RootModel {
    /**
     * @return MongoDB\Database
     */
    function getDB() {
        return EcoDb::get_instance()->getDb();
    }

    /**
     * @return MongoDB\Database
     */
    function getDBPartner() {
        return EcoDb::get_instance()->getDbPartner();

    }
}