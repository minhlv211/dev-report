<?php

class AdminUserModel {

    public static function get_count_list($condition = array()) {
        $db = EcoDb::get_instance()->getDb();
        $collection = $db->admin_user;
        $count = $collection->count($condition);
        return $count;
    }

    public static function get_list_all($condition = array()) {
        $db = EcoDb::get_instance()->getDb();
        $list = array();
        $collection = $db->admin_user;
        $rs = $collection->find($condition, array('sort' => array('create_at' => -1)));
        foreach ($rs as $item) {
            $list[] = $item;
        }
        return $list;
    }

    public static function get_list($limit, $offset, $condition = array(), $sort = array()) {
        $db = EcoDb::get_instance()->getDb();
        $list = array();
        $collection = $db->admin_user;
        $rs = $collection->find($condition, array('skip' => $offset, 'limit' => $limit, 'sort' => $sort));
        foreach ($rs as $item) {
            $list[] = $item;
        }
        return $list;
    }

    public static function getById($uid) {
        $db = EcoDb::get_instance()->getDb();
        $collection = $db->admin_user;
        $info = $collection->findOne(array('_id' => new MongoDB\BSON\ObjectID($uid)));
        return $info;
    }

    public static function getByEmail($email, $condition = array()) {
        $db = EcoDb::get_instance()->getDb();
        $collection = $db->admin_user;

        $condition['email'] = $email;
        $info = $collection->findOne($condition);
        return $info;
    }

    public static function getByUsername($username, $condition = array()) {
        $db = EcoDb::get_instance()->getDb();
        $collection = $db->admin_user;

        $condition['username'] = $username;
        $info = $collection->findOne($condition);
        return $info;
    }

    public static function save($info) {
        $db = EcoDb::get_instance()->getDb();
        $collection = $db->admin_user;
        $info['_id'] = isset($info['_id']) && !empty($info['_id']) ? $info['_id'] : new MongoDB\BSON\ObjectID();
        $r = $collection->updateOne(array('_id' => $info['_id']), array('$set' => $info), array('upsert' => true));
        return $r;
    }

    public static function deleteById($uid) {
        $db = EcoDb::get_instance()->getDb();
        $collection = $db->admin_user;
        $collection->deleteOne(array('_id' => new MongoDB\BSON\ObjectID($uid)));
    }

}
