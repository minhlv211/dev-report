<?php
class FBModel extends RootModel {


	function countComment($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('fb_comments');
		return $fb_comments->count($filter);
	}
	function getCommentList($filter  = array(), $limit = 25, $offset = 0, $sort = 'last_date', $order = 'desc') {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('fb_comments');
		if ($order == 'asc') {
			$order = 1;
		} else {
			$order = -1;
		}
		$cursor = $fb_comments->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
		$commentList = array();
		foreach ($cursor as $_comment) {
			$commentList[] = $_comment;
		}
		return $commentList;
	}

	function getComment($id) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('fb_comments');
		return $fb_comments->findOne(array('id' => $id));
	}

	function hideComment($id, $paren_id = null) {
	}

	function unHideComment($id, $paren_id = null) {

	}

	function ignoreComment($id) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('fb_comments');
		$query = array('id' => $id);
		$update = array('$set' => array('reply' => 3));
		return $fb_comments->updateOne($query, $update);
	}
	function unIgnoreComment($id) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('fb_comments');
		$commentInfo = $fb_comments->findOne(array('id' => $id));
		$reply = 2;
		if (count($commentInfo['comments'])) {
			$count = count($commentInfo['comments']);
			$last_comment = $commentInfo['comments'][$count - 1];
			$_last_id = $last_comment['from_id'];
			if ($_last_id == '1856139851382075' || $_last_id == '282042242270891') {
				$reply = 1;
			}
		}
		$query = array('id' => $id);
		$update = array('$set' => array('reply' => $reply));
		return $fb_comments->updateOne($query, $update);
	}

	function getCampaignList($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('fb_campaigns');
		$cursor = $collection->find($filter, array('sort' => array('spend' => -1)));
		$campaignList = array();
		foreach ($cursor as $_campaign) {
			$campaignList[$_campaign['campaign_id']] = $_campaign;
		}
		return $campaignList;
	}


	function getCampaignInfo($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('fb_campaigns');
		$cursor = $collection->find($filter, array('sort' => array('spend' => -1)));
		$campaignList = array();
		foreach ($cursor as $_campaign) {
			$campaignList[$_campaign['campaign_id']] = $_campaign;
		}
		return $campaignList;
	}

	function getAdsetInfo($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('adset_detail');
		$cursor = $collection->find($filter, array('sort' => array('spend' => -1)));
		$campaignList = array();
		foreach ($cursor as $_campaign) {
			$campaignList[$_campaign['adset_id']] = $_campaign;
		}
		return $campaignList;
	}

	function getAdInfo($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('ad_detail');
		$cursor = $collection->find($filter, array('sort' => array('spend' => -1)));
		$campaignList = array();
		foreach ($cursor as $_campaign) {
			$campaignList[$_campaign['ad_id']] = $_campaign;
		}
		return $campaignList;
	}

	function countCampaignList($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('campaign_detail');
		return $collection->count($filter);
	}

	function getCampaign($cid) {
		if (empty($cid)) {
			return false;
		}
		$mongo = EcoDb::get_instance()->getDb();
		$collection = $mongo->selectCollection('fb_campaigns');
		return $collection->findOne(array('campaign_id' => $cid));
	}

	function mappingCampaign($cid, $pid) {
		if (empty($cid) || empty($pid)) {
			return false;
		}
		$mongo = EcoDb::get_instance()->getDb();
		$query = array('campaign_id' => $cid);
		$collection = $mongo->selectCollection('fb_campaigns');
		$campaign = $collection->findOne($query);
		if (empty($campaign)) {
			return false;
		}

		$update = array('$set' => array('product_id' => $pid));
		return $collection->updateOne($query, $update);
	}

}