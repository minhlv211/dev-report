<?php
class ShopifyModel extends RootModel {
	function getOrder($filter, $limit = 25, $offset = 0, $sort = 'created_at', $order = 'desc') {
		$mongo = EcoDb::get_instance()->getDb();
		$order_collection = $mongo->selectCollection('orders');
		if ($order == 'asc') {
			$order = 1;
		} else {
			$order = -1;
		}
		$cursor = $order_collection->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
		$orderList = array();
		foreach ($cursor as $_order) {
			$orderList[] = $_order;
		}
		return $orderList;
	}

	function updateTracking($order_id, $tracking_status) {
		$mongo = EcoDb::get_instance()->getDb();
		$order_collection = $mongo->selectCollection('orders');
		$query = array('order_id' => $order_id);
		$update = array('tracking_status' => $tracking_status);
		$update = array('$set' => $update);
		$order_collection->updateOne($query, $update);
	}

	function countOrder($filter) {
		$mongo = EcoDb::get_instance()->getDb();
		$order_collection = $mongo->selectCollection('orders');
		return $order_collection->count($filter);
	}

	function getAliOrderList($orderListId) {
		$mongo = EcoDb::get_instance()->getDb();
		$order_mapping = $mongo->selectCollection('order_mapping');
		$cursor = $order_mapping->find(array('order_id' => array('$in' => $orderListId)));
		$aliOrderId = array();
		foreach ($cursor as $_order_mapping) {
			$aliOrderId[$_order_mapping['order_id']][] = $_order_mapping['ali_order_id'];
		}
		return $aliOrderId;
	}


	function getOrderList($orderListId) {
		$mongo = EcoDb::get_instance()->getDb();
		$order_mapping = $mongo->selectCollection('order_mapping');
		$cursor = $order_mapping->find(array('ali_order_id' => array('$in' => $orderListId)));
		$aliOrderId = array();
		foreach ($cursor as $_order_mapping) {
			$aliOrderId[$_order_mapping['ali_order_id']][] = $_order_mapping['order_id'];
		}
		return $aliOrderId;
	}

	function getProductList($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$product_collection = $mongo->selectCollection('products');
		$cursor = $product_collection->find($filter, array('sort' => array('sub_total' => -1)));
		$productList = array();
		foreach ($cursor as $_product) {
			$productList[] = $_product;
		}
		return $productList;
	}

	function getProduct($pid) {
		$mongo = EcoDb::get_instance()->getDb();
		$product_collection = $mongo->selectCollection('products');
		return $product_collection->findOne(array('product_id' => $pid));
	}

	function mapProduct($pid, $ali_pid) {
		$mongo = EcoDb::get_instance()->getDb();
		$product_collection = $mongo->selectCollection('products');
		$ali_product_collection = $mongo->selectCollection('ali_products');
		$product = $product_collection->findOne(array('product_id' => $pid));
		$ali_product = $ali_product_collection->findOne(array('product_id' => $ali_pid));

		if (empty($product) || empty($ali_product)) {
			return false;
		}


		$_product_ali = array(
			'product_id' => $ali_product['product_id'],
			'product_name' => $ali_product['name'],
			'img' => $ali_product['img'],
			'url' => $ali_product['url'],
			'price' => $ali_product['price']
		);

		$query = array('product_id' => $pid);
		$update = array('product_ali' => $_product_ali);
		$update = array('$set' => $update);
		return $product_collection->updateOne($query, $update);
	}

	function getProductShippingCost($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$product_collection = $mongo->selectCollection('product_shipping_cost');
		$cursor = $product_collection->find($filter);
		$shippingList = array();
		foreach ($cursor as $_ship) {
			$_product_id = $_ship['product_id'];
			$shippingList[$_product_id][] = $_ship;
		}
		return $shippingList;
	}

	function countProductList($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$product_collection = $mongo->selectCollection('products');
		return $product_collection->count($filter);

	}

}