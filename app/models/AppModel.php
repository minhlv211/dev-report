<?php


class AppModel extends RootModel {

    /**
     * @return \MongoDB\Collection
     */
    function getCollection() {
        $db = $this->getDB();
        return $db->selectCollection('apps');
    }


    function getAppList($query = array(), $limit = 25, $offset = 0, $sort = '_id', $order = 'desc') {
        $collection = $this->getCollection();
		if ($query == null) {
			$query = array();
		}
        if ($order == 'desc') {
			$order = -1;
		} else {
			$order = 1;
		}
        $cursor = $collection->find($query, array('sort' => array($sort => $order), 'limit' => $limit, 'skip' => $offset));
        $appList = array();
        $index = $offset;
        foreach ($cursor as $_app) {
			$_app['no'] = ++$index;
            $appList[] = $_app;
        }

        return $appList;
    }

	function countAppList($status = null) {
		$collection = $this->getCollection();
		$query = array();
		if ($status !== null) {
			$query = array('status' => $status);
		}

		return $collection->count($query);
	}



	function getAppByPackage($package) {
		try {
			$collection = $this->getCollection();
			return $collection->findOne(array('package_name' => $package));
		} catch (MongoDB\Driver\Exception\InvalidArgumentException $e) {
			return false;
		}

	}

    function getAppInfo($app_id) {
        try {
            $collection = $this->getCollection();
            return $collection->findOne(array('_id' => new MongoDB\BSON\ObjectID($app_id)));
        } catch (MongoDB\Driver\Exception\InvalidArgumentException $e) {
            return false;
        }
    }

    function editApp($app_id, $appInfo) {
        try {
            $collection = $this->getCollection();
            $query = array('_id' => new MongoDB\BSON\ObjectID($app_id));
            unset($appInfo['_id']);
			unset($appInfo['count']);
			$appInfo['last_modify'] = date('Y-m-d H:i:s');
            return $collection->updateOne($query, array('$set' => $appInfo));
        } catch (MongoDB\Driver\Exception\InvalidArgumentException $e) {
            return false;

        }
    }

    function createApp($appInfo) {
        $collection = $this->getCollection();
		$appInfo['create_at'] = date('Y-m-d H:i:s');
		$appInfo['last_modify'] = date('Y-m-d H:i:s');
        return $collection->insertOne($appInfo);
    }
}