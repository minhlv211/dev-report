<?php
class EmailModel extends RootModel {

	function countEmail($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('emails');
		return $fb_comments->count($filter);
	}
	function getEmailList($filter  = array(), $limit = 25, $offset = 0, $sort = 'to_time', $order = 'desc') {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('emails');
		if ($order == 'asc') {
			$order = 1;
		} else {
			$order = -1;
		}
		$cursor = $fb_comments->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
		$commentList = array();
		foreach ($cursor as $_comment) {
			$commentList[] = $_comment;
		}
		return $commentList;
	}


	function countTicket($filter = array()) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('ticket');
		return $fb_comments->count($filter);
	}
	function getTicketList($filter  = array(), $limit = 25, $offset = 0, $sort = 'date_time', $order = 'desc') {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('ticket');
		if ($order == 'asc') {
			$order = 1;
		} else {
			$order = -1;
		}
		$cursor = $fb_comments->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
		$commentList = array();
		foreach ($cursor as $_comment) {
			$commentList[] = $_comment;
		}
		return $commentList;
	}

	function getTicket($email) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('ticket');
		return $fb_comments->findOne(array('email' => $email));

	}

	function getEmail($id) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('emails');
		return $fb_comments->findOne(array('id' => $id));
	}

	function ignoreComment($id) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('fb_comments');
		$query = array('id' => $id);
		$update = array('$set' => array('reply' => 3));
		return $fb_comments->updateOne($query, $update);
	}
	function unIgnoreComment($id) {
		$mongo = EcoDb::get_instance()->getDb();
		$fb_comments = $mongo->selectCollection('fb_comments');
		$commentInfo = $fb_comments->findOne(array('id' => $id));
		$reply = 2;
		if (count($commentInfo['comments'])) {
			$count = count($commentInfo['comments']);
			$last_comment = $commentInfo['comments'][$count - 1];
			$_last_id = $last_comment['from_id'];
			if ($_last_id == '1856139851382075' || $_last_id == '282042242270891') {
				$reply = 1;
			}
		}
		$query = array('id' => $id);
		$update = array('$set' => array('reply' => $reply));
		return $fb_comments->updateOne($query, $update);
	}

}