<?php
require_once __DIR__ . '/RootModel.php';
class TrackingNumberModel extends RootModel {
	/**
	 * @return \MongoDB\Collection
	 */
	function _getCollection() {
		static $_collection = null;
		if (empty($_collection)) {
			$mongo = EcoDb::get_instance()->getDb();
			$_collection = $mongo->selectCollection('trackings');
		}
		return $_collection;
	}

	function getTrackingList($filter = array(), $limit = 25, $offset = 0, $sort = 'start_time', $order = 'desc') {
		$_collection = $this->_getCollection();
		if ($order == 'asc') {
			$order = 1;
		} else {
			$order = -1;
		}
		$cursor = $_collection->find($filter, array('limit' => $limit, 'skip' => $offset, 'sort' => array($sort => $order)));
		$list = array();
		foreach ($cursor as $_track) {
			$list[] = $_track;
		}
		return $list;
	}

	function countTracking($filter) {
		$_collection = $this->_getCollection();
		return $_collection->count($filter);
	}

	function getTracking($tracking_number) {
		$_collection = $this->_getCollection();
		$query = array('tracking_number' => $tracking_number);
		$trackingInfo = $_collection->findOne($query);
		return $trackingInfo;
	}

	function updateTracking($update_info) {
		$tracking_number = $update_info['tracking_number'] . '';
		unset($update_info['tracking_number']);

		$tracking_collections = $this->_getCollection();
		$query = array('tracking_number' => $tracking_number);
		$update = array('$set' => $update_info);
		$tracking_collections->updateOne($query, $update, array('upsert' => true));
	}

	function updateCarrierList($carrierList) {
		$mongo = EcoDb::get_instance()->getDb();
		$_collection = $mongo->selectCollection('carrier_list');
		foreach ($carrierList as $carrier ){
			$code = $carrier['code'];
			unset($carrier['code']);
			$query = array('code' => $code);
			$carrier['last_update'] = date('Y-m-d H:i:s');
			$update = array('$set' => $carrier);

			$_collection ->updateOne($query, $update, array('upsert' => true));
		}
		return true;

	}

	function getCarrierList() {
		$mongo = EcoDb::get_instance()->getDb();
		$_collection = $mongo->selectCollection('carrier_list');

		$cursor = $_collection->find();
		$carrierList = array();
		foreach ($cursor as $_carrier) {
			$_code = $_carrier['code'];
			$carrierList[$_code] = $_carrier;
		}
		return $carrierList;
	}

	function trackingStatistic($filters, $field) {
		$listFields = array(
			'count',
			'eco_process_time',
			'eco_process_time_count',
			'process_date',
			'process_date_count',
			'export_date',
			'export_date_count',
			'ali_deliver_date',
			'ali_deliver_date_count',
			'total_deliver_date',
			'total_deliver_date_count',
		);

		$mongo = EcoDb::get_instance()->getDb();
		$_collection = $mongo->selectCollection('tracking_stat');
		$cursor = $_collection->find($filters);

		$trackingCount = array();
		foreach ($cursor as $_track) {

			if ($field == 'month') {
				$_value = $_track['order_date'];
				$_value = substr($_value, 0, 7);
			} elseif ($field == 'weekday') {
				$_value = $_track['order_date'];
				$_value = date('w', strtotime($_value));
			} else {
				$_value = $_track[$field];
			}

			$trackingCount[$_value]['value'] = $_value;
			foreach ($listFields as $_f) {
				$trackingCount[$_value][$_f] += $_track[$_f];
			}
			if ($_track['status'] == 'Delivered') {
				$trackingCount[$_value]['success'] += $_track['count'];
			}
		}
		return $trackingCount;
	}

	function trackingStatusStatistic($filters, $field) {
		$mongo = EcoDb::get_instance()->getDb();
		$_collection = $mongo->selectCollection('tracking_stat');
		$cursor = $_collection->find($filters);

		$trackingCount = array();
		foreach ($cursor as $_track) {
			if ($field == 'month') {
				$_value = $_track['order_date'];
				$_value = substr($_value, 0, 7);
			} elseif ($field == 'weekday') {
				$_value = $_track['order_date'];
				$_value = date('w', strtotime($_value));
			} else {
				$_value = $_track[$field];
			}
			$trackingCount[$_value]['value'] = $_value;
			$status = $_track['status'];
			$trackingCount[$_value][$status] += $_track['count'];
			$trackingCount[$_value]['total'] += $_track['count'];
		}
		return $trackingCount;
	}


	function trackingReport($filters,$field)
    {
        $_collection = $this->_getCollection();
        $cursor = $_collection->find($filters);


        $trackingCount = array();
        foreach ($cursor as $_track) {
            if ($field == 'destination_country_iso3') {
                $_track[$field] = CountryHelper::convertISO3ToISO2($_track[$field]);
            }
            $trackingCount[$_track[$field]]['value'] = $_track[$field];
            $trackingCount[$_track[$field]]['count'] += 1;
        }

        return $trackingCount;
    }

}