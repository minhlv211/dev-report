<?php
define('ROOT_DIR' , __DIR__);
date_default_timezone_set('Asia/Ho_Chi_Minh');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
global $start_exe_time;
$start_exe_time = microtime(true);
require_once("vendor/autoload.php");
$f3 = Base::instance();

$f3->config('config.ini');
$f3->config('routes.ini');

$f3->set('SESSION.dashboard-tool', 'dashboard-tool');

new Session();

$f3->run();